//
//  NotificationViewCellTableViewCell.swift
//  MediaLink
//
//  Created by Naveen on 8/9/18.
//  Copyright © 2018 Naveen. All rights reserved.
//

import UIKit

class NotificationViewCellTableViewCell: UITableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
