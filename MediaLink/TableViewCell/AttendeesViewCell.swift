//
//  AttendeesViewCell.swift
//  MediaLink
//
//  Created by Naveen on 3/1/18.
//  Copyright © 2018 Naveen. All rights reserved.
//

import UIKit
import AlamofireImage

class AttendeesViewCell: UITableViewCell {

    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblFullname: UILabel!
    @IBOutlet weak var lblCompany: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func configCell(_ image: String?,_ fname :String?, _ company :String?){
        imgProfile.image = UIImage.imageWithColor(color: UIColor.lightGray, size: imgProfile.frame.size).af_imageRoundedIntoCircle()
        if let imgName = image{
            if let imageName = imgName.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed){
                if let url = URL(string: imageName){
                    let filter = AspectScaledToFillSizeWithRoundedCornersFilter(
                        size: self.imgProfile.frame.size,
                        radius: self.imgProfile.frame.width/2
                    )
                    self.imgProfile.af_setImage(withURL: url, placeholderImage: UIImage.imageWithColor(color: UIColor.lightGray, size: imgProfile.frame.size),filter: filter, imageTransition: .crossDissolve(0.2))
                }
            }
        }
        if let fullNameStr = fname{
            lblFullname.text = fullNameStr
        }
        if let companyStr = company{
            lblCompany.text = companyStr
        }
    }
    
}
