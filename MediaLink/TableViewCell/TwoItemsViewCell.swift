//
//  TwoItemsViewCell.swift
//  MediaLink
//
//  Created by Naveen on 3/5/18.
//  Copyright © 2018 Naveen. All rights reserved.
//

import UIKit

class TwoItemsViewCell: UITableViewCell {

    // UILabel
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubTitle: UILabel!
    
    // NSLayoutCounstaint
    @IBOutlet weak var leadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var trailingConstraint: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func configCell(_ title:String?,_ subTitle:String?) {
        lblTitle.text = ""
        lblSubTitle.text = ""
        if let titleStr = title {
            lblTitle.text = titleStr
        }
        if let subTitleStr = subTitle {
            lblSubTitle.text = subTitleStr
        }
    }
    
}
