//
//  EventAccessCell.swift
//  MediaLink
//
//  Created by Naveen on 4/11/18.
//  Copyright © 2018 Naveen. All rights reserved.
//

import UIKit

class EventAccessCell: UITableViewCell {
    @IBOutlet weak var lblMainTitle: UILabel!
    @IBOutlet weak var lblSubTitle: UILabel!
    @IBOutlet weak var lblLastItem: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configCell(_ mainTile:String?,_ subTitle:String?,_ lastItem:String?){
        lblMainTitle.text = ""
        lblSubTitle.text = ""
        lblLastItem.text = ""
        if let titleStr = mainTile{
            lblMainTitle.text = titleStr
        }
        if let subTitleStr = subTitle{
            lblSubTitle.text = subTitleStr
        }
        if let lastItemStr = lastItem{
            lblLastItem.text = lastItemStr
        }
        
    }
    
}
