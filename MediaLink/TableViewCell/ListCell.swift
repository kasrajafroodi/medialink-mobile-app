//
//  ScheduleCell.swift
//  jafroodi
//
//  Created by Naveen on 3/28/17.
//  Copyright © 2017 Naveen. All rights reserved.
//

import UIKit

class ListCell: UITableViewCell {

    @IBOutlet var lblTime: UILabel!
    @IBOutlet var lblLocation: UILabel!
    @IBOutlet var lblClientCompany: UILabel!
    @IBOutlet var lblTargetCompany: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func configCell(_ time:String?,_ location:String?,_ client:String?,_ target:String?){
        lblTime.text = ""
        lblLocation.text=""
        lblClientCompany.text=""
        lblTargetCompany.text=""
        if let timeStr = time{
            lblTime.text=timeFormatWithDay(timeStr)
        }
        if let locStr = location {
            lblLocation.text=locStr
        }
        if let clientStr = client{
            lblClientCompany.text=clientStr
        }
        if let targetStr = target{
            lblTargetCompany.text=targetStr
        }
    }
    
}
