//
//  OnTheGroundViewCell.swift
//  MediaLink
//
//  Created by Naveen on 3/8/18.
//  Copyright © 2018 Naveen. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
class customUILabel: UILabel {
    var indexValue:IndexPath!
    var section = 0
    var row = 0
}
class OnTheGroundViewCell: UITableViewCell {

    @IBOutlet var lblTime: UILabel!
    @IBOutlet var lblLocation: UILabel!
    @IBOutlet var lblClientCompany: customUILabel!
    @IBOutlet var lblTargetCompany: customUILabel!
    @IBOutlet weak var lblMeetingsCover: customUILabel!
    @IBOutlet var lblStatus: UILabel!
    var objMeetingCellItem:Meeting!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func configCell(_ objMeeting:Meeting ){
        lblTime.text = ""
        lblLocation.text=""
        lblClientCompany.text=""
        lblTargetCompany.text=""
        lblMeetingsCover.text = ""
        lblStatus.text = ""
        objMeetingCellItem = objMeeting
        if let timeStr = objMeeting.time{
            lblTime.text=timeFormatWithDay(timeStr)
        }
        if let locStr = objMeeting.location {
            lblLocation.text=locStr
        }
        lblClientCompany.textColor = UIColor.black
        if let clientStr = objMeeting.clientName{
            lblClientCompany.text=clientStr
            if let clientColor = objMeeting.clientColor{
                switch clientColor{
                case 0:
                    lblClientCompany.textColor = UIColor.black
                case 1:
                    lblClientCompany.textColor = UIColor.colorGreen
                case 2:
                    lblClientCompany.textColor = UIColor.colorRed
                default:
                    break
                }
                
            }
        }
        lblTargetCompany.textColor = UIColor.black
        if let targetStr = objMeeting.company{
            lblTargetCompany.text=targetStr
            if let companyColor = objMeeting.companyColor{
                switch companyColor{
                case 0:
                    lblTargetCompany.textColor = UIColor.black
                case 1:
                    lblTargetCompany.textColor = UIColor.colorGreen
                case 2:
                    lblTargetCompany.textColor = UIColor.colorRed
                default:
                    break
                }
                
            }
        }
        lblMeetingsCover.textColor = UIColor.black
        if let meetingStr = objMeeting.meetingCoverName{
            lblMeetingsCover.text=meetingStr
            if let meetingColor = objMeeting.meetingColor{
                switch meetingColor{
                case 0:
                    lblMeetingsCover.textColor = UIColor.black
                case 1:
                    lblMeetingsCover.textColor = UIColor.colorGreen
                case 2:
                    lblMeetingsCover.textColor = UIColor.colorRed
                default:
                    break
                }
                
            }
        }
        if let statusStr = objMeeting.requestStatus{
            lblStatus.text = statusStr
        }
    }
}
