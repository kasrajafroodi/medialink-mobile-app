//
//  CommonCell.swift
//  Jafroodi
//
//  Created by Naveen on 2/27/18.
//  Copyright © 2018 Naveen. All rights reserved.
//

import UIKit

class CommonCell: UITableViewCell {

    @IBOutlet weak var lblItem: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func configCell(_ item:String?){
        if let itemStr = item{
            lblItem.text = itemStr
        }
    }
    
}
