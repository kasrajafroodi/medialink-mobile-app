//
//  SignInsTableViewController.swift
//  MediaLink
//
//  Created by Naveen on 2/27/18.
//  Copyright © 2018 Naveen. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper
import SwiftyJSON
import Firebase
import Realm
import RealmSwift

class SignInsTableViewController: UITableViewController,UITextFieldDelegate {
    var fieldArray = [UITextField]()
    var activeField: Any!
    var arrNotifications = [Notification]()
    
    @IBOutlet var viewBack: UIView!
    @IBOutlet var txtuserName: UITextField!
    @IBOutlet var txtPassword: UITextField!
    @IBOutlet var btnSignIn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        self.tableView.estimatedRowHeight=150
        self.tableView.rowHeight=UITableViewAutomaticDimension
        viewBack.layer.masksToBounds = true
        viewBack.layer.cornerRadius = 8
        viewBack.layer.borderColor = UIColor.lightGray.cgColor
        viewBack.layer.borderWidth = 0.5
        fieldArray=[txtuserName,txtPassword]
        btnSignIn.layer.masksToBounds = true
        btnSignIn.layer.cornerRadius = 8
        self.tableView.alwaysBounceVertical = false
        hideKeyboardWhenTappedAround()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewDidAppear(_ animated: Bool) {
        
        
    }
    override func didRotate(from fromInterfaceOrientation: UIInterfaceOrientation) {
        self.tableView.reloadData()
    }
//    override var prefersStatusBarHidden: Bool {
//        return true
//    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.returnKeyType == .done {
            textField.resignFirstResponder()
        }
        else {
            let index = self.fieldArray.index(of: textField)
            if index! == NSNotFound || index! + 1 == fieldArray.count {
                return false
            }
            let nextField = fieldArray[index! + 1]
            activeField = nextField
            nextField.becomeFirstResponder()
        }
        return false
    }
    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    /*
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        // Configure the cell...

        return cell
    }
    */
    
    @IBAction func ClickedSignin(_ sender: Any) {
        if Reachability.isConnectedToNetwork() {
            if (txtuserName.text?.isBlank)!{
                self.alert(message: "Enter Username")
            } else if (txtPassword.text?.isBlank)! {
                self.alert(message: "Enter Password")
            } else {
                let url = MediaLinkUrls.login
                var params = [
                    "email":txtuserName.text!,
                    "password":txtPassword.text!
                ]
                if let deviceToken = InstanceID.instanceID().token() {
                    params["device_id"] =  deviceToken
                }
                activityIndicatorShow(button: btnSignIn)
                Alamofire.request(url, method: .post, parameters: params).validate()
                    .responseObject { (response:DataResponse<Login>) in
                        activityIndicatorHide(button: self.btnSignIn)
                        if response.result.error == nil {
                            if let serverResponce = response.result.value {
                                let repo = MediaLinkUserDefaults()
                                repo.setLoggedUserData(serverResponce)
                                
                                UIApplication.shared.statusBarStyle = .lightContent
                                
                                let obj = UIApplication.shared.delegate as! AppDelegate
                                obj.setRootViewcontroller()
                                
                                let realm = try! Realm()
                                let notifications = realm.objects(Notification.self)
                                for element in notifications {
                                    self.arrNotifications.append(element)
                                }
                                self.getNotificationListFromServer()
                                
                                self.dismiss(animated: true, completion: nil)
                            }
                        } else if response.response?.statusCode == 422 {
                            if let data = response.data {
                                let responseJSON = try! JSON(data: data)
                                if responseJSON["email"].exists(){
                                    if let message = responseJSON["username"][0].string {
                                        self.alert(message: message)
                                    }
                                } else if responseJSON["password"].exists() {
                                    if let message = responseJSON["password"][0].string {
                                        self.alert(message: message)
                                    }
                                } else if responseJSON["message"].exists() {
                                    if let message = responseJSON["message"].string {
                                        print(message)
                                        self.alert(message: "Invalid User Credentials")
                                    }
                                }
                            }
                        } else {
                            self.alert(message: checkNetworkError(response.result.error!))
                        }
                }
            }
        }else{
            self.alert(message: "It seems that there is no Internet connection.")
        }
    }

    // Get notifications
    func getNotificationListFromServer() {
        let url = MediaLinkUrls.notification
        let repo = MediaLinkUserDefaults()
        let todayDate = Date()
        repo.notificationDate = todayDate.stringFormatted
        var params = ["token":repo.tocken!]
        params["date"] = todayDate.stringFormatted
        
        request(url, method: .get, parameters: params)
            .validate()
            .responseArray { (response: DataResponse<[Notification]>) in
                
                switch response.result {
                case.success(let successValue):
                    if successValue.count != 0 {
                        for element in successValue {
                            if !self.arrNotifications.contains(where: { $0.id == element.id }) {
                                let notification = Notification()
                                notification.id = element.id
                                notification.date = Date()
                                notification.status = 0
                                notification.title = element.title
                                notification.message = element.message
                                
                                do {
                                    let realm = try Realm()
                                    try realm.write {
                                        realm.add(notification, update: true)
                                    }
                                } catch let error as NSError {
                                    print(error.localizedDescription)
                                }
                            }
                        }
                    }
                case.failure(let error):
                    if let statusCode = response.response?.statusCode {
                        if statusCode == 422 {
                            
                        }
                    } else {
                        print(checkNetworkError(error))
                    }
                }
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
