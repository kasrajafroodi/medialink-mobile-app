//
//  RightMenuViewController.swift
//  MediaLink
//
//  Created by Naveen on 8/3/18.
//  Copyright © 2018 Naveen. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper
import DZNEmptyDataSet
import Realm
import RealmSwift

class RightMenuViewController: UIViewController {

    // UITableView
    @IBOutlet weak var tableView: UITableView! {
        didSet{
            // Set footer view
            tableView.tableFooterView = UIView()
            
            // Set a background image for table view
            let image = UIImageView(image: UIImage(named: "menu-bg"))
            image.contentMode = .scaleAspectFill
            image.alpha = 0.85
            tableView.backgroundView = image
        }
    }
    
    // UIButton
    @IBOutlet weak var btnDone: UIButton! {
        didSet{
            btnDone.layer.borderColor = UIColor.white.cgColor
            btnDone.isHidden = true
            btnDone.addTarget(self, action: #selector(didDoneBtnClicked(sender:)), for: .touchUpInside)
        }
    }
    
    // Identifire
    let cellIdentifier = "NotificationViewCellTableViewCell"
    
    // Instance for slide menu controller
    var mainViewController: UIViewController!
    
    // Array
    var arrNotifications = [Notification]()
    
    // Register table view cell
    fileprivate func registerTableViewCell() {
        let nibName = UINib(nibName: "NotificationViewCellTableViewCell", bundle: nil)
        tableView.register(nibName, forCellReuseIdentifier: cellIdentifier)
    }
    
    // Gesture recognizer
    fileprivate func addGestureRecognizer() {
        let longPressGesture = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPressOfTableView(sender:)))
        longPressGesture.minimumPressDuration = 1.0
        tableView.addGestureRecognizer(longPressGesture)
    }
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Additional setup after loading the view.
//        addGestureRecognizer()
        registerTableViewCell()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        // Get realm data to display
        self.arrNotifications.removeAll()
        let realm = try! Realm()
        let predicate = NSPredicate(format: "status == %d", 0)
        let notifications = realm.objects(Notification.self).filter(predicate)
        for element in notifications {
            self.arrNotifications.append(element)
        }
        self.tableView.reloadData()
        self.tableView.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Actions
    
    // Gesture
    @objc func handleLongPressOfTableView(sender: UILongPressGestureRecognizer) {
        if sender.state == .ended {
//            let touchPoint = sender.location(in: self.tableView)
//            let indexPath = tableView.indexPathForRow(at: touchPoint)
            btnDone.isHidden = false
            tableView.isEditing = true
        }
    }
    
    @objc func didDoneBtnClicked(sender: UIButton) {
        tableView.isEditing = false
        btnDone.isHidden = true
    }
    
    // MARK: - Webservice
    
   /* */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

// MARK: - Table view data source methods

extension RightMenuViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrNotifications.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! NotificationViewCellTableViewCell
        
        // Initiate data to a variable
        let notification = arrNotifications[indexPath.row]
        
        // Configure the cell
        cell.accessoryType = .none
        cell.selectionStyle = .none
        
        // Set appearance
        cell.backgroundColor = UIColor.clear
        
        // Date formatting
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy hh:mm a"
        if let date = notification.date {
            let strDate = dateFormatter.string(from: date)
            cell.lblDate.text = strDate
        } else {
            cell.lblDate.text = ""
        }
        
        // Set values to ui
        cell.lblTitle.text = notification.title
        cell.lblMessage.text = notification.message
        
        
        return cell
    }
}

// MARK: - Table view delegate methods

extension RightMenuViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            
            // Get element of indexpath
            let element = arrNotifications[indexPath.row]
            
            do {
                // Initialize realm
                let realm = try Realm()
                
                // Get realm object of indexpath
                let result = realm.object(ofType: Notification.self, forPrimaryKey: element.id)
                
                // Realm write
                try realm.write {
                    result?.status = 1 // Change status of realm object
                }
            } catch let error as NSError { // be safe
                // Print error for debug
                print(error.localizedDescription)
            }
            
            // Remove element from array
            arrNotifications.remove(at: indexPath.row)
            
            // Remove row at indexpath
            tableView.deleteRows(at: [indexPath], with: .automatic)
            
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // End editing on select
        tableView.isEditing = false
    }
    
}

// MARK: - Table view empty data set methods

extension RightMenuViewController: DZNEmptyDataSetSource, DZNEmptyDataSetDelegate {
    
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        let text="No Older Notifications"
        let attributes = [NSAttributedStringKey.font: UIFont.boldSystemFont(ofSize: 18.0), NSAttributedStringKey.foregroundColor: UIColor.hex(hexStr: "ffffff", alpha: 0.7)]
        return NSAttributedString(string: text, attributes: attributes)
    }
}
