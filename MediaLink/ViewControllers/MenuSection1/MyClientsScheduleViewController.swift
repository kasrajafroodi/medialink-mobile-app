//
//  MyClientsScheduleViewController.swift
//  MediaLink
//
//  Created by Naveen on 3/2/18.
//  Copyright © 2018 Naveen. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper
import SwiftyJSON
import AlamofireImage
import RKTagsView

class MyClientsScheduleViewController: UIViewController {
    
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblCompany: UILabel!
    @IBOutlet weak var btnClient: UIButton!
    @IBOutlet weak var btnOutreach: UIButton!
    @IBOutlet weak var btnMeetingsCover: UIButton!
    @IBOutlet weak var schedulerTagsView: RKTagsView!
    @IBOutlet weak var lblRequestNotes: UILabel!
    @IBOutlet weak var lblMeetingNotes: UILabel!
    @IBOutlet weak var tblTargetAttendies: UITableView!
    @IBOutlet weak var tblClientAttendies: UITableView!
    @IBOutlet weak var hightTblTargetAttendies: NSLayoutConstraint!
    @IBOutlet weak var hightTblClientAttendees: NSLayoutConstraint!    
    @IBOutlet weak var lblDuration: UILabel!
    var objMeeting:Meeting!
    var arrTargetAttendees = [Attendees]()
    var arrClientAttendees = [Attendees]()
    var objMeetingDetails:ViewMeetingDetails!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        //tagsView
        schedulerTagsView.allowsMultipleSelection = false
        schedulerTagsView.editable = false
        schedulerTagsView.scrollsHorizontally = false
        schedulerTagsView.delegate = self
        schedulerTagsView.font = UIFont.systemFont(ofSize: 14)
        
        self.tblTargetAttendies.tableFooterView = UIView()
        self.tblTargetAttendies.register(UINib(nibName: "AttendeesViewCell", bundle: nil), forCellReuseIdentifier: "AttendeesViewCell")
        self.tblTargetAttendies.register(UINib(nibName: "MoreViewCell", bundle: nil), forCellReuseIdentifier: "MoreViewCell")
        self.tblClientAttendies.tableFooterView = UIView()
        self.tblClientAttendies.register(UINib(nibName: "AttendeesViewCell", bundle: nil), forCellReuseIdentifier: "AttendeesViewCell")
        self.tblClientAttendies.register(UINib(nibName: "MoreViewCell", bundle: nil), forCellReuseIdentifier: "MoreViewCell")
        getDataFromServer()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillLayoutSubviews() {
        self.hightTblTargetAttendies.constant = self.tblTargetAttendies.contentSize.height
        self.hightTblClientAttendees.constant = self.tblClientAttendies.contentSize.height
        self.view.layoutIfNeeded()
        super.viewDidLayoutSubviews()
    }
    func showEmployeeDatabase(_ employeeId:Int){
        let storyboard = UIStoryboard(name: "MenuSection3", bundle: nil)
        let destVc = storyboard.instantiateViewController(withIdentifier: "EmployeeDatabaseDetailsTableViewController") as! EmployeeDatabaseDetailsTableViewController
        destVc.isFromOtherView = true
        destVc.empId = employeeId
        let nav = UINavigationController(rootViewController: destVc)
        if !DeviceType(){
            self.modalPresentationStyle = .popover
        }
        self.present(nav, animated: true, completion: nil)
    }
    func setData(_ objMeetingDetails : ViewMeetingDetails){
        if let dateStr = objMeetingDetails.date {
            if dateStr != "" {
                lblDate.text = dateFormatWithDay(dateStr)
            }else {
                lblDate.text = "No Date"
            }
        }
        if let timeStr = objMeetingDetails.time {
            if timeStr != "" {
                lblTime.text = timeFormatWithDay(timeStr)
            }else {
                lblTime.text = "No Time"
            }
        }
        if let durationStr = objMeetingDetails.duration {
            lblDuration.text = durationStr
        }
        if let locationStr = objMeetingDetails.location{
            lblLocation.text = locationStr
        }
        if let statusStr = objMeetingDetails.requestStatus{
            lblStatus.text = statusStr
        }
        if let companyStr = objMeetingDetails.company{
            lblCompany.text = companyStr
        }
        if let clientStr = objMeetingDetails.clientName {
            btnClient.setTitle(clientStr, for: .normal)
        }
        if let meetingCoverStr = objMeetingDetails.meetingCoverName{
            btnMeetingsCover.setTitle(meetingCoverStr, for: .normal)
        }
        if let outreachLeadStr = objMeetingDetails.outreachLeadName{
            btnOutreach.setTitle(outreachLeadStr, for: .normal)
        }
        if objMeetingDetails.employees?.count != 0{
            for objItem in objMeetingDetails.employees!{
                self.schedulerTagsView.addTag(objItem.firstName!+" "+objItem.lastName!)
            }
        }
        if let requstNoteStr = objMeetingDetails.quickNote{
            lblRequestNotes.text = requstNoteStr
        }
        if let meetingNoteStr = objMeetingDetails.meetingNotes {
            lblMeetingNotes.text = meetingNoteStr
        }
        if objMeetingDetails.targetAttendees?.count != 0{
            self.arrTargetAttendees = objMeetingDetails.targetAttendees!
            self.tblTargetAttendies.reloadData()
        }
        if objMeetingDetails.clientAttendees?.count != 0{
            self.arrClientAttendees = objMeetingDetails.clientAttendees!
            self.tblClientAttendies.reloadData()
        }
        self.view.layoutIfNeeded()
    }
    //Mark Actions
    @IBAction func clickedAddToCalender(_ sender: Any) {
        if objMeetingDetails != nil{
            if let dateStr = self.objMeetingDetails.date {
                if dateStr != "" {
                    let optionMenu = UIAlertController(title: "", message: "Do you want to add reminder to Calendar?", preferredStyle: .alert)
                    let addEvent = UIAlertAction(title: "Yes", style: .default, handler: {
                        (alert: UIAlertAction!) -> Void in
                        
                                addEventToCalendar(self.objMeetingDetails.meetingCoverName,self.objMeetingDetails.location , dateFormatBase(self.objMeetingDetails.date!), dateFormatBase(self.objMeetingDetails.date!),self)
                        
                    })
                    let cancelAction = UIAlertAction(title: "No", style: .cancel, handler: {
                        (alert: UIAlertAction!) -> Void in
                    })
                    optionMenu.addAction(addEvent)
                    optionMenu.addAction(cancelAction)
                    self.present(optionMenu, animated: true, completion: nil)
                }else {
                    self.alert(message: "No date to add reminder.")
                }
            }else {
                self.alert(message: "No date to add reminder.")
            }
        }
    }
    @IBAction func clicckedClient(_ sender: Any) {
        if objMeetingDetails != nil{
            if let clientId = objMeetingDetails.clientId{
                let storyboard = UIStoryboard(name: "MenuSection3", bundle: nil)
                let destVc = storyboard.instantiateViewController(withIdentifier: "ClientDatabaseDetailsTableViewController") as! ClientDatabaseDetailsTableViewController
                destVc.isFromOtherView = true
                destVc.clientId = clientId
                let nav = UINavigationController(rootViewController: destVc)
                if !DeviceType(){
                    self.modalPresentationStyle = .popover
                }
                self.present(nav, animated: true, completion: nil)
            }
        }
    }
    @IBAction func clickedMeetingCover(_ sender: Any) {
        if objMeetingDetails != nil{
            if let meetingCoverId = objMeetingDetails.meetingCover{
                showEmployeeDatabase(meetingCoverId)
            }
        }
    }
    @IBAction func clickedOutreach(_ sender: Any) {
        if objMeetingDetails != nil{
            if let outreachId = objMeetingDetails.outreachLead{
                showEmployeeDatabase(outreachId)
            }
        }
    }
    //Server Response
    func getDataFromServer(){
        if Reachability.isConnectedToNetwork(){
            if objMeeting != nil{
                let url = MediaLinkUrls.viewMeetingDetails+String(objMeeting.id!)
                let repo = MediaLinkUserDefaults()
                let params = ["token":repo.tocken!]
                activityIndicatorShow(self)
                Alamofire.request(url, method: .get, parameters: params).validate()
                    .responseObject { (response:DataResponse<ViewMeetingDetails>) in
                        activityIndicatorHide()
                        if response.result.error == nil{
                            if let serverResponce = response.result.value{
                                self.objMeetingDetails = serverResponce
                                self.setData(serverResponce)
                            }
                        }else if response.response?.statusCode == 401{
                            authenticationError(self)
                        }else{
                            self.alert(message: checkNetworkError(response.result.error!))
                        }
                }
            }
        }else{
            self.alert(message: "It seems that there is no Internet connection.")
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
//Tags View
extension MyClientsScheduleViewController: RKTagsViewDelegate{
    func tagsView(_ tagsView: RKTagsView, shouldSelectTagAt index: Int) -> Bool {
        if let empId = objMeetingDetails.employees![index].id{
            showEmployeeDatabase(empId)
        }
        return true
    }
}
// MARK:- UITableView

extension MyClientsScheduleViewController: UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tblTargetAttendies{
            if arrTargetAttendees.count > 3{
                return 4
            }
            return arrTargetAttendees.count
        }else{
            if arrClientAttendees.count > 3{
                return 4
            }
            return arrClientAttendees.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AttendeesViewCell", for: indexPath)as! AttendeesViewCell
        switch indexPath.row {
        case 0,1,2:
            var objAttendee:Attendees!
            if tableView == tblTargetAttendies{
                objAttendee = arrTargetAttendees[indexPath.row]
            }else{
                objAttendee = arrClientAttendees[indexPath.row]
            }
            cell.configCell(objAttendee.picture, objAttendee.firstName!+" "+objAttendee.lastName!, objAttendee.company)
            
        case 3:
            let cell = tableView.dequeueReusableCell(withIdentifier: "MoreViewCell", for: indexPath)as! MoreViewCell
            return cell
        default:
            break
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0,1,2:
            let storyboard = UIStoryboard(name: "MenuSection2", bundle: nil)
            let destVc = storyboard.instantiateViewController(withIdentifier: "ContactDatabaseDetailsTableViewController") as! ContactDatabaseDetailsTableViewController
            if tableView == tblTargetAttendies{
                destVc.contactId = arrTargetAttendees[indexPath.row].contactId!
            }else{
                destVc.contactId = arrClientAttendees[indexPath.row].contactId!
            }
            self.show(destVc, sender: nil)
        case 3:
            let storyboard = UIStoryboard(name: "Common", bundle: nil)
            let attendeesVC = storyboard.instantiateViewController(withIdentifier: "AttendeesListViewController") as! AttendeesListViewController
            if tableView == tblTargetAttendies{
                attendeesVC.arrAttendees = arrTargetAttendees
                attendeesVC.arrTempAttendees = arrTargetAttendees
            }else{
                attendeesVC.arrAttendees = arrClientAttendees
                attendeesVC.arrTempAttendees = arrClientAttendees
            }
            
            let nav = UINavigationController(rootViewController: attendeesVC)
            if !DeviceType(){
                self.modalPresentationStyle = .popover
            }
            UIApplication.shared.statusBarStyle = .lightContent
            self.present(nav, animated: true, completion: nil)
        default:
            break
        }
    }
    
}
