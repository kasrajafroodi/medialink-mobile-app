//
//  LeftMenuViewController.swift
//  Jafroodi
//
//  Created by Naveen on 2/27/18.
//  Copyright © 2018 Naveen. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper
import SwiftyJSON

class LeftMenuViewController: UIViewController {

    @IBOutlet weak var tblView: UITableView!
    var mainViewController: UIViewController!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        tblView.register(UINib(nibName: "CommonCell", bundle: nil), forCellReuseIdentifier: "CommonCell")
        tblView.tableFooterView = UIView()
        tblView.estimatedRowHeight = 80
        tblView.rowHeight = UITableViewAutomaticDimension
        
        let image = UIImageView(image: UIImage(named: "menu-bg"))
        image.contentMode = .scaleAspectFill
        image.alpha = 0.85
        tblView.backgroundView = image
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //Server Access
    func logoutFromServer(){
        if Reachability.isConnectedToNetwork(){
            let url = MediaLinkUrls.logout
            let repo = MediaLinkUserDefaults()
            let params = ["token":repo.tocken!]
            Alamofire.request(url, method: .get, parameters: params).validate()
                .responseJSON{ response in
                    if response.result.error == nil{
                        logOutFromApp()
                    }
                    else if response.response?.statusCode == 401 {
                        authenticationError(self)
                    }else{
                        self.alert(message: checkNetworkError(response.result.error!))
                        
                    }
            }
        }else{
            self.alert(message: "It seems that there is no Internet connection.")
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension LeftMenuViewController:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let repo = MediaLinkUserDefaults()
        if repo.isAdmin{
            return 11
        }
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0{
            let cell = tblView.dequeueReusableCell(withIdentifier: "LeftMenuHeadCell") as! LeftMenuHeadCell
            let repo = MediaLinkUserDefaults()
            cell.lblHeading.text = repo.name
            cell.lblEmail.text = repo.email
            cell.backgroundColor = .clear
            return cell
        }else{
            let cell = tblView.dequeueReusableCell(withIdentifier: "CommonCell") as! CommonCell
            cell.lblItem.font = UIFont.systemFont(ofSize: 16)
            switch indexPath.row{
            case 1:
                cell.configCell("My Schedule")
            case 2:
                cell.configCell("My Client’s Schedule")
            case 3:
                cell.configCell("My Client’s Event Access")
            case 4:
                cell.configCell("Event Database")
            case 5:
                cell.configCell("Contact Database")
            case 6:
                cell.configCell("Client Database")
            case 7:
                cell.configCell("Employee Database")
            case 8:
                cell.configCell("Enter Meeting Notes")
            case 9:
                let repo = MediaLinkUserDefaults()
                if repo.isAdmin{
                    cell.configCell("On The Ground")
                }else{
                    cell.configCell("Log Out")
                }
                
            case 10:
                cell.configCell("Log Out")
            default:
                break
            }
            cell.lblItem.textColor = UIColor.white
            cell.backgroundColor = .clear
            return cell
        }
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var storyboard = UIStoryboard()
        switch indexPath.row {
        case 1:
            storyboard = UIStoryboard(name: "MenuSection1", bundle: nil)
            if DeviceType(){
                let vc  = storyboard.instantiateViewController(withIdentifier: "MyScheduleViewController")  as! MyScheduleViewController
                let viewController: UIViewController! = UINavigationController(rootViewController: vc)
                self.slideMenuController()?.changeMainViewController(viewController, close: true)
            }else{
                let vc = storyboard.instantiateViewController(withIdentifier: "MyScheduleSplitViewViewController")
                self.slideMenuController()?.changeMainViewController(vc, close: true)
            }
        case 2:
            storyboard = UIStoryboard(name: "MenuSection1", bundle: nil)
            if DeviceType(){
                let vc  = storyboard.instantiateViewController(withIdentifier: "MyClientsScheduleListViewController")  as! MyClientsScheduleListViewController
                let viewController: UIViewController! = UINavigationController(rootViewController: vc)
                self.slideMenuController()?.changeMainViewController(viewController, close: true)
            }else{
                let vc = storyboard.instantiateViewController(withIdentifier: "MyClientsMeetingSplitViewViewController")
                self.slideMenuController()?.changeMainViewController(vc, close: true)
            }
        case 3:
            storyboard = UIStoryboard(name: "MenuSection2", bundle: nil)
            if DeviceType(){
                let vc  = storyboard.instantiateViewController(withIdentifier: "MyClientsEventAccessListViewController")  as! MyClientsEventAccessListViewController
                let viewController: UIViewController! = UINavigationController(rootViewController: vc)
                self.slideMenuController()?.changeMainViewController(viewController, close: true)
            }else{
                let vc = storyboard.instantiateViewController(withIdentifier: "MyClientsEventAccessSplitViewViewController")
                self.slideMenuController()?.changeMainViewController(vc, close: true)
            }
        case 4:
            storyboard = UIStoryboard(name: "MenuSection2", bundle: nil)
            if DeviceType(){
                let vc  = storyboard.instantiateViewController(withIdentifier: "EventDatabaseListViewController")  as! EventDatabaseListViewController
                let viewController: UIViewController! = UINavigationController(rootViewController: vc)
                self.slideMenuController()?.changeMainViewController(viewController, close: true)
            }else{
                let vc = storyboard.instantiateViewController(withIdentifier: "EventDatabaseSplitViewController")
                self.slideMenuController()?.changeMainViewController(vc, close: true)
            }
        case 5:
            storyboard = UIStoryboard(name: "MenuSection2", bundle: nil)
            if DeviceType(){
                let vc  = storyboard.instantiateViewController(withIdentifier: "ContactDatabaseListViewController")  as! ContactDatabaseListViewController
                let viewController: UIViewController! = UINavigationController(rootViewController: vc)
                self.slideMenuController()?.changeMainViewController(viewController, close: true)
            }else{
                let vc = storyboard.instantiateViewController(withIdentifier: "ContactDatabaseSplitViewController")
                self.slideMenuController()?.changeMainViewController(vc, close: true)
            }
        case 6:
            storyboard = UIStoryboard(name: "MenuSection3", bundle: nil)
            if DeviceType(){
                let vc  = storyboard.instantiateViewController(withIdentifier: "ClientDatabaseListViewController")  as! ClientDatabaseListViewController
                let viewController: UIViewController! = UINavigationController(rootViewController: vc)
                self.slideMenuController()?.changeMainViewController(viewController, close: true)
            }else{
                let vc = storyboard.instantiateViewController(withIdentifier: "ClientDatabaseSplitViewController")
                self.slideMenuController()?.changeMainViewController(vc, close: true)
            }
        case 7:
            storyboard = UIStoryboard(name: "MenuSection3", bundle: nil)
            if DeviceType(){
                let vc  = storyboard.instantiateViewController(withIdentifier: "EmplyeeDatabaseViewController")  as! EmplyeeDatabaseViewController
                let viewController: UIViewController! = UINavigationController(rootViewController: vc)
                self.slideMenuController()?.changeMainViewController(viewController, close: true)
            }else{
                let vc = storyboard.instantiateViewController(withIdentifier: "EmployeeDatabaseSplitViewController")
                self.slideMenuController()?.changeMainViewController(vc, close: true)
            }
        case 8:
            storyboard = UIStoryboard(name: "MenuSection3", bundle: nil)
            if DeviceType(){
                let vc  = storyboard.instantiateViewController(withIdentifier: "EnterMeetingNotesViewController")  as! EnterMeetingNotesViewController
                let viewController: UIViewController! = UINavigationController(rootViewController: vc)
                self.slideMenuController()?.changeMainViewController(viewController, close: true)
            }else{
                let vc = storyboard.instantiateViewController(withIdentifier: "EnterMeetingNotesSplitViewController")
                self.slideMenuController()?.changeMainViewController(vc, close: true)
            }
        case 9:
            let repo = MediaLinkUserDefaults()
            if repo.isAdmin{
                storyboard = UIStoryboard(name: "MenuSection3", bundle: nil)
                if DeviceType(){
                    let vc  = storyboard.instantiateViewController(withIdentifier: "OnTheGroundViewController")  as! OnTheGroundViewController
                    let viewController: UIViewController! = UINavigationController(rootViewController: vc)
                    self.slideMenuController()?.changeMainViewController(viewController, close: true)
                }else{
                    let vc = storyboard.instantiateViewController(withIdentifier: "OnTheGroundSplitViewController")
                    self.slideMenuController()?.changeMainViewController(vc, close: true)
                }
            }else{
                logoutFromServer()
            }
        case 10:
            logoutFromServer()
        default:
            break
        }
    }
    
    
}
