//
//  MyScheduleFilterTableViewController.swift
//  MediaLink
//
//  Created by Naveen on 3/2/18.
//  Copyright © 2018 Naveen. All rights reserved.
//

import UIKit
protocol SelectedFilterDelegate {//Contains Selected items from the dropdown
    func didSelectedFilterItems(_ objFilter: SelectedFilter)
}
class CommonFilterTableViewController: UITableViewController,SelectedDropdownItemsDelegate,UITextFieldDelegate {
    @IBOutlet weak var lblSelectedClient: UILabel!
    @IBOutlet weak var lblSelectedConference: UILabel!
    @IBOutlet weak var lblSelecteLocation: UILabel!
    @IBOutlet weak var lblSelectedScheduler: UILabel!
    @IBOutlet weak var lblSelectedOutreach: UILabel!
    @IBOutlet weak var inpCompanyName: UITextField!
    @IBOutlet weak var inpDate: UITextField!
    
    var delegateCommonSelectedFilter: SelectedFilterDelegate?
    
    var objSelFilter = SelectedFilter()
    var selectedIndex = 0
    var dateStr = ""
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        
        lblSelectedClient.isHidden = true
        lblSelectedConference.isHidden = true
        lblSelecteLocation.isHidden = true
        lblSelectedScheduler.isHidden = true
        lblSelectedOutreach.isHidden = true
        
        setData()
        
        let datePickerView  = UIDatePicker()
        datePickerView.datePickerMode = UIDatePickerMode.date
        inpDate.inputView = datePickerView
        if !objSelFilter.strDate.isEmpty{
            datePickerView.date = dateFormatBaseFull(objSelFilter.strDate)
        }
        datePickerView.addTarget(self, action: #selector(handleDatePicker), for: UIControlEvents.valueChanged)
    }
    func setData(){
        self.tableView.tableFooterView = UIView()
        if !objSelFilter.strCompany.isEmpty{
            inpCompanyName.text = objSelFilter.strCompany
        }
        if !objSelFilter.strDate.isEmpty{
            dateStr = objSelFilter.strDate
            inpDate.text = DateFormatterDateTimeFullString(objSelFilter.strDate)
        }
        let objAppdelegate = UIApplication.shared.delegate as! AppDelegate
        if objSelFilter.arrClient.count != 0{
            var arrStr = [String]()
            for item in objSelFilter.arrClient{
                for objItem in objAppdelegate.objFilterData.arrClient{
                    if item == objItem.id{
                        arrStr.append(objItem.clientName!)
                    }
                }
            }
            lblSelectedClient.isHidden = false
            lblSelectedClient.text = arrStr.joined(separator: ", ")
            if arrStr.count > 3{
                lblSelectedClient.text = arrStr[0]+", "+arrStr[1]+", "+arrStr[2]+" & \(arrStr.count-3) more"
            }
        }
        if objSelFilter.arrConference.count != 0{
            var arrStr = [String]()
            for item in objSelFilter.arrConference{
                for objItem in objAppdelegate.objFilterData.arrConference{
                    if item == objItem.id{
                        arrStr.append(objItem.eventName!)
                    }
                }
            }
            lblSelectedConference.isHidden = false
            lblSelectedConference.text = arrStr.joined(separator: ", ")
            if arrStr.count > 3{
                lblSelectedConference.text = arrStr[0]+", "+arrStr[1]+", "+arrStr[2]+" & \(arrStr.count-3) more"
            }
        }
        if objSelFilter.arrLocation.count != 0{
            var arrStr = [String]()
            for item in objSelFilter.arrLocation{
                for objItem in objAppdelegate.objFilterData.arrLocation{
                    if item == objItem.id{
                        arrStr.append(objItem.location!)
                    }
                }
            }
            lblSelecteLocation.isHidden = false
            lblSelecteLocation.text = arrStr.joined(separator: ", ")
            if arrStr.count > 3{
                lblSelecteLocation.text = arrStr[0]+", "+arrStr[1]+", "+arrStr[2]+" & \(arrStr.count-3) more"
            }
        }
        if objSelFilter.arrScheduler.count != 0{
            var arrStr = [String]()
            for item in objSelFilter.arrScheduler{
                for objItem in objAppdelegate.objFilterData.arrEmployee{
                    if item == objItem.id{
                        arrStr.append(objItem.firstName!+" "+objItem.lastName!)
                    }
                }
            }
            lblSelectedScheduler.isHidden = false
            lblSelectedScheduler.text = arrStr.joined(separator: ", ")
            if arrStr.count > 3{
                lblSelectedScheduler.text = arrStr[0]+", "+arrStr[1]+", "+arrStr[2]+" & \(arrStr.count-3) more"
            }
        }
        if objSelFilter.arrOutreach.count != 0{
            var arrStr = [String]()
            for item in objSelFilter.arrOutreach{
                for objItem in objAppdelegate.objFilterData.arrEmployee{
                    if item == objItem.id{
                        arrStr.append(objItem.firstName!+" "+objItem.lastName!)
                    }
                }
            }
            lblSelectedOutreach.isHidden = false
            lblSelectedOutreach.text = arrStr.joined(separator: ", ")
            if arrStr.count > 3{
                lblSelectedOutreach.text = arrStr[0]+", "+arrStr[1]+", "+arrStr[2]+" & \(arrStr.count-3) more"
            }
        }
        self.tableView.reloadData()
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return .lightContent
    }
    @objc func handleDatePicker(sender: UIDatePicker) {
        dateStr = String(describing: sender.date)
        inpDate.text = DateFormatterDateTimeFullFormated(sender.date)
    }
    //Delegate Selecteditem
    func didFilterDropdownSelectedItem(_ arrSelectedIndex: [Int],_ arrSelectedStrings:[String]) {
        switch selectedIndex {
        case 1:
            if arrSelectedIndex.count != 0{
                lblSelectedClient.isHidden = false
            }
            objSelFilter.arrClient = arrSelectedIndex
            lblSelectedClient.text = arrSelectedStrings.joined(separator: ", ")
            if arrSelectedStrings.count > 3{
                lblSelectedClient.text = arrSelectedStrings[0]+", "+arrSelectedStrings[1]+", "+arrSelectedStrings[2]+" & \(arrSelectedStrings.count-3) more"
            }
        case 2:
            if arrSelectedIndex.count != 0{
                lblSelectedConference.isHidden = false
            }
            objSelFilter.arrConference = arrSelectedIndex
            lblSelectedConference.text = arrSelectedStrings.joined(separator: ", ")
            if arrSelectedStrings.count > 3{
                lblSelectedConference.text = arrSelectedStrings[0]+", "+arrSelectedStrings[1]+", "+arrSelectedStrings[2]+" & \(arrSelectedStrings.count-3) more"
            }
        case 4:
            if arrSelectedIndex.count != 0{
                lblSelecteLocation.isHidden = false
            }
            objSelFilter.arrLocation = arrSelectedIndex
            lblSelecteLocation.text = arrSelectedStrings.joined(separator: ", ")
            if arrSelectedStrings.count > 3{
                lblSelecteLocation.text = arrSelectedStrings[0]+", "+arrSelectedStrings[1]+", "+arrSelectedStrings[2]+" & \(arrSelectedStrings.count-3) more"
            }
        case 5:
            if arrSelectedIndex.count != 0{
                lblSelectedScheduler.isHidden = false
            }
            objSelFilter.arrScheduler = arrSelectedIndex
            lblSelectedScheduler.text = arrSelectedStrings.joined(separator: ", ")
            if arrSelectedStrings.count > 3{
                lblSelectedScheduler.text = arrSelectedStrings[0]+", "+arrSelectedStrings[1]+", "+arrSelectedStrings[2]+" & \(arrSelectedStrings.count-3) more"
            }
        case 6:
            if arrSelectedIndex.count != 0{
                lblSelectedOutreach.isHidden = false
            }
            objSelFilter.arrOutreach = arrSelectedIndex
            lblSelectedOutreach.text = arrSelectedStrings.joined(separator: ", ")
            if arrSelectedStrings.count > 3{
                lblSelectedOutreach.text = arrSelectedStrings[0]+", "+arrSelectedStrings[1]+", "+arrSelectedStrings[2]+" & \(arrSelectedStrings.count-3) more"
            }
        default:
            break
        }
        
        
        self.tableView.reloadData()
    }
    
    @IBAction func clickedClose(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func clickedClear(_ sender: Any) {
        objSelFilter.clearValues()
        lblSelectedClient.isHidden = true
        lblSelectedConference.isHidden = true
        lblSelecteLocation.isHidden = true
        lblSelectedScheduler.isHidden = true
        lblSelectedOutreach.isHidden = true
        dateStr = ""
        inpDate.text = ""
        inpCompanyName.text = ""
        UIView.animate(withDuration: 0.3) {
            self.tableView.reloadData()
        }
        
    }
    
    @IBAction func clickedApply(_ sender: Any) {
        if !(inpCompanyName.text?.isBlank)!{
            objSelFilter.strCompany = inpCompanyName.text!
        }
        if !dateStr.isBlank{
            objSelFilter.strDate = dateStr
        }
        
        delegateCommonSelectedFilter?.didSelectedFilterItems(objSelFilter)
        self.dismiss(animated: true, completion: nil)
    }
    //textField
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.returnKeyType == .done {
            textField.resignFirstResponder()
        }
        return false
    }
    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.view.endEditing(true)
        switch indexPath.row {
        case 1:
            self.selectedIndex = indexPath.row
            self.performSegue(withIdentifier: "showClientsDropdown", sender: nil)
        case 2:
            self.selectedIndex = indexPath.row
            self.performSegue(withIdentifier: "showConferenceDropdown", sender: nil)
        case 4:
            self.selectedIndex = indexPath.row
            self.performSegue(withIdentifier: "showLocationDropdown", sender: nil)
        case 5,6:
            self.selectedIndex = indexPath.row
            self.performSegue(withIdentifier: "ShowEmployeeDropdown", sender: nil)
        default:
            break
        }
    }

    
    

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showClientsDropdown"{
            let destVc = segue.destination as! ClientsDropdownListViewController
            destVc.delegateSelectedItem = self
            destVc.arrSelectedIndex = objSelFilter.arrClient
            
        }else if segue.identifier == "showConferenceDropdown"{
            let destVc = segue.destination as! ConferenceDropdownListViewController
            destVc.delegateSelectedItem = self
            destVc.arrSelectedIndex = objSelFilter.arrConference
            
        }else if segue.identifier == "showLocationDropdown"{
            let destVc = segue.destination as! LocationDropdownListViewController
            destVc.delegateSelectedItem = self
            destVc.arrSelectedIndex = objSelFilter.arrLocation
            
        }
        else if segue.identifier == "ShowEmployeeDropdown"{
            let destVc = segue.destination as! EmployeeDropDownListViewController
            destVc.delegateSelectedItem = self
            let index = self.tableView.indexPathForSelectedRow
            if index?.row == 5{
                destVc.type = 1
                destVc.arrSelectedIndex = objSelFilter.arrScheduler
            }else{
                destVc.type = 2
                destVc.arrSelectedIndex = objSelFilter.arrOutreach
            }
        }
    }
    

}
