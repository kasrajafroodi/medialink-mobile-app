//
//  StatusDropdownListViewController.swift
//  MediaLink
//
//  Created by Naveen on 3/15/18.
//  Copyright © 2018 Naveen. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper
import SwiftyJSON
import DZNEmptyDataSet

class StatusDropdownListViewController: UIViewController {
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var serchBar: UISearchBar!
    
    var delegateSelectedItem: SelectedDropdownItemsDelegate?
    var arrItems = [PartyStatus]()
    var arrTempItems = [PartyStatus]()
    var arrItemsReqStatus = [RequestedStatus]()
    var arrTempItemsReqStatus = [RequestedStatus]()
    var arrSelectedIndex = [Int]()
    var arrSelectedString = [String]()
    var objFilter = SelectedFilter()
    var isFromOntheGroundEdit = false // true when from on the Ground details
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.tblView.register(UINib(nibName: "CommonCell", bundle: nil), forCellReuseIdentifier: "CommonCell")
        self.tblView.tableFooterView = UIView()
        let obj = UIApplication.shared.delegate as! AppDelegate
        if isFromOntheGroundEdit{
            self.tblView.allowsMultipleSelection = false
            if obj.objFilterData.arrRequestedStatus.count == 0 {
                getRequestedStatusDataFromServer()
            }else{
                arrItemsReqStatus = obj.objFilterData.arrRequestedStatus
                arrTempItemsReqStatus = obj.objFilterData.arrRequestedStatus
                self.tblView.reloadData()
            }
        }else{
            self.tblView.allowsMultipleSelection = true
            if obj.objFilterData.arrPartyStatus.count == 0 {
                getPartyStatusDataFromServer()
            }else{
                arrItems = obj.objFilterData.arrPartyStatus
                arrTempItems = obj.objFilterData.arrPartyStatus

                self.tblView.reloadData()
            }
        }
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //Actions
    @IBAction func clickedDone(_ sender: Any) {
        delegateSelectedItem?.didFilterDropdownSelectedItem(arrSelectedIndex,arrSelectedString)
        self.navigationController!.popViewController(animated: true)
    }
    @IBAction func clickedRefresh(_ sender: Any) {
        arrSelectedIndex = []
        arrSelectedString = []
        if isFromOntheGroundEdit{
            self.arrItemsReqStatus = []
        }else{
            self.arrItems = []
        }        
        self.tblView.reloadData()
        if isFromOntheGroundEdit{
            getRequestedStatusDataFromServer()
        }else{
            getPartyStatusDataFromServer()
        }
        
    }
    //Reponse from Server
    func getPartyStatusDataFromServer(){
        if Reachability.isConnectedToNetwork() {
            let url = MediaLinkUrls.getPartyStatus
            let repo = MediaLinkUserDefaults()
            let params = ["token":repo.tocken!]
            activityIndicatorShow(self)
            Alamofire.request(url, method: .get, parameters: params).validate()
                .responseArray{(response: DataResponse<[PartyStatus]>) in
                    activityIndicatorHide()
                    if response.result.error == nil{
                        if let arrResponce = response.result.value{
                            self.arrItems = arrResponce
                            self.arrTempItems = arrResponce
                            let obj = UIApplication.shared.delegate as! AppDelegate
                            obj.objFilterData.arrPartyStatus = arrResponce
                            self.tblView.reloadData()
                        }
                    }else if response.response?.statusCode == 401{
                        authenticationError(self)
                    }else {
                        self.alert(message: checkNetworkError(response.result.error!))
                    }
            }
        }else{
            self.alert(message: "It seems that there is no Internet connection.")
        }
    }
    func getRequestedStatusDataFromServer(){
        if Reachability.isConnectedToNetwork() {
            let url = MediaLinkUrls.getOutreachRequestStatus
            let repo = MediaLinkUserDefaults()
            let params = ["token":repo.tocken!]
            activityIndicatorShow(self)
            Alamofire.request(url, method: .get, parameters: params).validate()
                .responseArray{(response: DataResponse<[RequestedStatus]>) in
                    activityIndicatorHide()
                    if response.result.error == nil{
                        if let arrResponce = response.result.value{
                            self.arrItemsReqStatus = arrResponce
                            self.arrTempItemsReqStatus = arrResponce
                            let obj = UIApplication.shared.delegate as! AppDelegate
                            obj.objFilterData.arrRequestedStatus = arrResponce
                            self.tblView.reloadData()
                        }
                    }else if response.response?.statusCode == 401{
                        authenticationError(self)
                    }else {
                        self.alert(message: checkNetworkError(response.result.error!))
                    }
            }
        }else{
            self.alert(message: "It seems that there is no Internet connection.")
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
//Serach Bar
extension StatusDropdownListViewController:UISearchBarDelegate{
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if isFromOntheGroundEdit{
            arrItemsReqStatus = arrTempItemsReqStatus.filter{($0.requestStatus?.localizedCaseInsensitiveContains(searchText))!}
            if(searchText.count == 0){
                arrItems=arrTempItems
            }
            self.tblView.reloadData()
        }else{
            arrItems = arrTempItems.filter{($0.partyStatus?.localizedCaseInsensitiveContains(searchText))!}
            if(searchText.count == 0){
                arrItems=arrTempItems
            }
            self.tblView.reloadData()
        }
    }
}

//EmptyDataSet
extension StatusDropdownListViewController: DZNEmptyDataSetSource, DZNEmptyDataSetDelegate{
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        let text="No Records Found"
        let attributes = [NSAttributedStringKey.font: UIFont.boldSystemFont(ofSize: 18.0), NSAttributedStringKey.foregroundColor: UIColor.darkGray]
        return NSAttributedString(string: text, attributes: attributes)
    }
}
extension StatusDropdownListViewController: UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isFromOntheGroundEdit{
            return arrItemsReqStatus.count
        }else{
            return arrItems.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CommonCell", for: indexPath)as! CommonCell
        cell.accessoryType = UITableViewCellAccessoryType.none
        if isFromOntheGroundEdit{
            let objItem = arrItemsReqStatus[indexPath.row]
            if arrSelectedIndex.contains(objItem.id!) {
                if !arrSelectedString.contains(objItem.requestStatus!){
                    arrSelectedString.append(objItem.requestStatus!)
                }                
                cell.accessoryType = .checkmark
                let rowToSelect = IndexPath(row: indexPath.row, section: 0)
                tableView.selectRow(at: rowToSelect, animated: true, scrollPosition: UITableViewScrollPosition.none)
            }
            
            cell.configCell(objItem.requestStatus!)
        }else{
            let objItem = arrItems[indexPath.row]
            if arrSelectedIndex.contains(objItem.id!) {
                if !arrSelectedString.contains(objItem.partyStatus!){
                    arrSelectedString.append(objItem.partyStatus!)
                }
                cell.accessoryType = .checkmark
                let rowToSelect = IndexPath(row: indexPath.row, section: 0);
                tableView.selectRow(at: rowToSelect, animated: true, scrollPosition: UITableViewScrollPosition.none)
            }
            
            cell.configCell(objItem.partyStatus!)
        }
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let cell = tableView.cellForRow(at: indexPath as IndexPath) {
            if cell.accessoryType == .none {
                if isFromOntheGroundEdit{
                    arrSelectedIndex = []
                    arrSelectedString = []
                    arrSelectedIndex.append(arrItemsReqStatus[indexPath.row].id!)
                    arrSelectedString.append(arrItemsReqStatus[indexPath.row].requestStatus!)
                    tableView.reloadData()
                    
                }else{
                    arrSelectedIndex.append(arrItems[indexPath.row].id!)
                    arrSelectedString.append(arrItems[indexPath.row].partyStatus!)
                }
                
                cell.accessoryType = .checkmark
            }
        }
    }
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        if let cell = tableView.cellForRow(at: indexPath as IndexPath) {
            if cell.accessoryType == .checkmark {
                
                let objClient = arrItems[indexPath.row]
                let itemIdex = arrSelectedIndex.index(of: objClient.id!)
                arrSelectedIndex.remove(at: itemIdex!)
                let itemSelIdex = arrSelectedString.index(of: objClient.partyStatus!)
                arrSelectedString.remove(at: itemSelIdex!)
                
                
                cell.accessoryType = .none
            }
        }
    }
}
