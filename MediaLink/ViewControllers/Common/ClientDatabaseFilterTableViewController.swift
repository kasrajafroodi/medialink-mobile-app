//
//  ClientDatabaseFilterTableViewController.swift
//  MediaLink
//
//  Created by Naveen on 3/8/18.
//  Copyright © 2018 Naveen. All rights reserved.
//

import UIKit

class ClientDatabaseFilterTableViewController: UITableViewController,SelectedDropdownItemsDelegate {
    
    @IBOutlet weak var lblSelectedClientTeam: UILabel!
    @IBOutlet weak var lblSelectedCategory: UILabel!
    @IBOutlet weak var lblSelectedClient: UILabel!
    
    var delegateClientDatabaseSelectedFilter: SelectedFilterDelegate?
    
    var objSelFilter = SelectedFilter()
    var selectedIndex = 0
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        self.tableView.tableFooterView = UIView()
        
        setData()
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func setData(){
        lblSelectedClientTeam.isHidden = true
        lblSelectedCategory.isHidden = true
        lblSelectedClient.isHidden = true
        let objAppdelegate = UIApplication.shared.delegate as! AppDelegate
        if objSelFilter.arrClientTeam.count != 0{
            var arrStr = [String]()
            for item in objSelFilter.arrClientTeam{
                for objItem in objAppdelegate.objFilterData.arrEmployee{
                    if item == objItem.id{
                        arrStr.append(objItem.firstName!+" "+objItem.lastName!)
                    }
                }
            }
            lblSelectedClientTeam.isHidden = false
            lblSelectedClientTeam.text = arrStr.joined(separator: ", ")
            if arrStr.count > 3{
                lblSelectedClientTeam.text = arrStr[0]+", "+arrStr[1]+", "+arrStr[2]+" & \(arrStr.count-3) more"
            }
        }
        if objSelFilter.arrCategory.count != 0{
            var arrStr = [String]()
            for item in objSelFilter.arrCategory{
                for objItem in objAppdelegate.objFilterData.arrCategory{
                    if item == objItem.id{
                        arrStr.append(objItem.categoryName!)
                    }
                }
            }
            lblSelectedCategory.isHidden = false
            lblSelectedCategory.text = arrStr.joined(separator: ", ")
            if arrStr.count > 3{
                lblSelectedCategory.text = arrStr[0]+", "+arrStr[1]+", "+arrStr[2]+" & \(arrStr.count-3) more"
            }
        }
        if objSelFilter.arrClient.count != 0{
            var arrStr = [String]()
            for item in objSelFilter.arrClient{
                for objItem in objAppdelegate.objFilterData.arrClient{
                    if item == objItem.id{
                        arrStr.append(objItem.clientName!)
                    }
                }
            }
            lblSelectedClient.isHidden = false
            lblSelectedClient.text = arrStr.joined(separator: ", ")
            if arrStr.count > 3{
                lblSelectedClient.text = arrStr[0]+", "+arrStr[1]+", "+arrStr[2]+" & \(arrStr.count-3) more"
            }
        }
        self.tableView.reloadData()
    }
    //Delegate Selecteditem
    func didFilterDropdownSelectedItem(_ arrSelectedIndex: [Int],_ arrSelectedStrings:[String]) {
        switch selectedIndex {
        case 0://Client Team
            if arrSelectedIndex.count != 0{
                lblSelectedClientTeam.isHidden = false
            }
            objSelFilter.arrClientTeam = arrSelectedIndex
            lblSelectedClientTeam.text = arrSelectedStrings.joined(separator: ", ")
            if arrSelectedStrings.count > 3{
                lblSelectedClientTeam.text = arrSelectedStrings[0]+", "+arrSelectedStrings[1]+", "+arrSelectedStrings[2]+" & \(arrSelectedStrings.count-3) more"
            }
        case 1:
            if arrSelectedIndex.count != 0{
                lblSelectedCategory.isHidden = false
            }
            objSelFilter.arrConference = arrSelectedIndex
            lblSelectedCategory.text = arrSelectedStrings.joined(separator: ", ")
            if arrSelectedStrings.count > 3{
                lblSelectedCategory.text = arrSelectedStrings[0]+", "+arrSelectedStrings[1]+", "+arrSelectedStrings[2]+" & \(arrSelectedStrings.count-3) more"
            }
        case 2:
            if arrSelectedIndex.count != 0{
                lblSelectedClient.isHidden = false
            }
            objSelFilter.arrClient = arrSelectedIndex
            lblSelectedClient.text = arrSelectedStrings.joined(separator: ", ")
            if arrSelectedStrings.count > 3{
                lblSelectedClient.text = arrSelectedStrings[0]+", "+arrSelectedStrings[1]+", "+arrSelectedStrings[2]+" & \(arrSelectedStrings.count-3) more"
            }
        default:
            break
        }
        self.tableView.reloadData()
    }
    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            self.selectedIndex = indexPath.row
            self.performSegue(withIdentifier: "ShowEmployeeDropdown", sender: nil)
        case 1:
            self.selectedIndex = indexPath.row
            self.performSegue(withIdentifier: "showCategoryDropdown", sender: nil)
        case 2:
            self.selectedIndex = indexPath.row
            self.performSegue(withIdentifier: "showClientsDropdown", sender: nil)
        default:
            break
        }
    }
    
    //Mark Actions
    @IBAction func clickedClear(_ sender: Any) {
        objSelFilter.clearValues()
        lblSelectedClientTeam.isHidden = true
        lblSelectedCategory.isHidden = true
        lblSelectedClient.isHidden = true
        UIView.animate(withDuration: 0.3) {
            self.tableView.reloadData()
        }
        
    }
    @IBAction func clickedClose(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func clickedApply(_ sender: Any) {
        delegateClientDatabaseSelectedFilter?.didSelectedFilterItems(objSelFilter)
        self.dismiss(animated: true, completion: nil)
    }

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showClientsDropdown"{
            let destVc = segue.destination as! ClientsDropdownListViewController
            destVc.delegateSelectedItem = self
            destVc.arrSelectedIndex = objSelFilter.arrClient
            
        }else if segue.identifier == "showCategoryDropdown"{
            let destVc = segue.destination as! CategoryDropdownListViewController
            destVc.delegateSelectedItem = self
            destVc.arrSelectedIndex = objSelFilter.arrCategory
            
        }else if segue.identifier == "ShowEmployeeDropdown"{
            let destVc = segue.destination as! EmployeeDropDownListViewController
            destVc.delegateSelectedItem = self
            destVc.type = 3
            destVc.arrSelectedIndex = objSelFilter.arrClientTeam
            
        }
    }
    

}
