//
//  EventDatabaseFilterTableViewController.swift
//  MediaLink
//
//  Created by Naveen on 3/5/18.
//  Copyright © 2018 Naveen. All rights reserved.
//

import UIKit
class EventDatabaseFilterTableViewController: UITableViewController,SelectedDropdownItemsDelegate,UITextFieldDelegate {
    @IBOutlet weak var inpDate: UITextField!
    @IBOutlet weak var inpTime: UITextField!
    @IBOutlet weak var lblSelectedConference: UILabel!
    @IBOutlet weak var inpEventName: UITextField!
    @IBOutlet weak var inpLocation: UITextField!
    
    var delegateEventSelectedFilter: SelectedFilterDelegate?
    
    var objSelFilter = SelectedFilter()
    var selectedIndex = 0
    var dateStr = ""
    var timeStr = ""
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        
        self.tableView.tableFooterView = UIView()
        
        
        setData()
        
        let datePickerView  = UIDatePicker()
        datePickerView.datePickerMode = UIDatePickerMode.date
        inpDate.inputView = datePickerView
        if !objSelFilter.strDate.isEmpty{
            datePickerView.date = dateFormatBaseFull(objSelFilter.strDate)
        }
        datePickerView.addTarget(self, action: #selector(handleDatePicker), for: UIControlEvents.valueChanged)
        
        let timePickerView  = UIDatePicker()
        timePickerView.datePickerMode = UIDatePickerMode.time
        inpTime.inputView = timePickerView
        if !objSelFilter.strTime.isEmpty{
            datePickerView.date = dateFormatBaseFull(objSelFilter.strTime)
        }
        timePickerView.addTarget(self, action: #selector(handleTimePicker), for: UIControlEvents.valueChanged)
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func setData(){
        lblSelectedConference.isHidden = true
        if !objSelFilter.strDate.isEmpty{
            dateStr = objSelFilter.strDate
            inpDate.text = DateFormatterDateTimeFullString(objSelFilter.strDate)
        }
        if !objSelFilter.strTime.isEmpty{
            timeStr = objSelFilter.strTime
            inpTime.text = DateFormatTimeFullString(objSelFilter.strTime)
        }
        if !objSelFilter.strEventName.isEmpty{
            inpEventName.text = objSelFilter.strEventName
        }
        if !objSelFilter.strPartyLocation.isEmpty{
            inpLocation.text = objSelFilter.strPartyLocation
        }
        let objAppdelegate = UIApplication.shared.delegate as! AppDelegate
        if objSelFilter.arrConference.count != 0{
            var arrStr = [String]()
            for item in objSelFilter.arrConference{
                for objItem in objAppdelegate.objFilterData.arrConference{
                    if item == objItem.id{
                        arrStr.append(objItem.eventName!)
                    }
                }
            }
            lblSelectedConference.isHidden = false
            lblSelectedConference.text = arrStr.joined(separator: ", ")
            if arrStr.count > 3{
                lblSelectedConference.text = arrStr[0]+", "+arrStr[1]+", "+arrStr[2]+" & \(arrStr.count-3) more"
            }
        }
        
    }
    //Date Picker
    @objc func handleDatePicker(sender: UIDatePicker) {
        dateStr = String(describing: sender.date)
        inpDate.text = DateFormatterDateTimeFullFormated(sender.date)
    }
    //Time Picker
    @objc func handleTimePicker(sender: UIDatePicker) {
        timeStr = String(describing: sender.date)
        inpTime.text = timeFormatFromDate(sender.date)
    }
    
    //Delegate Selecteditem
    func didFilterDropdownSelectedItem(_ arrSelectedIndex: [Int],_ arrSelectedStrings:[String]) {
        switch selectedIndex {
        case 4:
            if arrSelectedIndex.count != 0{
                lblSelectedConference.isHidden = false
            }
            objSelFilter.arrConference = arrSelectedIndex
            lblSelectedConference.text = arrSelectedStrings.joined(separator: ", ")
            if arrSelectedStrings.count > 3{
                lblSelectedConference.text = arrSelectedStrings[0]+", "+arrSelectedStrings[1]+", "+arrSelectedStrings[2]+" & \(arrSelectedStrings.count-3) more"
            }
        default:
            break
        }
        self.tableView.reloadData()
    }
    
    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 4:
            self.view.endEditing(true)
            self.selectedIndex = indexPath.row
            self.performSegue(withIdentifier: "showConferenceDropdown", sender: nil)
        default:
            break
        }
    }


   
    //Mark Actions
    @IBAction func clickedClear(_ sender: Any) {
        objSelFilter.clearValues()
        lblSelectedConference.isHidden = true
        inpDate.text = ""
        dateStr = ""
        timeStr = ""
        inpTime.text = ""
        inpEventName.text = ""
        inpLocation.text = ""
        UIView.animate(withDuration: 0.3) {
            self.tableView.reloadData()
        }
        
    }
    @IBAction func clickedClose(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func clickedApply(_ sender: Any) {
        if !(inpEventName.text?.isBlank)!{
            objSelFilter.strEventName = inpEventName.text!
        }
        if !dateStr.isBlank{
            objSelFilter.strDate = dateStr
        }
        if !timeStr.isBlank{
            objSelFilter.strTime = timeStr
        }
        if !(inpLocation.text?.isBlank)!{
            objSelFilter.strPartyLocation = inpLocation.text!
        }
        delegateEventSelectedFilter?.didSelectedFilterItems(objSelFilter)
        self.dismiss(animated: true, completion: nil)
    }
    //textField
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.returnKeyType == .done {
            textField.resignFirstResponder()
        }
        return false
    }
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showConferenceDropdown"{
            let destVc = segue.destination as! ConferenceDropdownListViewController
            destVc.delegateSelectedItem = self
            destVc.arrSelectedIndex = objSelFilter.arrConference
            
        }
    }
    

}
