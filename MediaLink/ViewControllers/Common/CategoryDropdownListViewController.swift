//
//  CategoryDropdownListViewController.swift
//  MediaLink
//
//  Created by Naveen on 3/20/18.
//  Copyright © 2018 Naveen. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper
import SwiftyJSON
import DZNEmptyDataSet

class CategoryDropdownListViewController: UIViewController {
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var serchBar: UISearchBar!
    
    var delegateSelectedItem: SelectedDropdownItemsDelegate?
    var arrItems = [Categories]()
    var arrTempItems = [Categories]()
    var arrSelectedIndex = [Int]()
    var arrSelectedString = [String]()
    var objFilter = SelectedFilter()
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.tblView.register(UINib(nibName: "CommonCell", bundle: nil), forCellReuseIdentifier: "CommonCell")
        self.tblView.tableFooterView = UIView()
        let obj = UIApplication.shared.delegate as! AppDelegate
        if obj.objFilterData.arrCategory.count == 0 {
            getCategoryDataFromServer()
        }else{
            arrItems = obj.objFilterData.arrCategory
            arrTempItems = obj.objFilterData.arrCategory
            self.tblView.reloadData()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //Actions
    @IBAction func clickedDone(_ sender: Any) {
        delegateSelectedItem?.didFilterDropdownSelectedItem(arrSelectedIndex,arrSelectedString)
        self.navigationController!.popViewController(animated: true)
    }
    @IBAction func clickedRefresh(_ sender: Any) {
        arrSelectedIndex = []
        arrSelectedString = []
        self.arrItems = []
        self.tblView.reloadData()
        getCategoryDataFromServer()
    }
    //Reponse from Server
    func getCategoryDataFromServer(){
        if Reachability.isConnectedToNetwork() {
            let url = MediaLinkUrls.getCategories
            let repo = MediaLinkUserDefaults()
            let params = ["token":repo.tocken!]
            activityIndicatorShow(self)
            Alamofire.request(url, method: .get, parameters: params).validate()
                .responseArray{(response: DataResponse<[Categories]>) in
                    activityIndicatorHide()
                    if response.result.error == nil{
                        if let arrResponce = response.result.value{
                            self.arrItems = arrResponce
                            self.arrTempItems = arrResponce
                            let obj = UIApplication.shared.delegate as! AppDelegate
                            obj.objFilterData.arrCategory = arrResponce
                            self.tblView.reloadData()
                        }
                    }else if response.response?.statusCode == 401{
                        authenticationError(self)
                    }else {
                        self.alert(message: checkNetworkError(response.result.error!))
                    }
            }
        }else{
            self.alert(message: "It seems that there is no Internet connection.")
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
//Serach Bar
extension CategoryDropdownListViewController:UISearchBarDelegate{
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        arrItems = arrTempItems.filter{($0.categoryName?.localizedCaseInsensitiveContains(searchText))!}
        if(searchText.count == 0){
            arrItems=arrTempItems
        }
        self.tblView.reloadData()
    }
}

//EmptyDataSet
extension CategoryDropdownListViewController: DZNEmptyDataSetSource, DZNEmptyDataSetDelegate{
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        let text="No Records Found"
        let attributes = [NSAttributedStringKey.font: UIFont.boldSystemFont(ofSize: 18.0), NSAttributedStringKey.foregroundColor: UIColor.darkGray]
        return NSAttributedString(string: text, attributes: attributes)
    }
}
extension CategoryDropdownListViewController: UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CommonCell", for: indexPath)as! CommonCell
        cell.accessoryType = UITableViewCellAccessoryType.none
        let objItem = arrItems[indexPath.row]
        if arrSelectedIndex.contains(objItem.id!) {
            if !arrSelectedString.contains(objItem.categoryName!){
                arrSelectedString.append(objItem.categoryName!)
            }
            cell.accessoryType = .checkmark
            let rowToSelect = IndexPath(row: indexPath.row, section: 0);
            tableView.selectRow(at: rowToSelect, animated: true, scrollPosition: UITableViewScrollPosition.none)
        }
        
        cell.configCell(objItem.categoryName!)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let cell = tableView.cellForRow(at: indexPath as IndexPath) {
            if cell.accessoryType == .none {
                arrSelectedIndex.append(arrItems[indexPath.row].id!)
                arrSelectedString.append(arrItems[indexPath.row].categoryName!)
                cell.accessoryType = .checkmark
            }
        }
    }
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        if let cell = tableView.cellForRow(at: indexPath as IndexPath) {
            if cell.accessoryType == .checkmark {
                let objClient = arrItems[indexPath.row]
                let itemIdex = arrSelectedIndex.index(of: objClient.id!)
                arrSelectedIndex.remove(at: itemIdex!)
                let itemSelIdex = arrSelectedString.index(of: objClient.categoryName!)
                arrSelectedString.remove(at: itemSelIdex!)
                cell.accessoryType = .none
            }
        }
    }
}
