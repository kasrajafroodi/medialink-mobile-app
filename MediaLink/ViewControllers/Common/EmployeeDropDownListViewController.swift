//
//  EmployeeDropDownListViewController.swift
//  MediaLink
//
//  Created by Naveen on 3/14/18.
//  Copyright © 2018 Naveen. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper
import SwiftyJSON
import DZNEmptyDataSet
protocol EmployeeDropDownSelectionDelegate {
    func selectedEmployee(_ arrObjEmp:[Employee])
}
class EmployeeDropDownListViewController: UIViewController {
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var serchBar: UISearchBar!
    
    var delegateSelectedItem: SelectedDropdownItemsDelegate?
    var delegateEmployeeSelection: EmployeeDropDownSelectionDelegate?
    
    var arrItems = [Employee]()
    var arrTempItems = [Employee]()
    var arrSelectedIndex = [Int]()
    var arrSelectedString = [String]()
    var objFilter = SelectedFilter()
    
    var arrSelectEmplyee = [Employee]()
    
    var type = 0
    var isFromOntheGroundEdit = false // true when from on the Ground details
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.tblView.register(UINib(nibName: "CommonCell", bundle: nil), forCellReuseIdentifier: "CommonCell")
        self.tblView.tableFooterView = UIView()
        let obj = UIApplication.shared.delegate as! AppDelegate
        switch type {
        case 1,5:
            self.title = "Scheduler"
        case 2:
            self.title = "Outreach Lead"
        case 3:
            self.title = "Client Team"
        case 4:
            self.title = "Meetings Cover"

        default:
            break
        }
        if obj.objFilterData.arrEmployee.count == 0 {
            getEmployeeDataFromServer()
        }else{
            arrItems = obj.objFilterData.arrEmployee
            arrTempItems = obj.objFilterData.arrEmployee
            self.tblView.reloadData()
        }
        if isFromOntheGroundEdit{
            self.tblView.allowsMultipleSelection = false
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //Actions
    @IBAction func clickedDone(_ sender: Any) {
        if type == 5{
            delegateEmployeeSelection?.selectedEmployee(arrSelectEmplyee)
        }else{
            delegateSelectedItem?.didFilterDropdownSelectedItem(arrSelectedIndex,arrSelectedString)
        }
        
        self.navigationController!.popViewController(animated: true)
    }
    @IBAction func clickedRefresh(_ sender: Any) {
        arrSelectedIndex = []
        arrSelectedString = []
        self.arrItems = []
        self.tblView.reloadData()
        getEmployeeDataFromServer()
    }
    
    
    //Reponse from Server
    func getEmployeeDataFromServer(){
        if Reachability.isConnectedToNetwork() {
            let url = MediaLinkUrls.getEmployees
            let repo = MediaLinkUserDefaults()
            let params = ["token":repo.tocken!]
            activityIndicatorShow(self)
            Alamofire.request(url, method: .get, parameters: params).validate()
                .responseArray{(response: DataResponse<[Employee]>) in
                    activityIndicatorHide()
                    if response.result.error == nil{
                        if let arrResponce = response.result.value{
                            self.arrItems = arrResponce
                            self.arrTempItems = arrResponce
                            let obj = UIApplication.shared.delegate as! AppDelegate
                            obj.objFilterData.arrEmployee = arrResponce
                            self.tblView.reloadData()
                        }
                    }else if response.response?.statusCode == 401{
                        authenticationError(self)
                    }else {
                        self.alert(message: checkNetworkError(response.result.error!))
                    }
            }
        }else{
            self.alert(message: "It seems that there is no Internet connection.")
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
//Serach Bar
extension EmployeeDropDownListViewController:UISearchBarDelegate{
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        arrItems = arrTempItems.filter{($0.firstName?.localizedCaseInsensitiveContains(searchText))! || ($0.lastName!.localizedCaseInsensitiveContains(searchText))}
        if(searchText.count == 0){
            arrItems=arrTempItems
        }
        self.tblView.reloadData()
    }
}
//EmptyDataSet
extension EmployeeDropDownListViewController: DZNEmptyDataSetSource, DZNEmptyDataSetDelegate{
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        let text="No Records Found"
        let attributes = [NSAttributedStringKey.font: UIFont.boldSystemFont(ofSize: 18.0), NSAttributedStringKey.foregroundColor: UIColor.darkGray]
        return NSAttributedString(string: text, attributes: attributes)
    }
}
extension EmployeeDropDownListViewController: UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CommonCell", for: indexPath)as! CommonCell
        cell.accessoryType = UITableViewCellAccessoryType.none
        let objItem = arrItems[indexPath.row]
        if type == 5{
            var i = 0
            while arrSelectEmplyee.count > i{
                if objItem.id == arrSelectEmplyee[i].id{
                    cell.accessoryType = .checkmark
                    let rowToSelect = IndexPath(row: indexPath.row, section: 0);
                    tableView.selectRow(at: rowToSelect, animated: true, scrollPosition: UITableViewScrollPosition.none)
                }
                i=i+1
            }
        }else{
            if arrSelectedIndex.contains(objItem.id!) {
                if !arrSelectedString.contains(objItem.firstName!+" "+objItem.lastName!){
                    arrSelectedString.append(objItem.firstName!+" "+objItem.lastName!)
                }
                cell.accessoryType = .checkmark
                let rowToSelect = IndexPath(row: indexPath.row, section: 0);
                tableView.selectRow(at: rowToSelect, animated: true, scrollPosition: UITableViewScrollPosition.none)
            }
        }
        
        
        cell.configCell(objItem.firstName!+" "+objItem.lastName!)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let cell = tableView.cellForRow(at: indexPath as IndexPath) {
            if cell.accessoryType == .none {
                if type == 5{
                    arrSelectEmplyee.append(arrItems[indexPath.row])
                }else{
                    if isFromOntheGroundEdit{
                        arrSelectedIndex = []
                        arrSelectedString = []
                        arrSelectedIndex.append(arrItems[indexPath.row].id!)
                        arrSelectedString.append(arrItems[indexPath.row].firstName!+" "+arrItems[indexPath.row].lastName!)
                        tableView.reloadData()
                    }else{
                        arrSelectedIndex.append(arrItems[indexPath.row].id!)
                        arrSelectedString.append(arrItems[indexPath.row].firstName!+" "+arrItems[indexPath.row].lastName!)
                    }
                    
                }
                cell.accessoryType = .checkmark
            }
        }
    }
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        if let cell = tableView.cellForRow(at: indexPath as IndexPath) {
            if cell.accessoryType == .checkmark {
                let objClient = arrItems[indexPath.row]
                if type == 5{
                    var i = 0
                    while arrSelectEmplyee.count > i{
                        if objClient.id == arrSelectEmplyee[i].id{
                            arrSelectEmplyee.remove(at: i)
                        }
                        i=i+1
                    }
                }else{
                    let itemIdex = arrSelectedIndex.index(of: objClient.id!)
                    arrSelectedIndex.remove(at: itemIdex!)
                    let itemSelIdex = arrSelectedString.index(of: objClient.firstName!+" "+objClient.lastName!)
                    arrSelectedString.remove(at: itemSelIdex!)
                }
                cell.accessoryType = .none
            }
        }
    }
}
