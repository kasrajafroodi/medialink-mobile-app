//
//  ClientsDropdownListViewController.swift
//  MediaLink
//
//  Created by Naveen on 3/14/18.
//  Copyright © 2018 Naveen. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper
import SwiftyJSON
import DZNEmptyDataSet
protocol SelectedDropdownItemsDelegate {
    func didFilterDropdownSelectedItem(_ arrSelectedIndex: [Int],_ arrSelectedStrings:[String])
}
class ClientsDropdownListViewController: UIViewController {

    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var serchBar: UISearchBar!
    
    var delegateSelectedItem: SelectedDropdownItemsDelegate?
    
    var arrClients = [Client]()
    var arrTempClients = [Client]()
    var arrSelectedIndex = [Int]()
    var arrSelectedString = [String]()
    var objFilter = SelectedFilter()
    var isFromSingleSelection = false // true when from need to select client
    var isMeetingHistory = false // true when from need to select client
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.tblView.register(UINib(nibName: "CommonCell", bundle: nil), forCellReuseIdentifier: "CommonCell")
        self.tblView.tableFooterView = UIView()
        let obj = UIApplication.shared.delegate as! AppDelegate
        if obj.objFilterData.arrClient.count == 0 {
            getClientsDataFromServer()
        }else{
            arrClients = obj.objFilterData.arrClient
            arrTempClients = obj.objFilterData.arrClient
            self.tblView.reloadData()
        }
        if isFromSingleSelection{
            self.tblView.allowsMultipleSelection = false
        }
        if isMeetingHistory{
            self.title = "Meeting History"
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //Actions
    @IBAction func clickedDone(_ sender: Any) {
         delegateSelectedItem?.didFilterDropdownSelectedItem(arrSelectedIndex,arrSelectedString)
        self.navigationController!.popViewController(animated: true)
    }
    @IBAction func clickedRefresh(_ sender: Any) {
        arrSelectedString = []
        arrSelectedIndex = []
        self.arrClients = []
        self.tblView.reloadData()
        getClientsDataFromServer()
    }
    
    
    //Reponse from Server
    func getClientsDataFromServer(){
        if Reachability.isConnectedToNetwork() {
            let url = MediaLinkUrls.activeClients
            let repo = MediaLinkUserDefaults()
            let params = ["token":repo.tocken!]
            
            activityIndicatorShow(self)
            Alamofire.request(url, method: .get, parameters: params).validate()
                .responseArray{(response: DataResponse<[Client]>) in
                    activityIndicatorHide()
                    if response.result.error == nil{
                        if let arrResponce = response.result.value{
                            self.arrClients = arrResponce
                            self.arrTempClients = arrResponce
                            let obj = UIApplication.shared.delegate as! AppDelegate
                            obj.objFilterData.arrClient = arrResponce
                            self.tblView.reloadData()
                        }
                    }else if response.response?.statusCode == 401{
                        authenticationError(self)
                    }else {
                        self.alert(message: checkNetworkError(response.result.error!))
                    }
            }
        }else{
            self.alert(message: "It seems that there is no Internet connection.")
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
//Serach Bar
extension ClientsDropdownListViewController:UISearchBarDelegate{
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        arrClients = arrTempClients.filter{($0.clientName?.localizedCaseInsensitiveContains(searchText))!}
        if(searchText.count == 0){
            arrClients=arrTempClients
        }
        self.tblView.reloadData()
    }
}

//EmptyDataSet
extension ClientsDropdownListViewController: DZNEmptyDataSetSource, DZNEmptyDataSetDelegate{
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        let text="No Records Found"
        let attributes = [NSAttributedStringKey.font: UIFont.boldSystemFont(ofSize: 18.0), NSAttributedStringKey.foregroundColor: UIColor.darkGray]
        return NSAttributedString(string: text, attributes: attributes)
    }
}
extension ClientsDropdownListViewController: UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrClients.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CommonCell", for: indexPath)as! CommonCell
        cell.accessoryType = UITableViewCellAccessoryType.none
        let objClient = arrClients[indexPath.row]
        if arrSelectedIndex.contains(objClient.id!) {
            if !arrSelectedString.contains(objClient.clientName!){
                arrSelectedString.append(objClient.clientName!)
            }
            cell.accessoryType = .checkmark
            let rowToSelect = IndexPath(row: indexPath.row, section: 0);
            tableView.selectRow(at: rowToSelect, animated: true, scrollPosition: UITableViewScrollPosition.none)
        }
        
        cell.configCell(objClient.clientName!)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let cell = tableView.cellForRow(at: indexPath as IndexPath) {
            if cell.accessoryType == .none {
                if isFromSingleSelection{
                    arrSelectedIndex = []
                    arrSelectedString = []
                    arrSelectedIndex.append(arrClients[indexPath.row].id!)
                    arrSelectedString.append(arrClients[indexPath.row].clientName!)
                    cell.accessoryType = .checkmark
                    tableView.reloadData()
                }else{
                    arrSelectedIndex.append(arrClients[indexPath.row].id!)
                    arrSelectedString.append(arrClients[indexPath.row].clientName!)
                    cell.accessoryType = .checkmark
                }
            }
        }
    }
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        if let cell = tableView.cellForRow(at: indexPath as IndexPath) {
            if cell.accessoryType == .checkmark {
                let objClient = arrClients[indexPath.row]
                let itemIdex = arrSelectedIndex.index(of: objClient.id!)
                arrSelectedIndex.remove(at: itemIdex!)
                let itemSelIdex = arrSelectedString.index(of: objClient.clientName!)
                arrSelectedString.remove(at: itemSelIdex!)
                cell.accessoryType = .none
            }
        }
    }
}

