//
//  MyClientsEventAccessFilterTableViewController.swift
//  MediaLink
//
//  Created by Naveen on 3/3/18.
//  Copyright © 2018 Naveen. All rights reserved.
//

import UIKit

class MyClientsEventAccessFilterTableViewController: UITableViewController,SelectedDropdownItemsDelegate,UITextFieldDelegate {
    @IBOutlet weak var inpCompany: UITextField!
    @IBOutlet weak var inpDate: UITextField!
    @IBOutlet weak var inpTitle: UITextField!
    @IBOutlet weak var lblClient: UILabel!
    @IBOutlet weak var lblConference: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    
    var delegateEventAccessFilter: SelectedFilterDelegate?
    
    var objSelFilter = SelectedFilter()
    var selectedIndex = 0
    var dateStr = ""
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        
        
        lblClient.isHidden = true
        lblConference.isHidden = true
        lblStatus.isHidden = true
        
        setData()
        
        let datePickerView  = UIDatePicker()
        datePickerView.datePickerMode = UIDatePickerMode.date
        inpDate.inputView = datePickerView
        if !objSelFilter.strDate.isEmpty{
            datePickerView.date = dateFormatBaseFull(objSelFilter.strDate)
        }
        datePickerView.addTarget(self, action: #selector(handleDatePicker), for: UIControlEvents.valueChanged)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func setData(){
        self.tableView.tableFooterView = UIView()
        if !objSelFilter.strCompany.isEmpty{
            inpCompany.text = objSelFilter.strCompany
        }
        if !objSelFilter.strEventName.isEmpty{
            inpTitle.text = objSelFilter.strEventName
        }
        if !objSelFilter.strDate.isEmpty{
            dateStr = objSelFilter.strDate
            inpDate.text = DateFormatterDateTimeFullString(objSelFilter.strDate)
        }
        let objAppdelegate = UIApplication.shared.delegate as! AppDelegate
        if objSelFilter.arrClient.count != 0{
            var arrStr = [String]()
            for item in objSelFilter.arrClient{
                for objItem in objAppdelegate.objFilterData.arrClient{
                    if item == objItem.id{
                        arrStr.append(objItem.clientName!)
                    }
                }
            }
            lblClient.isHidden = false
            lblClient.text = arrStr.joined(separator: ", ")
            if arrStr.count > 3{
                lblClient.text = arrStr[0]+", "+arrStr[1]+", "+arrStr[2]+" & \(arrStr.count-3) more"
            }
        }
        if objSelFilter.arrConference.count != 0{
            var arrStr = [String]()
            for item in objSelFilter.arrConference{
                for objItem in objAppdelegate.objFilterData.arrConference{
                    if item == objItem.id{
                        arrStr.append(objItem.eventName!)
                    }
                }
            }
            lblConference.isHidden = false
            lblConference.text = arrStr.joined(separator: ", ")
            if arrStr.count > 3{
                lblConference.text = arrStr[0]+", "+arrStr[1]+", "+arrStr[2]+" & \(arrStr.count-3) more"
            }
        }
        if objSelFilter.arrPartyStatus.count != 0{
            var arrStr = [String]()
            for item in objSelFilter.arrPartyStatus{
                for objItem in objAppdelegate.objFilterData.arrPartyStatus{
                    if item == objItem.id{
                        arrStr.append(objItem.partyStatus!)
                    }
                }
            }
            lblStatus.isHidden = false
            lblStatus.text = arrStr.joined(separator: ", ")
            if arrStr.count > 3{
                lblStatus.text = arrStr[0]+", "+arrStr[1]+", "+arrStr[2]+" & \(arrStr.count-3) more"
            }
        }
        
        
    }
    //Date Picker
    @objc func handleDatePicker(sender: UIDatePicker) {
        dateStr = String(describing: sender.date)
        inpDate.text = DateFormatterDateTimeFullFormated(sender.date)
    }
    //Delegate function Dropdown
    func didFilterDropdownSelectedItem(_ arrSelectedIndex: [Int],_ arrSelectedStrings:[String]) {
        switch selectedIndex {
        case 1:
            if arrSelectedIndex.count != 0{
                lblClient.isHidden = false
            }
            objSelFilter.arrClient = arrSelectedIndex
            lblClient.text = arrSelectedStrings.joined(separator: ", ")
            if arrSelectedStrings.count > 3{
                lblClient.text = arrSelectedStrings[0]+", "+arrSelectedStrings[1]+", "+arrSelectedStrings[2]+" & \(arrSelectedStrings.count-3) more"
            }
        case 2:
            if arrSelectedIndex.count != 0{
                lblConference.isHidden = false
            }
            objSelFilter.arrConference = arrSelectedIndex
            lblConference.text = arrSelectedStrings.joined(separator: ", ")
            if arrSelectedStrings.count > 3{
                lblConference.text = arrSelectedStrings[0]+", "+arrSelectedStrings[1]+", "+arrSelectedStrings[2]+" & \(arrSelectedStrings.count-3) more"
            }
        case 5:
            if arrSelectedIndex.count != 0{
                lblStatus.isHidden = false
            }
            objSelFilter.arrPartyStatus = arrSelectedIndex
            lblStatus.text = arrSelectedStrings.joined(separator: ", ")
            if arrSelectedStrings.count > 3{
                lblStatus.text = arrSelectedStrings[0]+", "+arrSelectedStrings[1]+", "+arrSelectedStrings[2]+" & \(arrSelectedStrings.count-3) more"
            }
        
        default:
            break
        }
        
        
        self.tableView.reloadData()
    }
    //textField
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.returnKeyType == .done {
            textField.resignFirstResponder()
        }
        return false
    }
    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 1:
            self.selectedIndex = indexPath.row
            self.performSegue(withIdentifier: "showClientsDropdown", sender: nil)
        case 2:
            self.selectedIndex = indexPath.row
            self.performSegue(withIdentifier: "showConferenceDropdown", sender: nil)
        case 5:
            self.selectedIndex = indexPath.row
            self.performSegue(withIdentifier: "showStatusDropdown", sender: nil)
        default:
            break
        }
    }

   
    //Mark Actions
    @IBAction func clickedClear(_ sender: Any) {
        objSelFilter.clearValues()
        lblClient.isHidden = true
        lblConference.isHidden = true
        lblStatus.isHidden = true
        inpDate.text = ""
        dateStr = ""
        inpCompany.text = ""
        inpTitle.text = ""
        UIView.animate(withDuration: 0.3) {
            self.tableView.reloadData()
        }
        
    }
    @IBAction func clickedClose(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func clickedApply(_ sender: Any) {
        if !(inpCompany.text?.isBlank)!{
            objSelFilter.strCompany = inpCompany.text!
        }
        if !(inpTitle.text?.isBlank)!{
            objSelFilter.strEventName = inpTitle.text!
        }
        if !dateStr.isBlank{
            objSelFilter.strDate = dateStr
        }
        delegateEventAccessFilter?.didSelectedFilterItems(objSelFilter)
        self.dismiss(animated: true, completion: nil)
    }
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "showClientsDropdown"{
            let destVc = segue.destination as! ClientsDropdownListViewController
            destVc.delegateSelectedItem = self
            destVc.arrSelectedIndex = objSelFilter.arrClient
            
        }else if segue.identifier == "showConferenceDropdown"{
            let destVc = segue.destination as! ConferenceDropdownListViewController
            destVc.delegateSelectedItem = self
            destVc.arrSelectedIndex = objSelFilter.arrConference
            
        }else if segue.identifier == "showStatusDropdown"{
            let destVc = segue.destination as! StatusDropdownListViewController
            destVc.delegateSelectedItem = self
            destVc.arrSelectedIndex = objSelFilter.arrPartyStatus
        }
    }
    

}
