//
//  ContactDatabaseFilterTableViewController.swift
//  MediaLink
//
//  Created by Naveen on 3/5/18.
//  Copyright © 2018 Naveen. All rights reserved.
//

import UIKit
import DLRadioButton

class ContactDatabaseFilterTableViewController: UITableViewController,SelectedDropdownItemsDelegate,UITextFieldDelegate {

    
    @IBOutlet weak var lblSelectedCategory: UILabel!
    @IBOutlet weak var lblConferenceHistory: UILabel!
    @IBOutlet weak var lblSelectedMeetingHistory: UILabel!
    @IBOutlet weak var lblSelectedPartyHistory: UILabel!
    @IBOutlet weak var lblSelectedOutreachLead: UILabel!
    @IBOutlet weak var lblSelectedScheduler: UILabel!
    @IBOutlet weak var inpCompany: UITextField!
    @IBOutlet weak var radioBtnIsClient: DLRadioButton!
    
    var delegateContactSelectedFilter: SelectedFilterDelegate?
    
    var objSelFilter = SelectedFilter()
    var selectedIndex = 0
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        self.tableView.tableFooterView = UIView()
        lblSelectedCategory.isHidden = true
        lblConferenceHistory.isHidden = true
        lblSelectedMeetingHistory.isHidden = true
        lblSelectedPartyHistory.isHidden = true
        lblSelectedOutreachLead.isHidden = true
        lblSelectedScheduler.isHidden = true
        setData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func setData(){
        self.tableView.tableFooterView = UIView()
        if !objSelFilter.strCompany.isEmpty{
            inpCompany.text = objSelFilter.strCompany
        }
        if objSelFilter.isClient != 0{
            radioBtnIsClient.isSelected = true
        }
        let objAppdelegate = UIApplication.shared.delegate as! AppDelegate
        if objSelFilter.arrCategory.count != 0{
            var arrStr = [String]()
            for item in objSelFilter.arrCategory{
                for objItem in objAppdelegate.objFilterData.arrCategory{
                    if item == objItem.id{
                        arrStr.append(objItem.categoryName!)
                    }
                }
            }
            lblSelectedCategory.isHidden = false
            lblSelectedCategory.text = arrStr.joined(separator: ", ")
            if arrStr.count > 3{
                lblSelectedCategory.text = arrStr[0]+", "+arrStr[1]+", "+arrStr[2]+" & \(arrStr.count-3) more"
            }
        }
        if objSelFilter.arrConference.count != 0{
            var arrStr = [String]()
            for item in objSelFilter.arrConference{
                for objItem in objAppdelegate.objFilterData.arrConference{
                    if item == objItem.id{
                        arrStr.append(objItem.eventName!)
                    }
                }
            }
            lblConferenceHistory.isHidden = false
            lblConferenceHistory.text = arrStr.joined(separator: ", ")
            if arrStr.count > 3{
                lblConferenceHistory.text = arrStr[0]+", "+arrStr[1]+", "+arrStr[2]+" & \(arrStr.count-3) more"
            }
        }
        if objSelFilter.arrMeeting.count != 0{
            var arrStr = [String]()
            for item in objSelFilter.arrMeeting{
                for objItem in objAppdelegate.objFilterData.arrClient{
                    if item == objItem.id{
                        arrStr.append(objItem.clientName!)
                    }
                }
            }
            lblSelectedMeetingHistory.isHidden = false
            lblSelectedMeetingHistory.text = arrStr.joined(separator: ", ")
            if arrStr.count > 3{
                lblSelectedMeetingHistory.text = arrStr[0]+", "+arrStr[1]+", "+arrStr[2]+" & \(arrStr.count-3) more"
            }
        }
        if objSelFilter.arrPartyHistory.count != 0{
            if objSelFilter.arrPartyHistory.count != 0{
                for objItem in objAppdelegate.objFilterData.arrPartHistory{
                    if objSelFilter.arrPartyHistory[0] == objItem.id{
                        lblSelectedPartyHistory.isHidden = false
                        lblSelectedPartyHistory.text = objItem.partyName!
                    }
                }
            }
        }
        if objSelFilter.arrScheduler.count != 0{
            var arrStr = [String]()
            for item in objSelFilter.arrScheduler{
                for objItem in objAppdelegate.objFilterData.arrEmployee{
                    if item == objItem.id{
                        arrStr.append(objItem.firstName!+" "+objItem.lastName!)
                    }
                }
            }
            lblSelectedScheduler.isHidden = false
            lblSelectedScheduler.text = arrStr.joined(separator: ", ")
            if arrStr.count > 3{
                lblSelectedScheduler.text = arrStr[0]+", "+arrStr[1]+", "+arrStr[2]+" & \(arrStr.count-3) more"
            }
        }
        if objSelFilter.arrOutreach.count != 0{
            var arrStr = [String]()
            for item in objSelFilter.arrOutreach{
                for objItem in objAppdelegate.objFilterData.arrEmployee{
                    if item == objItem.id{
                        arrStr.append(objItem.firstName!+" "+objItem.lastName!)
                    }
                }
            }
            lblSelectedOutreachLead.isHidden = false
            lblSelectedOutreachLead.text = arrStr.joined(separator: ", ")
            if arrStr.count > 3{
                lblSelectedOutreachLead.text = arrStr[0]+", "+arrStr[1]+", "+arrStr[2]+" & \(arrStr.count-3) more"
            }
        }
        self.tableView.reloadData()
    }
    //Delegate Selecteditem
    func didFilterDropdownSelectedItem(_ arrSelectedIndex: [Int],_ arrSelectedStrings:[String]) {
        switch selectedIndex {
        case 0:
            if arrSelectedIndex.count != 0{
                lblSelectedCategory.isHidden = false
            }
            objSelFilter.arrCategory = arrSelectedIndex
            lblSelectedCategory.text = arrSelectedStrings.joined(separator: ", ")
            if arrSelectedStrings.count > 3{
                lblSelectedCategory.text = arrSelectedStrings[0]+", "+arrSelectedStrings[1]+", "+arrSelectedStrings[2]+" & \(arrSelectedStrings.count-3) more"
            }
        case 1:
            
            if arrSelectedIndex.count != 0{
                lblConferenceHistory.isHidden = false
            }
            objSelFilter.arrConference = arrSelectedIndex
            lblConferenceHistory.text = arrSelectedStrings.joined(separator: ", ")
            if arrSelectedStrings.count > 3{
                lblConferenceHistory.text = arrSelectedStrings[0]+", "+arrSelectedStrings[1]+", "+arrSelectedStrings[2]+" & \(arrSelectedStrings.count-3) more"
            }
            
        case 2:
            if arrSelectedIndex.count != 0{
                lblSelectedMeetingHistory.isHidden = false
            }
            objSelFilter.arrMeeting = arrSelectedIndex
            lblSelectedMeetingHistory.text = arrSelectedStrings.joined(separator: ", ")
            if arrSelectedStrings.count > 3{
                lblSelectedMeetingHistory.text = arrSelectedStrings[0]+", "+arrSelectedStrings[1]+", "+arrSelectedStrings[2]+" & \(arrSelectedStrings.count-3) more"
            }
        case 3:
            if arrSelectedIndex.count != 0{
                lblSelectedPartyHistory.isHidden = false
            }
            if arrSelectedStrings.count != 0{
                objSelFilter.arrPartyHistory = arrSelectedIndex
                lblSelectedPartyHistory.text = arrSelectedStrings[0]
            }
        case 4:
            if arrSelectedIndex.count != 0{
                lblSelectedOutreachLead.isHidden = false
            }
            objSelFilter.arrOutreach = arrSelectedIndex
            lblSelectedOutreachLead.text = arrSelectedStrings.joined(separator: ", ")
            if arrSelectedStrings.count > 3{
                lblSelectedOutreachLead.text = arrSelectedStrings[0]+", "+arrSelectedStrings[1]+", "+arrSelectedStrings[2]+" & \(arrSelectedStrings.count-3) more"
            }
        case 5:
            if arrSelectedIndex.count != 0{
                lblSelectedScheduler.isHidden = false
            }
            objSelFilter.arrScheduler = arrSelectedIndex
            lblSelectedScheduler.text = arrSelectedStrings.joined(separator: ", ")
            if arrSelectedStrings.count > 3{
                lblSelectedScheduler.text = arrSelectedStrings[0]+", "+arrSelectedStrings[1]+", "+arrSelectedStrings[2]+" & \(arrSelectedStrings.count-3) more"
            }
            
        default:
            break
        }
        self.tableView.reloadData()
    }
    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.view.endEditing(true)
        switch indexPath.row {
        case 0:
            self.selectedIndex = indexPath.row
            self.performSegue(withIdentifier: "showCategoryDropdown", sender: nil)
        case 1:
            self.selectedIndex = indexPath.row
            self.performSegue(withIdentifier: "showConferenceDropdown", sender: nil)
        case 2:
            self.selectedIndex = indexPath.row
            self.performSegue(withIdentifier: "showClientsDropdown", sender: nil)
        case 3:
            self.selectedIndex = indexPath.row
            self.performSegue(withIdentifier: "showPartyHistoryDropdown", sender: nil)
        case 4,5:
            self.selectedIndex = indexPath.row
            self.performSegue(withIdentifier: "ShowEmployeeDropdown", sender: nil)
        default:
            break
        }
            
    }
    

    //Mark Actions
    @IBAction func clickedClear(_ sender: Any) {
        objSelFilter.clearValues()
        lblSelectedCategory.isHidden = true
        lblConferenceHistory.isHidden = true
        lblSelectedMeetingHistory.isHidden = true
        lblSelectedPartyHistory.isHidden = true
        lblSelectedOutreachLead.isHidden = true
        lblSelectedScheduler.isHidden = true
        inpCompany.text = ""
        if radioBtnIsClient.isSelected{
            radioBtnIsClient.isSelected = true
        }
        UIView.animate(withDuration: 0.3) {
            self.tableView.reloadData()
        }
        
    }
    @IBAction func clickedClose(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func clickedApply(_ sender: Any) {
        if !(inpCompany.text?.isBlank)!{
            objSelFilter.strCompany = inpCompany.text!
        }
        delegateContactSelectedFilter?.didSelectedFilterItems(objSelFilter)
        self.dismiss(animated: true, completion: nil)
    }
    @objc @IBAction private func logSelectedButton(radioButton : DLRadioButton) {
        if (radioButton.isMultipleSelectionEnabled) {
            for button in radioButton.selectedButtons() {
                print(String(format: "%@ is selected.\n", button.titleLabel!.text!));
            }
        } else {
            print(String(format: "%@ is selected.\n", radioButton.selected()!.titleLabel!.text!));
            if radioButton.tag == 1{
                radioButton.tag = 0
                objSelFilter.isClient = 0
                radioButton.isSelected = false
            }else{
                if radioButton.isSelected{
                    radioButton.tag = 1
                    objSelFilter.isClient = 1
                }
            }
        }
    }
    //textField
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.returnKeyType == .done {
            textField.resignFirstResponder()
        }
        return false
    }
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showCategoryDropdown"{
            let destVc = segue.destination as! CategoryDropdownListViewController
            destVc.delegateSelectedItem = self
            destVc.arrSelectedIndex = objSelFilter.arrCategory
            
        }else if segue.identifier == "showConferenceDropdown"{
            let destVc = segue.destination as! ConferenceDropdownListViewController
            destVc.delegateSelectedItem = self
            destVc.arrSelectedIndex = objSelFilter.arrConference
            
        }else if segue.identifier == "ShowEmployeeDropdown"{
            let destVc = segue.destination as! EmployeeDropDownListViewController
            destVc.delegateSelectedItem = self
            let index = self.tableView.indexPathForSelectedRow
            if index?.row == 5{
                destVc.type = 1
                destVc.arrSelectedIndex = objSelFilter.arrScheduler
            }else{
                destVc.type = 2
                destVc.arrSelectedIndex = objSelFilter.arrOutreach
            }
        }else if segue.identifier == "showClientsDropdown"{
            let destVc = segue.destination as! ClientsDropdownListViewController
            destVc.delegateSelectedItem = self
            destVc.isMeetingHistory = true
            destVc.arrSelectedIndex = objSelFilter.arrMeeting
            
        }else if segue.identifier == "showPartyHistoryDropdown"{
            let destVc = segue.destination as! PartyHistoryDropdownListViewController
            destVc.delegateSelectedItem = self
            destVc.arrSelectedIndex = objSelFilter.arrPartyHistory
            
        }
    }
    

}
