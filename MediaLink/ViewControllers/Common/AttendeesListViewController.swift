//
//  AttendeesListViewController.swift
//  MediaLink
//
//  Created by Naveen on 3/2/18.
//  Copyright © 2018 Naveen. All rights reserved.
//

import UIKit

class AttendeesListViewController: UIViewController {

    @IBOutlet weak var tblView: UITableView!
    var arrAttendees = [Attendees]()
    var arrTempAttendees = [Attendees]()
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.tblView.tableFooterView = UIView()
        self.tblView.register(UINib(nibName: "AttendeesViewCell", bundle: nil), forCellReuseIdentifier: "AttendeesViewCell")
        self.tblView.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return .lightContent
    }
    @IBAction func clickedClose(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
// MARK:- UITableView

extension AttendeesListViewController: UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrAttendees.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AttendeesViewCell", for: indexPath)as! AttendeesViewCell
        let objAttendee = arrAttendees[indexPath.row]
        cell.configCell(objAttendee.picture, objAttendee.firstName!+" "+objAttendee.lastName!, objAttendee.company)
        cell.accessoryType = .disclosureIndicator
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "MenuSection2", bundle: nil)
        let destVc = storyboard.instantiateViewController(withIdentifier: "ContactDatabaseDetailsTableViewController") as! ContactDatabaseDetailsTableViewController
        destVc.contactId = arrAttendees[indexPath.row].contactId!
//        destVc.isFromOtherView = true
        self.show(destVc, sender: nil)
    }
    
}
//Serach Bar
extension AttendeesListViewController:UISearchBarDelegate{
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        arrAttendees = arrTempAttendees.filter{($0.firstName?.localizedCaseInsensitiveContains(searchText))! || ($0.lastName!.localizedCaseInsensitiveContains(searchText))}
        if(searchText.count == 0){
            arrAttendees=arrTempAttendees
        }
        self.tblView.reloadData()
    }
}
