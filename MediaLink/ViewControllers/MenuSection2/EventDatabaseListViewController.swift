//
//  EventDatabaseListViewController.swift
//  MediaLink
//
//  Created by Naveen on 3/5/18.
//  Copyright © 2018 Naveen. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper
import SwiftyJSON
import DZNEmptyDataSet

class EventDatabaseListViewController: UIViewController,SlideMenuControllerDelegate,UISplitViewControllerDelegate,SelectedFilterDelegate {

    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var segmentEvent: UISegmentedControl!
    
    var arrActiveParties = [EventDatabase]()
    var arrPastParties = [EventDatabase]()
    var offsetValueAcive = 0
    var offsetValuePast = 0
    var objSelectedFilter = SelectedFilter()
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.tblView.tableFooterView = UIView()
        self.tblView.register(UINib(nibName:"TwoItemsViewCell",bundle:nil), forCellReuseIdentifier: "TwoItemsViewCell")
        self.tblView.layoutMargins = UIEdgeInsets.zero
        self.tblView.separatorInset = UIEdgeInsets.zero
        self.tblView.estimatedRowHeight=180
        self.tblView.rowHeight=UITableViewAutomaticDimension
        self.setNavigationBarItem()
        splitViewController?.delegate=self
        getDataFromServer(offsetValueAcive,true,true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //Delegate Filter
    func didSelectedFilterItems(_ objFilter: SelectedFilter) {
        objSelectedFilter = objFilter
        self.arrActiveParties = []
        self.arrPastParties = []
        self.tblView.reloadData()
        if !DeviceType(){
            self.selectFirstRowIpad()
        }
        offsetValueAcive = 0
        offsetValuePast = 0
        if segmentEvent.selectedSegmentIndex == 0 {
            getDataFromServer(offsetValueAcive,true,true)
        }else{
            getDataFromServer(offsetValuePast,true,false)
        }
    }
    //For Ipad
    func selectFirstRowIpad(){
        if segmentEvent.selectedSegmentIndex == 0 {
            if arrActiveParties.count != 0{
                let index = IndexPath(row: 0, section: 0)
                self.tblView.selectRow(at: index, animated: false, scrollPosition: .top)
            }
            self.performSegue(withIdentifier: "ipadShowEventDetails", sender: nil)
        }else{
            if arrPastParties.count != 0{
                let index = IndexPath(row: 0, section: 0)
                self.tblView.selectRow(at: index, animated: false, scrollPosition: .top)
            }
            self.performSegue(withIdentifier: "ipadShowEventDetails", sender: nil)
        }
    }
    //menu Display
    func slideNavigationControllerShouldDisplayLeftMenu() -> Bool {
        return true
    }
    //Split View
    func splitViewController(_ svc: UISplitViewController, shouldHide vc: UIViewController, in orientation: UIInterfaceOrientation) -> Bool {
        return false
    }
    //ScrollView
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        //Bottom Refresh
        if segmentEvent.selectedSegmentIndex == 0{
            if self.arrActiveParties.count > 0 {
                if scrollView == tblView{
                    let currentOffset = Int(scrollView.contentOffset.y)
                    let maximumOffset = Int(scrollView.contentSize.height - scrollView.frame.size.height)
                    if maximumOffset - currentOffset <= -10 {
                        offsetValueAcive = offsetValueAcive + CommonStrings.limit
                        getDataFromServer(offsetValueAcive,false,true)
                    }
                }
            }
        }else{
            if self.arrPastParties.count > 0 {
                if scrollView == tblView{
                    let currentOffset = Int(scrollView.contentOffset.y)
                    let maximumOffset = Int(scrollView.contentSize.height - scrollView.frame.size.height)
                    if maximumOffset - currentOffset <= -10 {
                        offsetValuePast = offsetValuePast + CommonStrings.limit
                        getDataFromServer(offsetValuePast,false,false)
                    }
                }
            }
        }
    }
    //Actions
    @IBAction func clickedSegment(_ sender: Any) {
        self.tblView.reloadData()
        if segmentEvent.selectedSegmentIndex == 0 {
            if arrActiveParties.count == 0{
                getDataFromServer(offsetValueAcive,true,true)
            }else{
                if !DeviceType(){
                    self.selectFirstRowIpad()
                }
            }
        }else{
            if arrPastParties.count == 0{
                getDataFromServer(offsetValuePast,true,false)
            }else{
                if !DeviceType(){
                    self.selectFirstRowIpad()
                }
            }
        }
    }
    
    @IBAction func clickedReload(_ sender: Any) {
        
        if !Reachability.isConnectedToNetwork() {
            self.alert(message: "It seems that there is no Internet connection.")
            return
        }
        
        objSelectedFilter.clearValues()
        self.arrActiveParties = []
        self.arrPastParties = []
        
        offsetValueAcive = 0
        offsetValuePast = 0
        if segmentEvent.selectedSegmentIndex == 0 {
            getDataFromServer(offsetValueAcive,true,true)
        }else{
            getDataFromServer(offsetValuePast,true,false)
        }
    }
    
    @IBAction func clickedFilter(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Common", bundle: nil)
        let filterVC = storyboard.instantiateViewController(withIdentifier: "EventDatabaseFilterTableViewController") as! EventDatabaseFilterTableViewController
        filterVC.delegateEventSelectedFilter = self
        filterVC.objSelFilter = objSelectedFilter
        let nav = UINavigationController(rootViewController: filterVC)
        if !DeviceType(){
            self.modalPresentationStyle = .popover
        }
        UIApplication.shared.statusBarStyle = .lightContent
        self.present(nav, animated: true, completion: nil)
    }
    //Server Response
    func getDataFromServer(_ offSet:Int,_ isShowLoading : Bool,_ isActive:Bool){
        if Reachability.isConnectedToNetwork() {
            var url = ""
            if isActive{
                url = MediaLinkUrls.eventDatabaseListActive
            }else{
                url = MediaLinkUrls.eventDatabaseListPast
            }
            let repo = MediaLinkUserDefaults()
            var params = ["limit":String(CommonStrings.limit),
                          "offset":String(offSet),
                          "token":repo.tocken!]
            if !objSelectedFilter.strEventName.isEmpty{
                params["party_name"] = objSelectedFilter.strEventName
            }
            if objSelectedFilter.arrConference.count != 0{
                params["conference_ids"] = convertToJson(objSelectedFilter.arrConference)
            }
            if !objSelectedFilter.strPartyLocation.isEmpty{
                params["party_location"] = objSelectedFilter.strPartyLocation
            }
            if !objSelectedFilter.strDate.isEmpty{
                params["date"] = DateFormatterDateTimeFull(objSelectedFilter.strDate)
            }
            if !objSelectedFilter.strTime.isEmpty{
                params["time"] = timeFormatFromDateStringToServer(objSelectedFilter.strTime)
            }
            if isShowLoading{
                activityIndicatorShow(self)
            }
            Alamofire.request(url, method: .get, parameters: params).validate()
                .responseObject { (response:DataResponse<EventDatabaseResponse>) in
                    activityIndicatorHide()
                    if response.result.error == nil{
                        if let serverResponce = response.result.value{
                            if isActive{
                                if serverResponce.activeParties?.count != 0{
                                    if self.arrActiveParties.last?.date == serverResponce.activeParties?.first?.date{
                                        for item in (serverResponce.activeParties?.first?.data)!{
                                            self.arrActiveParties.last?.data?.append(item)
                                        }
                                        serverResponce.activeParties?.remove(at: 0)
                                    }
                                }
                                self.arrActiveParties.append(contentsOf: serverResponce.activeParties!)
                            }else{
                                if serverResponce.pastParties?.count != 0{
                                    if self.arrPastParties.last?.date == serverResponce.pastParties?.first?.date{
                                        for item in (serverResponce.pastParties?.first?.data)!{
                                            self.arrPastParties.last?.data?.append(item)
                                        }
                                        serverResponce.pastParties?.remove(at: 0)
                                    }
                                }
                                self.arrPastParties.append(contentsOf:serverResponce.pastParties!)
                            }
                            self.tblView.reloadData()
                            if !DeviceType() && isShowLoading{
                                self.selectFirstRowIpad()
                            }
                        }
                    }else if response.response?.statusCode == 422{
                        if let data = response.data {
                            let responseJSON = try! JSON(data: data)
                            if responseJSON["limit"].exists(){
                                if let message = responseJSON["limit"][0].string {
                                    self.alert(message: message)
                                }
                            }
                            else if responseJSON["offset"].exists(){
                                if let message = responseJSON["offset"][0].string {
                                    self.alert(message: message)
                                }
                            }
                        }
                    }else if response.response?.statusCode == 401{
                        authenticationError(self)
                    }else{
                        self.alert(message: checkNetworkError(response.result.error!))
                    }
            }
        }else{
            self.alert(message: "It seems that there is no Internet connection.")
        }
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showEventDatabaseDetails"{
            let destVc = segue.destination as! EventDatabaseDetailsTableViewController
            let index = tblView.indexPathForSelectedRow
            if segmentEvent.selectedSegmentIndex == 0{
                destVc.objEventParty = arrActiveParties[(index?.section)!].data![(index?.row)!]
            }else{
                destVc.objEventParty = arrPastParties[(index?.section)!].data![(index?.row)!]
            }
        }else if segue.identifier == "ipadShowEventDetails"{
            let destVc = segue.destination as?
            UINavigationController
            let detailViewController = destVc?.topViewController as! EventDatabaseDetailsTableViewController
            let index = tblView.indexPathForSelectedRow
            if segmentEvent.selectedSegmentIndex == 0{
                if arrActiveParties.count != 0{
                    detailViewController.objEventParty = arrActiveParties[(index?.section)!].data![(index?.row)!]
                }else{
                    detailViewController.objEventParty = nil
                }
            }else{
                if arrPastParties.count != 0{
                    detailViewController.objEventParty = arrPastParties[(index?.section)!].data![(index?.row)!]
                }else{
                    detailViewController.objEventParty = nil
                }
            }
        }
    }
    

}
//EmptyDataSet
extension EventDatabaseListViewController: DZNEmptyDataSetSource, DZNEmptyDataSetDelegate{
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        let text="No Records Found"
        let attributes = [NSAttributedStringKey.font: UIFont.boldSystemFont(ofSize: 18.0), NSAttributedStringKey.foregroundColor: UIColor.darkGray]
        return NSAttributedString(string: text, attributes: attributes)
    }
}
// TableView
extension EventDatabaseListViewController: UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 25
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: 25))
        headerView.backgroundColor = UIColor.colorPrimaryLight()
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: 25))
        if segmentEvent.selectedSegmentIndex == 0 {
            label.text =  dateFormatWithDay(arrActiveParties[section].date!)
        }else{
            label.text = dateFormatWithDay(arrPastParties[section].date!)
        }
        label.textColor = UIColor.white
        label.textAlignment = .center
        label.font = UIFont.systemFont(ofSize: 14)
        headerView.addSubview(label)
        
        return headerView
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        if segmentEvent.selectedSegmentIndex == 0 {
            return arrActiveParties.count
        }else{
            return arrPastParties.count
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if segmentEvent.selectedSegmentIndex == 0 {
            return (arrActiveParties[section].data?.count)!
        }else{
            return (arrPastParties[section].data?.count)!
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TwoItemsViewCell", for: indexPath) as! TwoItemsViewCell
        if segmentEvent.selectedSegmentIndex == 0 {
            let obj = arrActiveParties[indexPath.section].data![indexPath.row]
            cell.configCell(obj.partyName, obj.partyLocation)
        }else{
            let obj = arrPastParties[indexPath.section].data![indexPath.row]
            cell.configCell(obj.partyName, obj.partyLocation)
        }
        if DeviceType(){
            cell.selectionStyle = .none
        }
        else{
            cell.selectionStyle = .default
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if DeviceType(){
            self.performSegue(withIdentifier: "showEventDatabaseDetails", sender: nil)
        }else{
            self.performSegue(withIdentifier: "ipadShowEventDetails", sender: nil)
        }
    
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 48
    }
}
