//
//  ContactDatabaseDetailsTableViewController.swift
//  MediaLink
//
//  Created by Naveen on 3/5/18.
//  Copyright © 2018 Naveen. All rights reserved.
//

import UIKit
import MessageUI
import Alamofire
import AlamofireObjectMapper
import SwiftyJSON
import AlamofireImage

class ContactDatabaseDetailsTableViewController: UITableViewController,MFMailComposeViewControllerDelegate {

    @IBOutlet weak var imgProfilePic: UIImageView!
    @IBOutlet weak var lblFullname: UILabel!
    @IBOutlet weak var lblCompany: UILabel!
    @IBOutlet weak var btnUserEmail: UIButton!
    @IBOutlet weak var btnPhone: UIButton!
    @IBOutlet weak var lblOutreachLead: UILabel!
    @IBOutlet weak var lblScheduler: UILabel!
    @IBOutlet weak var lblCountry: UILabel!
    @IBOutlet weak var lblCategory: UILabel!
    @IBOutlet weak var btnAlternateEmail: UIButton!
    @IBOutlet weak var lblConferenceHistory: UILabel!
    @IBOutlet weak var lblPartyHistory: UILabel!
    @IBOutlet weak var lblMeetingHistory: UILabel!
    @IBOutlet weak var lblContactNotes: UILabel!
    
    var contactId = 0
    var isFromOtherView = false // true when from other view controller
    var objContactDatabase:ContactDatabase!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        if isFromOtherView {
            let barBtnClose = UIBarButtonItem(barButtonSystemItem: .stop, target: self, action: #selector(clickedClose))
            navigationItem.leftBarButtonItem = barBtnClose
        }
        self.tableView.tableFooterView = UIView()
        getDataFromServer()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @objc func clickedClose(){
        self.dismiss(animated: true, completion: nil)
    }
    func setData(_ objContact:ContactDatabase){
        imgProfilePic.image = UIImage.imageWithColor(color: UIColor.lightGray, size: imgProfilePic.frame.size).af_imageRoundedIntoCircle()
        if let imgName = objContact.picture{
            if let imageName = imgName.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed){
                if let url = URL(string: imageName){
                    let filter = AspectScaledToFillSizeWithRoundedCornersFilter(
                        size: self.imgProfilePic.frame.size,
                        radius: self.imgProfilePic.frame.width/2
                    )
                    self.imgProfilePic.af_setImage(withURL: url, placeholderImage: UIImage.imageWithColor(color: UIColor.lightGray, size: imgProfilePic.frame.size),filter: filter, imageTransition: .crossDissolve(0.2))
                }
            }
        }
        lblFullname.text = objContact.firstName!+" "+objContact.lastName!
        if let companyStr = objContact.company {
            lblCompany.text = companyStr
        }
        if let emailStr = objContact.email{
            btnUserEmail.setTitle(emailStr, for: .normal)
        }
        if let phoneStr = objContact.phoneNumber{
            btnPhone.setTitle(phoneStr, for: .normal)
        }
        if let outreachStr = objContact.outreachLeadName{
            lblOutreachLead.text = outreachStr
        }
        if let schedulerStr = objContact.schedulerName{
            lblScheduler.text = schedulerStr
        }
        if let countryStr = objContact.country{
            lblCountry.text = countryStr
        }
        var categoriesStr = ""
        for objItem in objContact.contactCategories!{
            if categoriesStr.isBlank{
                categoriesStr = objItem.categoryName!
            }else{
                categoriesStr = categoriesStr+", "+objItem.categoryName!
            }
        }
        lblCategory.text = categoriesStr
        if let assistEmailStr = objContact.assistantEmail{
            btnAlternateEmail.setTitle(assistEmailStr, for: .normal)
        }
        var conferenceStr = ""
        for objItem in objContact.contactConferences!{
            if conferenceStr.isBlank{
                conferenceStr = objItem.eventName!
            }else{
                conferenceStr = conferenceStr+", "+objItem.eventName!
            }
        }
        lblConferenceHistory.text = conferenceStr
        
        var partyStr = ""
        for objItem in objContact.contactParties!{
            if partyStr.isBlank{
                partyStr = objItem.partyName!
            }else{
                partyStr = partyStr+", "+objItem.partyName!
            }
        }
        lblPartyHistory.text = partyStr
        
        var meetingStr = ""
        for objItem in objContact.meetingHistory!{
            if meetingStr.isBlank{
                meetingStr = objItem.clientName!
            }else{
                partyStr = meetingStr+", "+objItem.clientName!
            }
        }
        lblMeetingHistory.text = meetingStr
        
        if let notesStr = objContact.contactNotes{
            lblContactNotes.text = notesStr
        }
        self.tableView.reloadData()
    }
    
    //Mail
    func sendMail(_ toMail: String){
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.navigationBar.tintColor = UIColor.white
            mail.setToRecipients(["\(toMail)"])
            if !DeviceType(){
                self.modalPresentationStyle = .popover
            }
            present(mail, animated: true)
        } else {
            // show failure alert
        }
    }
        
    //MFMailComposeViewController
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }
    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }

    
    @IBAction func clickedPhone(_ sender: Any) {
        if objContactDatabase != nil{
            if let phone = objContactDatabase.phoneNumber {
                callPhone(phone)
            }
        }
        
    }
    @IBAction func clickedUserMail(_ sender: Any) {
        if objContactDatabase != nil{
            if let email = objContactDatabase.email {
                self.sendMail(email)
            }
        }
    }
    @IBAction func clickedAlternateEmail(_ sender: Any) {
        if objContactDatabase != nil{
            if let email = objContactDatabase.assistantEmail {
                self.sendMail(email)
            }
        }
    }
    //Server Response
    func getDataFromServer(){
        if Reachability.isConnectedToNetwork(){
            if contactId != 0{
                let url = MediaLinkUrls.contactDatabase+"/"+String(contactId)
                let repo = MediaLinkUserDefaults()
                let params = ["token":repo.tocken!]
                activityIndicatorShow(self)
                Alamofire.request(url, method: .get, parameters: params).validate()
                    .responseObject { (response:DataResponse<ContactDatabase>) in
                        activityIndicatorHide()
                        if response.result.error == nil{
                            if let serverResponce = response.result.value{
                                self.objContactDatabase = serverResponce
                                self.setData(serverResponce)
                            }
                        }else if response.response?.statusCode == 422{
                        }else if response.response?.statusCode == 401{
                            authenticationError(self)
                        }else{
                            self.alert(message: checkNetworkError(response.result.error!))
                        }
                }
            }
            
        }else{
            self.alert(message: "It seems that there is no Internet connection.")
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
