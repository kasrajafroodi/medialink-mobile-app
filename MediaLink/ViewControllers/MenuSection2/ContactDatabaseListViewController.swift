//
//  ContactDatabaseListViewController.swift
//  MediaLink
//
//  Created by Naveen on 3/5/18.
//  Copyright © 2018 Naveen. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper
import SwiftyJSON
import DZNEmptyDataSet

class ContactDatabaseListViewController: UIViewController,SlideMenuControllerDelegate,UISplitViewControllerDelegate,SelectedFilterDelegate {

    @IBOutlet weak var tblView: UITableView!
    
    var arrContact = [ContactDatabase]()
    var offsetValue = 0
    var objSelectedFilter = SelectedFilter()
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.tblView.tableFooterView = UIView()
        self.tblView.register(UINib(nibName:"TwoItemsViewCell",bundle:nil), forCellReuseIdentifier: "TwoItemsViewCell")
        self.tblView.layoutMargins = UIEdgeInsets.zero
        self.tblView.separatorInset = UIEdgeInsets.zero
        self.tblView.estimatedRowHeight=180
        self.tblView.rowHeight=UITableViewAutomaticDimension
        self.setNavigationBarItem()
        splitViewController?.delegate=self
        getDataFromServer(offsetValue,true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //Delegate Filter
    func didSelectedFilterItems(_ objFilter: SelectedFilter) {
        objSelectedFilter = objFilter
        self.arrContact = []
        self.tblView.reloadData()
        if !DeviceType(){
            self.selectFirstRowIpad()
        }
        offsetValue = 0
        getDataFromServer(offsetValue,true)
    }
    //For Ipad
    func selectFirstRowIpad(){
        if arrContact.count != 0{
            let index = IndexPath(row: 0, section: 0)
            self.tblView.selectRow(at: index, animated: false, scrollPosition: .top)
        }
        self.performSegue(withIdentifier: "ipadShowContactDatabaseDetails", sender: nil)
    }
    //menu Display
    func slideNavigationControllerShouldDisplayLeftMenu() -> Bool {
        return true
    }
    //Split View
    func splitViewController(_ svc: UISplitViewController, shouldHide vc: UIViewController, in orientation: UIInterfaceOrientation) -> Bool {
        return false
    }
    //ScrollView
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        //Bottom Refresh
        if self.arrContact.count > 0 {
            if scrollView == tblView{
                let currentOffset = Int(scrollView.contentOffset.y)
                let maximumOffset = Int(scrollView.contentSize.height - scrollView.frame.size.height)
                if maximumOffset - currentOffset <= -10 {
                    offsetValue = offsetValue + CommonStrings.limit
                    getDataFromServer(offsetValue,false)
                }
            }
        }
    }
    
    @IBAction func clickedReload(_ sender: Any) {
        
        if !Reachability.isConnectedToNetwork() {
            self.alert(message: "It seems that there is no Internet connection.")
            return
        }
        
        objSelectedFilter.clearValues()
        self.arrContact = []
        
        offsetValue = 0
        getDataFromServer(offsetValue,true)
    }
    
    @IBAction func clickedFilter(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Common", bundle: nil)
        let filterVC = storyboard.instantiateViewController(withIdentifier: "ContactDatabaseFilterTableViewController") as! ContactDatabaseFilterTableViewController
        filterVC.delegateContactSelectedFilter = self
        filterVC.objSelFilter = objSelectedFilter
        let nav = UINavigationController(rootViewController: filterVC)
        if !DeviceType(){
            self.modalPresentationStyle = .popover
        }
        UIApplication.shared.statusBarStyle = .lightContent
        self.present(nav, animated: true, completion: nil)
    }
    //Server Response
    func getDataFromServer(_ offSet:Int,_ isShowLoading : Bool){
        if Reachability.isConnectedToNetwork() {
            let url = MediaLinkUrls.contactDatabase
            let repo = MediaLinkUserDefaults()
            var params = ["limit":String(CommonStrings.limit),
                          "offset":String(offSet),
                          "token":repo.tocken!]
            if !objSelectedFilter.strCompany.isEmpty{
                params["company"] = objSelectedFilter.strCompany
            }
            if objSelectedFilter.isClient != 0{
                params["is_client"] = String(objSelectedFilter.isClient)
            }
            if objSelectedFilter.arrConference.count != 0{
                params["conference_ids"] = convertToJson(objSelectedFilter.arrConference)
            }
            if objSelectedFilter.arrOutreach.count != 0{
                params["outreach_lead_ids"] = convertToJson(objSelectedFilter.arrOutreach)
            }
            if objSelectedFilter.arrScheduler.count != 0{
                params["scheduler_id"] = convertToJson(objSelectedFilter.arrScheduler)
            }
            if objSelectedFilter.arrCategory.count != 0{
                params["category"] = convertToJson(objSelectedFilter.arrCategory)
            }
            if objSelectedFilter.arrMeeting.count != 0{
                params["client_id"] = convertToJson(objSelectedFilter.arrMeeting)
            }
            if objSelectedFilter.arrPartyHistory.count != 0{
                params["party_id"] = String(objSelectedFilter.arrPartyHistory[0])
            }
            if isShowLoading{
                activityIndicatorShow(self)
            }
            Alamofire.request(url, method: .get, parameters: params).validate()
                .responseArray { (response:DataResponse<[ContactDatabase]>) in
                    activityIndicatorHide()
                    if response.result.error == nil{
                        if let serverResponce = response.result.value{
                            self.arrContact.append(contentsOf:serverResponce)
                            self.tblView.reloadData()
                            if !DeviceType() && isShowLoading{
                                self.selectFirstRowIpad()
                            }
                        }
                    }else if response.response?.statusCode == 422{
                        if let data = response.data {
                            let responseJSON = try! JSON(data: data)
                            if responseJSON["limit"].exists(){
                                if let message = responseJSON["limit"][0].string {
                                    self.alert(message: message)
                                }
                            }
                            else if responseJSON["offset"].exists(){
                                if let message = responseJSON["offset"][0].string {
                                    self.alert(message: message)
                                }
                            }
                        }
                    }else if response.response?.statusCode == 401{
                        authenticationError(self)
                    }else{
                        self.alert(message: checkNetworkError(response.result.error!))
                    }
            }
        }else{
            self.alert(message: "It seems that there is no Internet connection.")
        }
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showContactDatabaseDetails"{
            let destVc = segue.destination as! ContactDatabaseDetailsTableViewController
            let index = tblView.indexPathForSelectedRow
            destVc.contactId = arrContact[(index?.row)!].id!
        }else if segue.identifier == "ipadShowContactDatabaseDetails"{
            let destVc = segue.destination as?
            UINavigationController
            let detailViewController = destVc?.topViewController as! ContactDatabaseDetailsTableViewController
            let index = tblView.indexPathForSelectedRow
            if arrContact.count != 0{
                detailViewController.contactId = arrContact[(index?.row)!].id!
            }else{
                detailViewController.contactId = 0
            }
        }
    }
    

}
//EmptyDataSet
extension ContactDatabaseListViewController: DZNEmptyDataSetSource, DZNEmptyDataSetDelegate{
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        let text="No Records Found"
        let attributes = [NSAttributedStringKey.font: UIFont.boldSystemFont(ofSize: 18.0), NSAttributedStringKey.foregroundColor: UIColor.darkGray]
        return NSAttributedString(string: text, attributes: attributes)
    }
}
// TableView
extension ContactDatabaseListViewController: UITableViewDataSource,UITableViewDelegate{
   
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrContact.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TwoItemsViewCell", for: indexPath) as! TwoItemsViewCell
        let objContact = arrContact[indexPath.row]
        cell.configCell(objContact.firstName!+" "+objContact.lastName!, objContact.company)
        
        if DeviceType(){
            cell.selectionStyle = .none
        }
        else{
            cell.selectionStyle = .default
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if DeviceType(){
            self.performSegue(withIdentifier: "showContactDatabaseDetails", sender: nil)
        }else{
            self.performSegue(withIdentifier: "ipadShowContactDatabaseDetails", sender: nil)
        }
    
    }
}
