//
//  MyClientsEventAccessListViewController.swift
//  MediaLink
//
//  Created by Naveen on 3/3/18.
//  Copyright © 2018 Naveen. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper
import SwiftyJSON
import DZNEmptyDataSet

class MyClientsEventAccessListViewController: UIViewController,SlideMenuControllerDelegate,UISplitViewControllerDelegate,SelectedFilterDelegate {

    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var segmentMeetngs: UISegmentedControl!
    
    var arrActiveRequest = [EventRequest]()
    var arrPastRequest = [EventRequest]()
    var offsetValueAcive = 0
    var offsetValuePast = 0
    var objSelectedFilter = SelectedFilter()
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        //tableView
        
        self.tblView.tableFooterView = UIView()
        self.tblView.register(UINib(nibName:"EventAccessCell",bundle:nil), forCellReuseIdentifier: "EventAccessCell")
        self.tblView.layoutMargins = UIEdgeInsets.zero
        self.tblView.separatorInset = UIEdgeInsets.zero
        self.tblView.estimatedRowHeight=180
        self.tblView.rowHeight=UITableViewAutomaticDimension
        self.setNavigationBarItem()
        splitViewController?.delegate=self
        getDataFromServer(offsetValueAcive,true,true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func didSelectedFilterItems(_ objFilter: SelectedFilter) {
        objSelectedFilter = objFilter
        self.arrActiveRequest = []
        self.arrPastRequest = []
        self.tblView.reloadData()
        if !DeviceType(){
            self.selectFirstRowIpad()
        }
        offsetValueAcive = 0
        offsetValuePast = 0
        if segmentMeetngs.selectedSegmentIndex == 0 {
            getDataFromServer(offsetValueAcive,true,true)
        }else{
            getDataFromServer(offsetValuePast,true,false)
        }
    }
    //For Ipad
    func selectFirstRowIpad(){
        if segmentMeetngs.selectedSegmentIndex == 0 {
            if arrActiveRequest.count != 0{
                let index = IndexPath(row: 0, section: 0)
                self.tblView.selectRow(at: index, animated: false, scrollPosition: .top)
            }
            self.performSegue(withIdentifier: "ipadShowEventAccesDetails", sender: nil)
        }else{
            if arrPastRequest.count != 0{
                let index = IndexPath(row: 0, section: 0)
                self.tblView.selectRow(at: index, animated: false, scrollPosition: .top)
            }
            self.performSegue(withIdentifier: "ipadShowEventAccesDetails", sender: nil)
        }
    }
    //menu Display
    func slideNavigationControllerShouldDisplayLeftMenu() -> Bool {
        return true
    }
    //Split View
    func splitViewController(_ svc: UISplitViewController, shouldHide vc: UIViewController, in orientation: UIInterfaceOrientation) -> Bool {
        return false
    }
    //ScrollView
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        //Bottom Refresh
        if segmentMeetngs.selectedSegmentIndex == 0{
            if self.arrActiveRequest.count > 0 {
                if scrollView == tblView{
                    let currentOffset = Int(scrollView.contentOffset.y)
                    let maximumOffset = Int(scrollView.contentSize.height - scrollView.frame.size.height)
                    if maximumOffset - currentOffset <= -10 {
                        offsetValueAcive = offsetValueAcive + CommonStrings.limit
                        getDataFromServer(offsetValueAcive,false,true)
                    }
                }
            }
        }else{
            if self.arrPastRequest.count > 0 {
                if scrollView == tblView{
                    let currentOffset = Int(scrollView.contentOffset.y)
                    let maximumOffset = Int(scrollView.contentSize.height - scrollView.frame.size.height)
                    if maximumOffset - currentOffset <= -10 {
                        offsetValuePast = offsetValuePast + CommonStrings.limit
                        getDataFromServer(offsetValuePast,false,false)
                    }
                }
            }
        }
    }
    //Actions
    @IBAction func clickedSegment(_ sender: Any) {
        self.tblView.reloadData()
        if segmentMeetngs.selectedSegmentIndex == 0 {
            if arrActiveRequest.count == 0{
                getDataFromServer(offsetValueAcive,true,true)
            }else{
                if !DeviceType(){
                    self.selectFirstRowIpad()
                }
            }
        }else{
            if arrPastRequest.count == 0{
                getDataFromServer(offsetValuePast,true,false)
            }else{
                if !DeviceType(){
                    self.selectFirstRowIpad()
                }
            }
        }
    }
    
    @IBAction func clickedReload(_ sender: Any) {
        
        if !Reachability.isConnectedToNetwork() {
            self.alert(message: "It seems that there is no Internet connection.")
            return
        }
        
        objSelectedFilter.clearValues()
        self.arrActiveRequest = []
        self.arrPastRequest = []
        
        offsetValueAcive = 0
        offsetValuePast = 0
        if segmentMeetngs.selectedSegmentIndex == 0 {
            getDataFromServer(offsetValueAcive,true,true)
        }else{
            getDataFromServer(offsetValuePast,true,false)
        }
    }
    
    @IBAction func clickedFilter(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Common", bundle: nil)
        let filterVC = storyboard.instantiateViewController(withIdentifier: "MyClientsEventAccessFilterTableViewController") as! MyClientsEventAccessFilterTableViewController
        filterVC.delegateEventAccessFilter = self
        filterVC.objSelFilter = objSelectedFilter
        let nav = UINavigationController(rootViewController: filterVC)
        if !DeviceType(){
            self.modalPresentationStyle = .popover
        }
        UIApplication.shared.statusBarStyle = .lightContent
        self.present(nav, animated: true, completion: nil)
    }
    //Server Response
    func getDataFromServer(_ offSet:Int,_ isShowLoading : Bool,_ isActive:Bool){
        if Reachability.isConnectedToNetwork() {
            var url = ""
            if isActive{
                url = MediaLinkUrls.myClientsEventAccessActive
            }else{
                url = MediaLinkUrls.myClientsEventAccessPast
            }
            let repo = MediaLinkUserDefaults()
            var params = ["user_id":repo.userId,
                          "limit":String(CommonStrings.limit),
                          "offset":String(offSet),
                          "token":repo.tocken!]
            if !objSelectedFilter.strCompany.isEmpty{
                params["company"] = objSelectedFilter.strCompany
            }
            if !objSelectedFilter.strEventName.isEmpty{
                params["party_name"] = objSelectedFilter.strEventName
            }
            if objSelectedFilter.arrClient.count != 0{
                params["client_id"] = convertToJson(objSelectedFilter.arrClient)
            }
            if objSelectedFilter.arrConference.count != 0{
                params["conference_ids"] = convertToJson(objSelectedFilter.arrConference)
            }
            if objSelectedFilter.arrPartyStatus.count != 0{
                params["party_status"] = convertToJson(objSelectedFilter.arrPartyStatus)
            }            
            if !objSelectedFilter.strDate.isEmpty{
                params["date"] = DateFormatterDateTimeFull(objSelectedFilter.strDate)
            }
            if isShowLoading{
                activityIndicatorShow(self)
            }
            Alamofire.request(url, method: .get, parameters: params).validate()
                .responseObject { (response:DataResponse<EventAccessResponse>) in
                    activityIndicatorHide()
                    if response.result.error == nil{
                        if let serverResponce = response.result.value{
                            if isActive{
                                if serverResponce.activeRequests?.count != 0{
                                    if self.arrActiveRequest.last?.partyName == serverResponce.activeRequests?.first?.partyName{
                                        for item in (serverResponce.activeRequests?.first?.data)!{
                                            self.arrActiveRequest.last?.data?.append(item)
                                        }
                                        serverResponce.activeRequests?.remove(at: 0)
                                    }
                                }
                                self.arrActiveRequest.append(contentsOf: serverResponce.activeRequests!)
                            }else{
                                if serverResponce.pastRequests?.count != 0{
                                    if self.arrPastRequest.last?.partyName == serverResponce.pastRequests?.first?.partyName{
                                        for item in (serverResponce.pastRequests?.first?.data)!{
                                            self.arrPastRequest.last?.data?.append(item)
                                        }
                                        serverResponce.pastRequests?.remove(at: 0)
                                    }
                                }
                                self.arrPastRequest.append(contentsOf: serverResponce.pastRequests!)
                            }
                            self.tblView.reloadData()
                            if !DeviceType() && isShowLoading{
                                self.selectFirstRowIpad()
                            }
                        }
                    }else if response.response?.statusCode == 422{
                        if let data = response.data {
                            let responseJSON = try! JSON(data: data)
                            if responseJSON["limit"].exists(){
                                if let message = responseJSON["limit"][0].string {
                                    self.alert(message: message)
                                }
                            }
                            else if responseJSON["offset"].exists(){
                                if let message = responseJSON["offset"][0].string {
                                    self.alert(message: message)
                                }
                            }
                        }
                    }else if response.response?.statusCode == 401{
                        authenticationError(self)
                    }else{
                        self.alert(message: checkNetworkError(response.result.error!))
                    }
            }
        }else{
            self.alert(message: "It seems that there is no Internet connection.")
        }
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetails"{
            let destVc = segue.destination as! MyClientsEventAccessTableViewController
            let index = tblView.indexPathForSelectedRow
            if segmentMeetngs.selectedSegmentIndex == 0{
                destVc.objEventAccess = arrActiveRequest[(index?.section)!].data![(index?.row)!]
            }else{
                destVc.objEventAccess = arrPastRequest[(index?.section)!].data![(index?.row)!]
            }
        }else if segue.identifier == "ipadShowEventAccesDetails"{
            let destVc = segue.destination as?
            UINavigationController
            let detailViewController = destVc?.topViewController as! MyClientsEventAccessTableViewController
            let index = tblView.indexPathForSelectedRow
            if segmentMeetngs.selectedSegmentIndex == 0{
                if arrActiveRequest.count != 0{
                    detailViewController.objEventAccess = arrActiveRequest[(index?.section)!].data![(index?.row)!]
                }else{
                    detailViewController.objEventAccess = nil
                }
            }else{
                if arrPastRequest.count != 0{
                    detailViewController.objEventAccess = arrPastRequest[(index?.section)!].data![(index?.row)!]
                }else{
                    detailViewController.objEventAccess = nil
                }
            }
        }
    }
    

}
//EmptyDataSet
extension MyClientsEventAccessListViewController: DZNEmptyDataSetSource, DZNEmptyDataSetDelegate{
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        let text="No Records Found"
        let attributes = [NSAttributedStringKey.font: UIFont.boldSystemFont(ofSize: 18.0), NSAttributedStringKey.foregroundColor: UIColor.darkGray]
        return NSAttributedString(string: text, attributes: attributes)
    }
}
// TableView
extension MyClientsEventAccessListViewController: UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 25
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: 25))
        headerView.backgroundColor = UIColor.colorPrimaryLight()
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: 25))
        if segmentMeetngs.selectedSegmentIndex == 0 {
            label.text =  arrActiveRequest[section].partyName
        }else{
            label.text = arrPastRequest[section].partyName
        }
        label.textColor = UIColor.white
        label.textAlignment = .center
        label.font = UIFont.systemFont(ofSize: 14)
        headerView.addSubview(label)
        
        return headerView
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        if segmentMeetngs.selectedSegmentIndex == 0 {
            return arrActiveRequest.count
        }else{
            return arrPastRequest.count
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if segmentMeetngs.selectedSegmentIndex == 0 {
            return (arrActiveRequest[section].data?.count)!
        }else{
            return (arrPastRequest[section].data?.count)!
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "EventAccessCell", for: indexPath) as! EventAccessCell
        if segmentMeetngs.selectedSegmentIndex == 0 {
            let obj = arrActiveRequest[indexPath.section].data![indexPath.row]
            cell.configCell(obj.firstName!+" "+obj.lastName!, obj.company, obj.partyStatus)
        }else{
            let obj = arrPastRequest[indexPath.section].data![indexPath.row]
            cell.configCell(obj.firstName!+" "+obj.lastName!, obj.company, obj.partyStatus)
        }
        if DeviceType(){
            cell.selectionStyle = .none
        }
        else{
            cell.selectionStyle = .default
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if DeviceType(){
            self.performSegue(withIdentifier: "showDetails", sender: nil)
        }else{
            self.performSegue(withIdentifier: "ipadShowEventAccesDetails", sender: nil)
        }
    
    }
}
