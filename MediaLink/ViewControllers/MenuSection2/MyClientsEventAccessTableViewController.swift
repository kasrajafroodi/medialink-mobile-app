//
//  MyClientsEventAccessDetailsTableViewController.swift
//  MediaLink
//
//  Created by Naveen on 3/3/18.
//  Copyright © 2018 Naveen. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper
import SwiftyJSON
import AlamofireImage

class MyClientsEventAccessTableViewController: UITableViewController {

    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var lblFullname: UILabel!
    @IBOutlet weak var btnCompany: UIButton!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblNotes: UILabel!

    
    var objEventAccess:EventAccess!
    var objEventAccessDetails:EventAccess!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        if objEventAccess != nil{
            self.title = objEventAccess.partyName
        }
        self.tableView.tableFooterView = UIView()
        getDataFromServer()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func setData(_ objEventAccess : EventAccess){
        if let dateStr = objEventAccess.dateTime {
            lblDate.text = dateFormatWithDay(dateStr)
        }
        if let timeStr = objEventAccess.dateTime {
            lblTime.text = timeFormatWithFulldate(timeStr)
        }
        if let locationStr = objEventAccess.partyLocation{
            lblLocation.text = locationStr
        }
        
        if let companyStr = objEventAccess.company{
            self.btnCompany.setTitle(companyStr, for: .normal)
        }
        lblFullname.text = objEventAccess.firstName!+" "+objEventAccess.lastName!
        if let statusStr = objEventAccess.partyStatus{
            lblStatus.text = statusStr
        }
        if let notes = objEventAccess.notes{
            lblNotes.text = notes
        }
        self.tableView.reloadData()
    }
    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    

    @IBAction func clickedCompany(_ sender: Any) {
        if objEventAccessDetails != nil{
            if let contactId = objEventAccessDetails.contactId{
                let storyboard = UIStoryboard(name: "MenuSection2", bundle: nil)
                let destVc = storyboard.instantiateViewController(withIdentifier: "ContactDatabaseDetailsTableViewController") as! ContactDatabaseDetailsTableViewController
                destVc.isFromOtherView = true
                destVc.contactId = contactId
                let nav = UINavigationController(rootViewController: destVc)
                if !DeviceType(){
                    self.modalPresentationStyle = .popover
                }
                self.present(nav, animated: true, completion: nil)
            }
        }
    }
    //Server Response
    func getDataFromServer(){
        if Reachability.isConnectedToNetwork(){
            if objEventAccess != nil{
                let url = MediaLinkUrls.myClientsEventAccess+"/"+String(objEventAccess.id!)
                let repo = MediaLinkUserDefaults()
                let params = ["token":repo.tocken!]
                activityIndicatorShow(self)
                Alamofire.request(url, method: .get, parameters: params).validate()
                    .responseObject { (response:DataResponse<EventAccess>) in
                        activityIndicatorHide()
                        if response.result.error == nil{
                            if let serverResponce = response.result.value{
                                self.objEventAccessDetails = serverResponce
                                self.setData(serverResponce)
                            }
                        }else if response.response?.statusCode == 401{
                            authenticationError(self)
                        }else{
                            self.alert(message: checkNetworkError(response.result.error!))
                        }
                }
            }            
        }else{
            self.alert(message: "It seems that there is no Internet connection.")
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
