//
//  OnTheGroundViewController.swift
//  MediaLink
//
//  Created by Naveen on 3/6/18.
//  Copyright © 2018 Naveen. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper
import SwiftyJSON
import DZNEmptyDataSet

class OnTheGroundViewController: UIViewController,SlideMenuControllerDelegate,UISplitViewControllerDelegate,SelectedFilterDelegate,ReloadDelegate {

    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var segmentMeetings: UISegmentedControl!
    
    var arrActiveMeeting = [MeetingList]()
    var arrPastMeeting = [MeetingList]()
    var offsetValueAcive = 0
    var offsetValuePast = 0
    var objSelectedFilter = SelectedFilter()
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.tblView.tableFooterView = UIView()
        self.tblView.layoutMargins = UIEdgeInsets.zero
        self.tblView.separatorInset = UIEdgeInsets.zero
        self.tblView.estimatedRowHeight=180
        self.tblView.rowHeight=UITableViewAutomaticDimension
        self.setNavigationBarItem()
        splitViewController?.delegate=self
        getDataFromServer(offsetValueAcive,true,true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //Delegate
    //Reload
    func reloadData(){
        self.arrActiveMeeting = []
        self.arrPastMeeting = []
        self.tblView.reloadData()
        offsetValueAcive = 0
        offsetValuePast = 0
        if segmentMeetings.selectedSegmentIndex == 0 {
            getDataFromServer(offsetValueAcive,true,true)
        }else{
            getDataFromServer(offsetValuePast,true,false)
        }
    }
    //Filter
    func didSelectedFilterItems(_ objFilter: SelectedFilter) {
        objSelectedFilter = objFilter
        self.arrActiveMeeting = []
        self.arrPastMeeting = []
        self.tblView.reloadData()
        if !DeviceType(){
            self.selectFirstRowIpad()
        }
        offsetValueAcive = 0
        offsetValuePast = 0
        if segmentMeetings.selectedSegmentIndex == 0 {
            getDataFromServer(offsetValueAcive,true,true)
        }else{
            getDataFromServer(offsetValuePast,true,false)
        }
    }
    
   
    
    //For Ipad
    func selectFirstRowIpad(){
        if segmentMeetings.selectedSegmentIndex == 0 {
            if arrActiveMeeting.count != 0{
                let index = IndexPath(row: 0, section: 0)
                self.tblView.selectRow(at: index, animated: false, scrollPosition: .top)
            }
            self.performSegue(withIdentifier: "ipadShowOnTheGroundDetails", sender: nil)
        }else{
            if arrPastMeeting.count != 0{
                let index = IndexPath(row: 0, section: 0)
                self.tblView.selectRow(at: index, animated: false, scrollPosition: .top)
            }
            self.performSegue(withIdentifier: "ipadShowOnTheGroundDetails", sender: nil)
        }
    }
    //menu Display
    func slideNavigationControllerShouldDisplayLeftMenu() -> Bool {
        return true
    }
    //Split View
    func splitViewController(_ svc: UISplitViewController, shouldHide vc: UIViewController, in orientation: UIInterfaceOrientation) -> Bool {
        return false
    }
    //ScrollView
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        //Bottom Refresh
        if segmentMeetings.selectedSegmentIndex == 0{
            if self.arrActiveMeeting.count > 0 {
                if scrollView == tblView{
                    let currentOffset = Int(scrollView.contentOffset.y)
                    let maximumOffset = Int(scrollView.contentSize.height - scrollView.frame.size.height)
                    if maximumOffset - currentOffset <= -10 {
                        offsetValueAcive = offsetValueAcive + CommonStrings.limit
                        getDataFromServer(offsetValueAcive,false,true)
                    }
                }
            }
        }else{
            if self.arrPastMeeting.count > 0 {
                if scrollView == tblView{
                    let currentOffset = Int(scrollView.contentOffset.y)
                    let maximumOffset = Int(scrollView.contentSize.height - scrollView.frame.size.height)
                    if maximumOffset - currentOffset <= -10 {
                        offsetValuePast = offsetValuePast + CommonStrings.limit
                        getDataFromServer(offsetValuePast,false,false)
                    }
                }
            }
        }
    }
    //Actions
    @IBAction func clickedSegment(_ sender: Any) {
        self.tblView.reloadData()
        if segmentMeetings.selectedSegmentIndex == 0 {
            if arrActiveMeeting.count == 0{
                getDataFromServer(offsetValueAcive,true,true)
            }else{
                if !DeviceType(){
                    self.selectFirstRowIpad()
                }
            }
        }else{
            if arrPastMeeting.count == 0{
                getDataFromServer(offsetValuePast,true,false)
            }else{
                if !DeviceType(){
                    self.selectFirstRowIpad()
                }
            }
        }
    }
    
    @IBAction func clickedReload(_ sender: Any) {
        
        if !Reachability.isConnectedToNetwork() {
            self.alert(message: "It seems that there is no Internet connection.")
            return
        }
        
        objSelectedFilter.clearValues()
        self.arrActiveMeeting = []
        self.arrPastMeeting = []
        
        offsetValueAcive = 0
        offsetValuePast = 0
        if segmentMeetings.selectedSegmentIndex == 0 {
            getDataFromServer(offsetValueAcive,true,true)
        }else{
            getDataFromServer(offsetValuePast,true,false)
        }
    }
    
    @IBAction func clickedFilter(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Common", bundle: nil)
        let filterVC = storyboard.instantiateViewController(withIdentifier: "CommonFilterTableViewController") as! CommonFilterTableViewController
        filterVC.delegateCommonSelectedFilter = self
        filterVC.objSelFilter = objSelectedFilter
        let nav = UINavigationController(rootViewController: filterVC)
        if !DeviceType(){
            self.modalPresentationStyle = .popover
        }
        UIApplication.shared.statusBarStyle = .lightContent
        self.present(nav, animated: true, completion: nil)
    }
    //Server Response
    func getDataFromServer(_ offSet:Int,_ isShowLoading : Bool,_ isActive:Bool){
        if Reachability.isConnectedToNetwork() {
            var url = ""
            if isActive{
                url = MediaLinkUrls.onTheGroundActive
            }else{
                url = MediaLinkUrls.onTheGroundPast
            }
            let repo = MediaLinkUserDefaults()
            var params = ["user_id":repo.userId,
                          "limit":String(CommonStrings.limit),
                          "offset":String(offSet),
                          "token":repo.tocken!]
            if !objSelectedFilter.strCompany.isEmpty{
                params["company"] = objSelectedFilter.strCompany
            }
            if objSelectedFilter.arrClient.count != 0{
                params["clients"] = convertToJson(objSelectedFilter.arrClient)
            }
            if objSelectedFilter.arrConference.count != 0{
                params["conference_ids"] = convertToJson(objSelectedFilter.arrConference)
            }
            if objSelectedFilter.arrLocation.count != 0{
                params["location"] = convertToJson(objSelectedFilter.arrLocation)
            }
            if objSelectedFilter.arrScheduler.count != 0{
                params["scheduler"] = convertToJson(objSelectedFilter.arrScheduler)
            }
            if !objSelectedFilter.strDate.isEmpty{
                params["date"] = DateFormatterDateTimeFull(objSelectedFilter.strDate)
            }
            if objSelectedFilter.arrOutreach.count != 0{
                params["scheduler"] = convertToJson(objSelectedFilter.arrOutreach)
            }
            if isShowLoading{
                activityIndicatorShow(self)
            }
            Alamofire.request(url, method: .get, parameters: params).validate()
                .responseObject { (response:DataResponse<MeetingResponse>) in
                    activityIndicatorHide()
                    if response.result.error == nil{
                        if let serverResponce = response.result.value{
                            if isActive{
                                if serverResponce.activeMeetings?.count != 0{
                                    if self.arrActiveMeeting.last?.date == serverResponce.activeMeetings?.first?.date{
                                        for item in (serverResponce.activeMeetings?.first?.data)!{
                                            self.arrActiveMeeting.last?.data?.append(item)
                                        }
                                        serverResponce.activeMeetings?.remove(at: 0)
                                    }
                                }
                                self.arrActiveMeeting.append(contentsOf: serverResponce.activeMeetings!)
                            }else{
                                if serverResponce.pastMeetings?.count != 0{
                                    if self.arrPastMeeting.last?.date == serverResponce.pastMeetings?.first?.date{
                                        for item in (serverResponce.pastMeetings?.first?.data)!{
                                            self.arrPastMeeting.last?.data?.append(item)
                                        }
                                        serverResponce.pastMeetings?.remove(at: 0)
                                    }
                                }
                                self.arrPastMeeting.append(contentsOf: serverResponce.pastMeetings!)
                            }
                            self.tblView.reloadData()
                            if !DeviceType() && isShowLoading{
                                self.selectFirstRowIpad()
                            }
                        }
                    }else if response.response?.statusCode == 422{
                        if let data = response.data {
                            let responseJSON = try! JSON(data: data)
                            if responseJSON["limit"].exists(){
                                if let message = responseJSON["limit"][0].string {
                                    self.alert(message: message)
                                }
                            }
                            else if responseJSON["offset"].exists(){
                                if let message = responseJSON["offset"][0].string {
                                    self.alert(message: message)
                                }
                            }
                        }
                    }else if response.response?.statusCode == 401{
                        authenticationError(self)
                    }else{
                        self.alert(message: checkNetworkError(response.result.error!))
                    }
            }
        }else{
            self.alert(message: "It seems that there is no Internet connection.")
        }
    }
    @objc func clickedClient(_ tap:UITapGestureRecognizer){
        if Reachability.isConnectedToNetwork(){
            let getLabel = tap.view as! customUILabel
            var objDetails:Meeting!
            if segmentMeetings.selectedSegmentIndex == 0 {
                objDetails = arrActiveMeeting[getLabel.section].data![getLabel.row]
            }else{
                objDetails = arrPastMeeting[getLabel.section].data![getLabel.row]
            }
            var colorId = 0
            if let clientColor = objDetails.clientColor{
                colorId = clientColor
            }
            switch colorId{
            case 0:
                colorId = 1
            case 1:
                colorId = 2
            case 2:
                colorId = 0
            default:
                break
            }
            let url = MediaLinkUrls.updateClientColor+"\(objDetails.id!)/\(colorId)"
            let repo = MediaLinkUserDefaults()
            let params = ["token":repo.tocken!]
            activityIndicatorShow(self)
            Alamofire.request(url, method: .get, parameters: params).validate()
                .responseJSON{ response in
                    activityIndicatorHide()
                    if response.result.error == nil{
                        if let data = response.data {
                            let responseJSON = try! JSON(data: data)
                            if responseJSON["message"].exists(){
                                if self.segmentMeetings.selectedSegmentIndex == 0 {
                                    self.arrActiveMeeting[getLabel.section].data![getLabel.row].clientColor = colorId
                                }else{
                                    self.arrPastMeeting[getLabel.section].data![getLabel.row].clientColor = colorId
                                }
                                self.tblView.reloadData()
                            }
                        }
                    }else if response.response?.statusCode == 401{
                        authenticationError(self)
                    }else{
                        self.alert(message: checkNetworkError(response.result.error!))
                    }
            }
        }else{
            self.alert(message: "It seems that there is no Internet connection.")
        }
    }
    @objc func clickedCompany(_ tap:UITapGestureRecognizer){
        if Reachability.isConnectedToNetwork(){
            let getLabel = tap.view as! customUILabel
            var objDetails:Meeting!
            if segmentMeetings.selectedSegmentIndex == 0 {
                objDetails = arrActiveMeeting[getLabel.section].data![getLabel.row]
            }else{
                objDetails = arrPastMeeting[getLabel.section].data![getLabel.row]
            }
            var colorId = 0
            if let companyColor = objDetails.companyColor{
                colorId = companyColor
            }
            switch colorId{
            case 0:
                colorId = 1
            case 1:
                colorId = 2
            case 2:
                colorId = 0
            default:
                break
            }
            let url = MediaLinkUrls.updateCompanyColor+"\(objDetails.id!)/\(colorId)"
            let repo = MediaLinkUserDefaults()
            let params = ["token":repo.tocken!]
            activityIndicatorShow(self)
            Alamofire.request(url, method: .get, parameters: params).validate()
                .responseJSON{ response in
                    activityIndicatorHide()
                    if response.result.error == nil{
                        if let data = response.data {
                            let responseJSON = try! JSON(data: data)
                            if responseJSON["message"].exists(){
                                if self.segmentMeetings.selectedSegmentIndex == 0 {
                                    self.arrActiveMeeting[getLabel.section].data![getLabel.row].companyColor = colorId
                                }else{
                                    self.arrPastMeeting[getLabel.section].data![getLabel.row].companyColor = colorId
                                }
                                self.tblView.reloadData()
                            }
                        }
                    }else if response.response?.statusCode == 401{
                        authenticationError(self)
                    }else{
                        self.alert(message: checkNetworkError(response.result.error!))
                    }
            }
        }else{
            self.alert(message: "It seems that there is no Internet connection.")
        }
    }
    @objc func clickedMeetingsCover(_ tap:UITapGestureRecognizer){
        if Reachability.isConnectedToNetwork(){
            let getLabel = tap.view as! customUILabel
            var objDetails:Meeting!
            if segmentMeetings.selectedSegmentIndex == 0 {
                objDetails = arrActiveMeeting[getLabel.section].data![getLabel.row]
            }else{
                objDetails = arrPastMeeting[getLabel.section].data![getLabel.row]
            }
            var colorId = 0
            if let meetingColor = objDetails.meetingColor{
                colorId = meetingColor
            }
            switch colorId{
            case 0:
                colorId = 1
            case 1:
                colorId = 2
            case 2:
                colorId = 0
            default:
                break
            }
            let url = MediaLinkUrls.updateMeetingCoverColor+"\(objDetails.id!)/\(colorId)"
            let repo = MediaLinkUserDefaults()
            let params = ["token":repo.tocken!]
            activityIndicatorShow(self)
            Alamofire.request(url, method: .get, parameters: params).validate()
                .responseJSON{ response in
                    activityIndicatorHide()
                    if response.result.error == nil{
                        if let data = response.data {
                            let responseJSON = try! JSON(data: data)
                            if responseJSON["message"].exists(){
                                if self.segmentMeetings.selectedSegmentIndex == 0 {
                                    self.arrActiveMeeting[getLabel.section].data![getLabel.row].meetingColor = colorId
                                }else{
                                    self.arrPastMeeting[getLabel.section].data![getLabel.row].meetingColor = colorId
                                }
                                self.tblView.reloadData()
                            }
                        }
                    }else if response.response?.statusCode == 401{
                        authenticationError(self)
                    }else{
                        self.alert(message: checkNetworkError(response.result.error!))
                    }
            }
        }else{
            self.alert(message: "It seems that there is no Internet connection.")
        }
    }
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showOnTheGroundDetails"{
            let destVc = segue.destination as! OnTheGroundDetailsViewController
            let index = tblView.indexPathForSelectedRow
            destVc.reloadDelegate = self
            if segmentMeetings.selectedSegmentIndex == 0{
                destVc.objMeeting = arrActiveMeeting[(index?.section)!].data![(index?.row)!]
            }else{
                destVc.objMeeting = arrPastMeeting[(index?.section)!].data![(index?.row)!]
            }
        }else if segue.identifier == "ipadShowOnTheGroundDetails"{
            let destVc = segue.destination as?
            UINavigationController
            let detailViewController = destVc?.topViewController as! OnTheGroundDetailsViewController
            let index = tblView.indexPathForSelectedRow
            if segmentMeetings.selectedSegmentIndex == 0{
                if arrActiveMeeting.count != 0{
                    detailViewController.objMeeting = arrActiveMeeting[(index?.section)!].data![(index?.row)!]
                }else{
                    detailViewController.objMeeting = nil
                }
            }else{
                if arrPastMeeting.count != 0{
                    detailViewController.objMeeting = arrPastMeeting[(index?.section)!].data![(index?.row)!]
                }else{
                    detailViewController.objMeeting = nil
                }
            }
        }
    }
    

}
//EmptyDataSet
extension OnTheGroundViewController: DZNEmptyDataSetSource, DZNEmptyDataSetDelegate{
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        let text="No Meetings Found"
        let attributes = [NSAttributedStringKey.font: UIFont.boldSystemFont(ofSize: 18.0), NSAttributedStringKey.foregroundColor: UIColor.darkGray]
        return NSAttributedString(string: text, attributes: attributes)
    }
}
//table view
extension OnTheGroundViewController: UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 25
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: 25))
        headerView.backgroundColor = UIColor.colorPrimaryLight()
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: 25))
        if segmentMeetings.selectedSegmentIndex == 0 {
            if let dateStr = arrActiveMeeting[section].date {
                if !dateStr.isBlank {
                    label.text = dateFormatWithDay(dateStr)
                }else {
                    label.text = "No Date"
                }
            }else {
                label.text = "No Date"
            }
        }else {
            if let dateStr = arrPastMeeting[section].date {
                if !dateStr.isBlank {
                    label.text = dateFormatWithDay(dateStr)
                }else {
                    label.text = "No Date"
                }
            }else {
                label.text = "No Date"
            }            
        }
        label.textColor = UIColor.white
        label.textAlignment = .center
        label.font = UIFont.systemFont(ofSize: 14)
        headerView.addSubview(label)
        
        return headerView
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        if segmentMeetings.selectedSegmentIndex == 0 {
            return arrActiveMeeting.count
        }else{
            return arrPastMeeting.count
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if segmentMeetings.selectedSegmentIndex == 0 {
            return (arrActiveMeeting[section].data?.count)!
        }else{
            return (arrPastMeeting[section].data?.count)!
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "OnTheGroundViewCell", for: indexPath) as! OnTheGroundViewCell
        var objMeeting:Meeting!
        if segmentMeetings.selectedSegmentIndex == 0 {
            objMeeting = arrActiveMeeting[indexPath.section].data![indexPath.row]
            
        }else{
            objMeeting = arrPastMeeting[indexPath.section].data![indexPath.row]
        }
        if objMeeting.clientName != nil{
            cell.lblClientCompany.section = indexPath.section
            cell.lblClientCompany.row = indexPath.row
            let tapClient = UITapGestureRecognizer(target: self, action: #selector(clickedClient))
            cell.lblClientCompany.addGestureRecognizer(tapClient)
        }
        if objMeeting.company != nil{
            cell.lblTargetCompany.section = indexPath.section
            cell.lblTargetCompany.row = indexPath.row
            let tapCompany = UITapGestureRecognizer(target: self, action: #selector(clickedCompany))
            cell.lblTargetCompany.addGestureRecognizer(tapCompany)
        }
        if objMeeting.meetingCoverName != nil{
            cell.lblMeetingsCover.section = indexPath.section
            cell.lblMeetingsCover.row = indexPath.row
            let tapMeetings = UITapGestureRecognizer(target: self, action: #selector(clickedMeetingsCover))
            cell.lblMeetingsCover.addGestureRecognizer(tapMeetings)
        }
        cell.configCell(objMeeting)
        if DeviceType(){
            cell.selectionStyle = .none
        }
        else{
            cell.selectionStyle = .default
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if DeviceType(){
            self.performSegue(withIdentifier: "showOnTheGroundDetails", sender: nil)
        }else{
            self.performSegue(withIdentifier: "ipadShowOnTheGroundDetails", sender: nil)
        }
    
    }
}
