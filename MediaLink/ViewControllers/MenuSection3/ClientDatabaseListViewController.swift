//
//  ClientDatabaseListViewController.swift
//  MediaLink
//
//  Created by Naveen on 3/6/18.
//  Copyright © 2018 Naveen. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper
import SwiftyJSON
import DZNEmptyDataSet

class ClientDatabaseListViewController: UIViewController,SlideMenuControllerDelegate,UISplitViewControllerDelegate,SelectedFilterDelegate  {

    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var segmentClients: UISegmentedControl!
    
    var arrActiveClient = [ClientDatabase]()
    var arrInactiveClient = [ClientDatabase]()
    var offsetValueAcive = 0
    var offsetValueInactive = 0
    var objSelectedFilter = SelectedFilter()
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.tblView.register(UINib(nibName: "CommonCell", bundle: nil), forCellReuseIdentifier: "CommonCell")
        self.tblView.tableFooterView = UIView()
        self.tblView.layoutMargins = UIEdgeInsets.zero
        self.tblView.separatorInset = UIEdgeInsets.zero
        self.tblView.estimatedRowHeight=180
        self.tblView.rowHeight=UITableViewAutomaticDimension
        self.setNavigationBarItem()
        splitViewController?.delegate=self
        getDataFromServer(offsetValueAcive,true,true)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //Delegate Filter
    func didSelectedFilterItems(_ objFilter: SelectedFilter) {
        objSelectedFilter = objFilter
        self.arrActiveClient = []
        self.arrInactiveClient = []
        self.tblView.reloadData()
        if !DeviceType(){
            self.selectFirstRowIpad()
        }
        offsetValueAcive = 0
        offsetValueInactive = 0
        if segmentClients.selectedSegmentIndex == 0 {
            getDataFromServer(offsetValueAcive,true,true)
        }else{
            getDataFromServer(offsetValueInactive,true,false)
        }
    }
    
    
    
    //For Ipad
    func selectFirstRowIpad(){
        if segmentClients.selectedSegmentIndex == 0 {
            if arrActiveClient.count != 0{
                let index = IndexPath(row: 0, section: 0)
                self.tblView.selectRow(at: index, animated: false, scrollPosition: .top)
            }
            self.performSegue(withIdentifier: "ipadShowClientDatabaseDetails", sender: nil)
        }else{
            if arrInactiveClient.count != 0{
                let index = IndexPath(row: 0, section: 0)
                self.tblView.selectRow(at: index, animated: false, scrollPosition: .top)
            }
            self.performSegue(withIdentifier: "ipadShowClientDatabaseDetails", sender: nil)
        }
    }
    //menu Display
    func slideNavigationControllerShouldDisplayLeftMenu() -> Bool {
        return true
    }
    //Split View
    func splitViewController(_ svc: UISplitViewController, shouldHide vc: UIViewController, in orientation: UIInterfaceOrientation) -> Bool {
        return false
    }
    //ScrollView
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        //Bottom Refresh
        if segmentClients.selectedSegmentIndex == 0{
            if self.arrActiveClient.count > 0 {
                if scrollView == tblView{
                    let currentOffset = Int(scrollView.contentOffset.y)
                    let maximumOffset = Int(scrollView.contentSize.height - scrollView.frame.size.height)
                    if maximumOffset - currentOffset <= -10 {
                        offsetValueAcive = offsetValueAcive + CommonStrings.limit
                        getDataFromServer(offsetValueAcive,false,true)
                    }
                }
            }
        }else{
            if self.arrInactiveClient.count > 0 {
                if scrollView == tblView{
                    let currentOffset = Int(scrollView.contentOffset.y)
                    let maximumOffset = Int(scrollView.contentSize.height - scrollView.frame.size.height)
                    if maximumOffset - currentOffset <= -10 {
                        offsetValueInactive = offsetValueInactive + CommonStrings.limit
                        getDataFromServer(offsetValueInactive,false,false)
                    }
                }
            }
        }
    }
    //Actions
    @IBAction func clickedSegment(_ sender: Any) {
        self.tblView.reloadData()
        if segmentClients.selectedSegmentIndex == 0 {
            if arrActiveClient.count == 0{
                getDataFromServer(offsetValueAcive,true,true)
            }else{
                if !DeviceType(){
                    self.selectFirstRowIpad()
                }
            }
        }else{
            if arrInactiveClient.count == 0{
                getDataFromServer(offsetValueInactive,true,false)
            }else{
                if !DeviceType(){
                    self.selectFirstRowIpad()
                }
            }
        }
    }
    
    @IBAction func clickedReload(_ sender: Any) {
        
        if !Reachability.isConnectedToNetwork() {
            self.alert(message: "It seems that there is no Internet connection.")
            return
        }
        
        objSelectedFilter.clearValues()
        self.arrActiveClient = []
        self.arrInactiveClient = []
        
        offsetValueAcive = 0
        offsetValueInactive = 0
        if segmentClients.selectedSegmentIndex == 0 {
            getDataFromServer(offsetValueAcive,true,true)
        }else{
            getDataFromServer(offsetValueInactive,true,false)
        }
    }
    
    @IBAction func clickedFilter(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Common", bundle: nil)
        let filterVC = storyboard.instantiateViewController(withIdentifier: "ClientDatabaseFilterTableViewController") as! ClientDatabaseFilterTableViewController
        filterVC.delegateClientDatabaseSelectedFilter = self
        filterVC.objSelFilter = objSelectedFilter
        let nav = UINavigationController(rootViewController: filterVC)
        if !DeviceType(){
            self.modalPresentationStyle = .popover
        }
        UIApplication.shared.statusBarStyle = .lightContent
        self.present(nav, animated: true, completion: nil)
    }
    //Server Response
    func getDataFromServer(_ offSet:Int,_ isShowLoading : Bool,_ isActive:Bool){
        if Reachability.isConnectedToNetwork() {
            var url = ""
            if isActive{
                url = MediaLinkUrls.clientDatabaseActive
            }else{
                url = MediaLinkUrls.clientDatabaseInactive
            }
            let repo = MediaLinkUserDefaults()
            var params = ["limit":String(CommonStrings.limit),
                          "offset":String(offSet),
                          "token":repo.tocken!]
            if objSelectedFilter.arrClient.count != 0{
                params["client_ids"] = convertToJson(objSelectedFilter.arrClient)
            }
            if objSelectedFilter.arrCategory.count != 0{
                params["category"] = convertToJson(objSelectedFilter.arrCategory)
            }
            if objSelectedFilter.arrClientTeam.count != 0{
                params["client_team"] = convertToJson(objSelectedFilter.arrClientTeam)
            }
            if isShowLoading{
                activityIndicatorShow(self)
            }
            Alamofire.request(url, method: .get, parameters: params).validate()
                .responseObject { (response:DataResponse<ClientDatabaseResponse>) in
                    activityIndicatorHide()
                    if response.result.error == nil{
                        if let serverResponce = response.result.value{
                            if isActive{
                                self.arrActiveClient.append(contentsOf: serverResponce.activeClients!)
                            }else{
                                self.arrInactiveClient.append(contentsOf:serverResponce.inActiveClients!)
                            }
                            self.tblView.reloadData()
                            if !DeviceType() && isShowLoading{
                                self.selectFirstRowIpad()
                            }
                        }
                    }else if response.response?.statusCode == 422{
                        if let data = response.data {
                            let responseJSON = try! JSON(data: data)
                            if responseJSON["limit"].exists(){
                                if let message = responseJSON["limit"][0].string {
                                    self.alert(message: message)
                                }
                            }
                            else if responseJSON["offset"].exists(){
                                if let message = responseJSON["offset"][0].string {
                                    self.alert(message: message)
                                }
                            }
                        }
                    }else if response.response?.statusCode == 401{
                        authenticationError(self)
                    }else{
                        self.alert(message: checkNetworkError(response.result.error!))
                    }
            }
        }else{
            self.alert(message: "It seems that there is no Internet connection.")
        }
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showClientDatabaseDetails"{
            let destVc = segue.destination as! ClientDatabaseDetailsTableViewController
            let index = tblView.indexPathForSelectedRow
            if segmentClients.selectedSegmentIndex == 0{
                destVc.clientId = arrActiveClient[(index?.row)!].id!
            }else{
                destVc.clientId = arrInactiveClient[(index?.row)!].id!
            }
        }else if segue.identifier == "ipadShowClientDatabaseDetails"{
            let destVc = segue.destination as?
            UINavigationController
            let detailViewController = destVc?.topViewController as! ClientDatabaseDetailsTableViewController
            let index = tblView.indexPathForSelectedRow
            if segmentClients.selectedSegmentIndex == 0{
                if arrActiveClient.count != 0{
                    detailViewController.clientId = arrActiveClient[(index?.row)!].id!
                }else{
                    detailViewController.clientId = 0
                }
            }else{
                if arrInactiveClient.count != 0{
                    detailViewController.clientId = arrInactiveClient[(index?.row)!].id!
                }else{
                    detailViewController.clientId = 0
                }
            }
        }
    }
    

}
//EmptyDataSet
extension ClientDatabaseListViewController: DZNEmptyDataSetSource, DZNEmptyDataSetDelegate{
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        let text="No Records Found"
        let attributes = [NSAttributedStringKey.font: UIFont.boldSystemFont(ofSize: 18.0), NSAttributedStringKey.foregroundColor: UIColor.darkGray]
        return NSAttributedString(string: text, attributes: attributes)
    }
}
//Table View
extension ClientDatabaseListViewController: UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if segmentClients.selectedSegmentIndex == 0 {
            return arrActiveClient.count
        }else{
            return arrInactiveClient.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CommonCell", for: indexPath)as! CommonCell
        if segmentClients.selectedSegmentIndex == 0 {
            let obj = arrActiveClient[indexPath.row]
            cell.configCell(obj.clientName)
        }else{
            let obj = arrInactiveClient[indexPath.row]
            cell.configCell(obj.clientName)
        }
        
        cell.accessoryType = .disclosureIndicator
        if DeviceType(){
            cell.selectionStyle = .none
        }
        else{
            cell.selectionStyle = .default
        }

        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if DeviceType(){
            self.performSegue(withIdentifier: "showClientDatabaseDetails", sender: nil)
        }else{
            self.performSegue(withIdentifier: "ipadShowClientDatabaseDetails", sender: nil)
        }
    }
}
