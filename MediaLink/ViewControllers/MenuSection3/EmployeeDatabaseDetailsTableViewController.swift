//
//  EmployeeDatabaseDetailsTableViewController.swift
//  MediaLink
//
//  Created by Naveen on 3/6/18.
//  Copyright © 2018 Naveen. All rights reserved.
//

import UIKit
import MessageUI
import Alamofire
import AlamofireObjectMapper
import SwiftyJSON
import RKTagsView

class EmployeeDatabaseDetailsTableViewController: UITableViewController,MFMailComposeViewControllerDelegate {

    @IBOutlet weak var lblFullname: UILabel!
    @IBOutlet weak var btnEmail: UIButton!
    @IBOutlet weak var btnPhone: UIButton!
    @IBOutlet weak var tagsClientsView: RKTagsView!
    
    var objEmployeeDetails:Employee!
    var isFromOtherView = false // true when from other view controller
    var empId = 0
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        
        tagsClientsView.allowsMultipleSelection = false
        tagsClientsView.editable = false
        tagsClientsView.scrollsHorizontally = false
        tagsClientsView.delegate = self
        tagsClientsView.font = UIFont.systemFont(ofSize: 14)
        
        if isFromOtherView {
            let barBtnClose = UIBarButtonItem(barButtonSystemItem: .stop, target: self, action: #selector(clickedClose))
            navigationItem.leftBarButtonItem = barBtnClose
        }
        self.tableView.tableFooterView = UIView()
        getDataFromServer()
    }
    override func viewDidAppear(_ animated: Bool) {
        self.tableView.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @objc func clickedClose(){
        self.dismiss(animated: true, completion: nil)
    }
    func setData(_ objEMpDetails : Employee){
        lblFullname.text = objEmployeeDetails.firstName!+" "+objEmployeeDetails.lastName!
        if let phoneStr = objEMpDetails.phone {
            btnPhone.setTitle(phoneStr, for: .normal)
        }
        if let emailStr = objEMpDetails.email {
            btnEmail.setTitle(emailStr, for: .normal)
        }
        if objEMpDetails.clientList?.count != 0{
            for objItem in objEMpDetails.clientList!{
                tagsClientsView.addTag(objItem.clientName!)
            }
        }
        self.tableView.reloadData()
    }
    
    //MFMailComposeViewController
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }
    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }

    
    @IBAction func clickedPhoneno(_ sender: Any) {
        if let phone = objEmployeeDetails.phone {
            callPhone(phone)
        }
        
    }
    
    @IBAction func clickedEmail(_ sender: Any) {
        if let email = objEmployeeDetails.email {
            if MFMailComposeViewController.canSendMail() {
                let mail = MFMailComposeViewController()
                mail.mailComposeDelegate = self
                mail.navigationBar.tintColor = UIColor.white
                mail.setToRecipients(["\(email)"])
                if !DeviceType(){
                    self.modalPresentationStyle = .popover
                }
                present(mail, animated: true)
            } else {
                // show failure alert
            }
        }
    }
    //Server Response
    func getDataFromServer(){
        if Reachability.isConnectedToNetwork(){
            if empId != 0{
                let url = MediaLinkUrls.employeeDatabase+"/"+String(empId)
                let repo = MediaLinkUserDefaults()
                let params = ["token":repo.tocken!]
                activityIndicatorShow(self)
                Alamofire.request(url, method: .get, parameters: params).validate()
                    .responseObject { (response:DataResponse<Employee>) in
                        activityIndicatorHide()
                        if response.result.error == nil{
                            if let serverResponce = response.result.value{
                                self.objEmployeeDetails = serverResponce
                                self.setData(serverResponce)
                            }
                        }else if response.response?.statusCode == 401{
                            authenticationError(self)
                        }else{
                            self.alert(message: checkNetworkError(response.result.error!))
                        }
                }
            }            
        }else{
            self.alert(message: "It seems that there is no Internet connection.")
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
//Tags View
extension EmployeeDatabaseDetailsTableViewController:RKTagsViewDelegate{
    func tagsView(_ tagsView: RKTagsView, shouldSelectTagAt index: Int) -> Bool {
        
        if objEmployeeDetails != nil{
            if let clientId = objEmployeeDetails.clientList![index].id{
                let storyboard = UIStoryboard(name: "MenuSection3", bundle: nil)
                let destVc = storyboard.instantiateViewController(withIdentifier: "ClientDatabaseDetailsTableViewController") as! ClientDatabaseDetailsTableViewController
                destVc.isFromOtherView = true
                destVc.clientId = clientId
                let nav = UINavigationController(rootViewController: destVc)
                if !DeviceType(){
                    self.modalPresentationStyle = .popover
                }
                self.present(nav, animated: true, completion: nil)
            }
        }
        return true
    }
}
