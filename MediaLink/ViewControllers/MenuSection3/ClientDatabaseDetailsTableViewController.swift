//
//  ClientDatabaseDetailsTableViewController.swift
//  MediaLink
//
//  Created by Naveen on 3/6/18.
//  Copyright © 2018 Naveen. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper
import SwiftyJSON
import AlamofireImage
import RKTagsView

class ClientDatabaseDetailsTableViewController: UITableViewController {
    @IBOutlet weak var lblClient: UILabel!
    @IBOutlet weak var lblCategories: UILabel!
    @IBOutlet weak var lblClientTeam: UILabel!
    @IBOutlet weak var lblInternalDes: UILabel!
    @IBOutlet weak var lblRationaleAgencies: UILabel!
    @IBOutlet weak var lblRationaleBrands: UILabel!
    @IBOutlet weak var lblRationalePublishers: UILabel!
    @IBOutlet weak var contactsTagView: RKTagsView!
    
    var isFromOtherView = false // true when from other view controller
    var objClientDatabase:ClientDatabase!
    var clientId = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        
        //tagsView
        contactsTagView.allowsMultipleSelection = false
        contactsTagView.editable = false
        contactsTagView.scrollsHorizontally = false
        contactsTagView.delegate = self
        contactsTagView.font = UIFont.systemFont(ofSize: 14)
        
        if isFromOtherView {
            let barBtnClose = UIBarButtonItem(barButtonSystemItem: .stop, target: self, action: #selector(clickedClose))
            navigationItem.leftBarButtonItem = barBtnClose
        }
        
        self.tableView.tableFooterView = UIView()
        getDataFromServer()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewDidAppear(_ animated: Bool) {
        self.tableView.reloadData()
    }
    @objc func clickedClose(){
        self.dismiss(animated: true, completion: nil)
    }
    func setData(_ objClient:ClientDatabase){
        if let clientStr = objClient.clientName {
            lblClient.text = clientStr
        }
        var categoriesStr = ""
        for objItem in objClient.categories!{
            if categoriesStr.isBlank{
                categoriesStr = objItem.categoryName!
            }else{
                categoriesStr = categoriesStr+", "+objItem.categoryName!
            }
        }
        lblCategories.text = categoriesStr
        
        if objClient.clientTeam?.count != 0{
            var arrMeetingStr = [String]()
            for objItem in objClient.clientTeam!{
                arrMeetingStr.append(objItem.firstName!+" "+objItem.lastName!)
            }
            let str = arrMeetingStr.joined(separator: ", ")
            lblClientTeam.text = str
        }
        if objClient.clientContacts?.count != 0{
            for objItem in objClient.clientContacts!{
                contactsTagView.addTag(objItem.firstName!+" "+objItem.lastName!)
            }
        }
        if let internalDes = objClient.internalDescription{
            lblInternalDes.text = internalDes
        }
        if let rationaleAgencies = objClient.rationaleForAgencies{
            lblRationaleAgencies.text = rationaleAgencies
        }
        if let rationaleBrands = objClient.rationaleForBrands{
            lblRationaleBrands.text = rationaleBrands
        }
        if let rationalePublishers = objClient.rationaleForPublishers{
            lblRationalePublishers.text = rationalePublishers
        }
        self.tableView.reloadData()
    }
    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }

    //Server Response
    func getDataFromServer(){
        if Reachability.isConnectedToNetwork(){
            if clientId != 0{
                let url = MediaLinkUrls.clientDatabase+"/"+String(clientId)
                let repo = MediaLinkUserDefaults()
                let params = ["token":repo.tocken!]
                activityIndicatorShow(self)
                Alamofire.request(url, method: .get, parameters: params).validate()
                    .responseObject { (response:DataResponse<ClientDatabase>) in
                        activityIndicatorHide()
                        if response.result.error == nil{
                            if let serverResponce = response.result.value{
                                self.setData(serverResponce)
                                self.objClientDatabase = serverResponce
                            }
                        }else if response.response?.statusCode == 422{
                        }else if response.response?.statusCode == 401{
                            authenticationError(self)
                        }else{
                            self.alert(message: checkNetworkError(response.result.error!))
                        }
                }
            }            
        }else{
            self.alert(message: "It seems that there is no Internet connection.")
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
//Tags View
extension ClientDatabaseDetailsTableViewController: RKTagsViewDelegate{
    // Tag Touch Action
    func tagsView(_ tagsView: RKTagsView, shouldSelectTagAt index: Int) -> Bool {
        if objClientDatabase != nil{
            if let cId = objClientDatabase.clientContacts![index].id{
                let storyboard = UIStoryboard(name: "MenuSection2", bundle: nil)
                let destVc = storyboard.instantiateViewController(withIdentifier: "ContactDatabaseDetailsTableViewController") as! ContactDatabaseDetailsTableViewController
                destVc.isFromOtherView = true
                destVc.contactId = cId
                let nav = UINavigationController(rootViewController: destVc)
                if !DeviceType(){
                    self.modalPresentationStyle = .popover
                }
                self.present(nav, animated: true, completion: nil)
            }
        }
        return true
    }
}
