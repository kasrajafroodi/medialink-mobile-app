//
//  EmplyeeDatabaseViewController.swift
//  MediaLink
//
//  Created by Naveen on 3/6/18.
//  Copyright © 2018 Naveen. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper
import SwiftyJSON
import DZNEmptyDataSet

class EmplyeeDatabaseViewController: UIViewController,SlideMenuControllerDelegate,UISplitViewControllerDelegate  {

    @IBOutlet weak var tblView: UITableView!
    
    var arrEmployee = [Employee]()
    var offsetValue = 0
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.tblView.register(UINib(nibName: "CommonCell", bundle: nil), forCellReuseIdentifier: "CommonCell")
        self.tblView.tableFooterView = UIView()
        self.tblView.layoutMargins = UIEdgeInsets.zero
        self.tblView.separatorInset = UIEdgeInsets.zero
        self.tblView.estimatedRowHeight=180
        self.tblView.rowHeight=UITableViewAutomaticDimension
        self.setNavigationBarItem()
        splitViewController?.delegate=self
        getDataFromServer(offsetValue,true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //For Ipad
    func selectFirstRowIpad(){
        if arrEmployee.count != 0{
            let index = IndexPath(row: 0, section: 0)
            self.tblView.selectRow(at: index, animated: false, scrollPosition: .top)
        }
        self.performSegue(withIdentifier: "ipadShowEmployeeDatabaseDetails", sender: nil)
    }
    //menu Display
    func slideNavigationControllerShouldDisplayLeftMenu() -> Bool {
        return true
    }
    //Split View
    func splitViewController(_ svc: UISplitViewController, shouldHide vc: UIViewController, in orientation: UIInterfaceOrientation) -> Bool {
        return false
    }
    //ScrollView
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        //Bottom Refresh
        if self.arrEmployee.count > 0 {
            if scrollView == tblView{
                let currentOffset = Int(scrollView.contentOffset.y)
                let maximumOffset = Int(scrollView.contentSize.height - scrollView.frame.size.height)
                if maximumOffset - currentOffset <= -10 {
                    offsetValue = offsetValue + CommonStrings.limit
                    getDataFromServer(offsetValue,false)
                }
            }
        }
    }
    
    @IBAction func clickedReload(_ sender: Any) {
        
        if !Reachability.isConnectedToNetwork() {
            self.alert(message: "It seems that there is no Internet connection.")
            return
        }
        
        self.arrEmployee = []
        
        offsetValue = 0
        getDataFromServer(offsetValue,true)
    }
    
    //Server Response
    func getDataFromServer(_ offSet:Int,_ isShowLoading : Bool){
        if Reachability.isConnectedToNetwork() {
            let url = MediaLinkUrls.employeeDatabase
            let repo = MediaLinkUserDefaults()
            let params = ["limit":String(CommonStrings.limit),
                          "offset":String(offSet),
                          "token":repo.tocken!]
            if isShowLoading{
                activityIndicatorShow(self)
            }
            Alamofire.request(url, method: .get, parameters: params).validate()
                .responseArray { (response:DataResponse<[Employee]>) in
                    activityIndicatorHide()
                    if response.result.error == nil{
                        if let serverResponce = response.result.value{
                            self.arrEmployee.append(contentsOf:serverResponce)
                            self.tblView.reloadData()
                            if !DeviceType() && isShowLoading{
                                self.selectFirstRowIpad()
                            }
                        }
                    }else if response.response?.statusCode == 422{
                        if let data = response.data {
                            let responseJSON = try! JSON(data: data)
                            if responseJSON["limit"].exists(){
                                if let message = responseJSON["limit"][0].string {
                                    self.alert(message: message)
                                }
                            }
                            else if responseJSON["offset"].exists(){
                                if let message = responseJSON["offset"][0].string {
                                    self.alert(message: message)
                                }
                            }
                        }
                    }else if response.response?.statusCode == 401{
                        authenticationError(self)
                    }else{
                        self.alert(message: checkNetworkError(response.result.error!))
                    }
            }
        }else{
            self.alert(message: "It seems that there is no Internet connection.")
        }
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showEmployeeDatabaseDetails"{
            let destVc = segue.destination as! EmployeeDatabaseDetailsTableViewController
            let index = tblView.indexPathForSelectedRow
            destVc.empId = arrEmployee[(index?.row)!].id!
        }else if segue.identifier == "ipadShowEmployeeDatabaseDetails"{
            let destVc = segue.destination as?
            UINavigationController
            let detailViewController = destVc?.topViewController as! EmployeeDatabaseDetailsTableViewController
            let index = tblView.indexPathForSelectedRow
            if arrEmployee.count != 0{
                detailViewController.empId = arrEmployee[(index?.row)!].id!
            }else{
                detailViewController.empId = 0
            }
        }
    }
    

}
//EmptyDataSet
extension EmplyeeDatabaseViewController: DZNEmptyDataSetSource, DZNEmptyDataSetDelegate{
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        let text="No Records Found"
        let attributes = [NSAttributedStringKey.font: UIFont.boldSystemFont(ofSize: 18.0), NSAttributedStringKey.foregroundColor: UIColor.darkGray]
        return NSAttributedString(string: text, attributes: attributes)
    }
}
//Table View
extension EmplyeeDatabaseViewController: UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrEmployee.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CommonCell", for: indexPath)as! CommonCell
        let objEmp = arrEmployee[indexPath.row]
        cell.configCell(objEmp.firstName!+" "+objEmp.lastName!)
        cell.accessoryType = .disclosureIndicator
        if DeviceType(){
            cell.selectionStyle = .none
        }
        else{
            cell.selectionStyle = .default
        }
    
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if DeviceType(){
            self.performSegue(withIdentifier: "showEmployeeDatabaseDetails", sender: nil)
        }else{
            self.performSegue(withIdentifier: "ipadShowEmployeeDatabaseDetails", sender: nil)
        }
    }
}
