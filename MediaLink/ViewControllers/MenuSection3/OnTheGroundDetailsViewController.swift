//
//  OnTheGroundDetailsViewController.swift
//  MediaLink
//
//  Created by Naveen on 3/6/18.
//  Copyright © 2018 Naveen. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper
import SwiftyJSON
import AlamofireImage
import RKTagsView

class OnTheGroundDetailsViewController: UIViewController,SelectedDropdownItemsDelegate,EmployeeDropDownSelectionDelegate {
    
    @IBOutlet weak var inpDate: UITextField!
    @IBOutlet weak var inpTime: UITextField!
    @IBOutlet weak var lblCompany: UILabel!
    @IBOutlet weak var btnCompany: UIButton!
    @IBOutlet weak var schedulerTagsView: RKTagsView!
    @IBOutlet weak var tblTargetAttendies: UITableView!
    @IBOutlet weak var tblClientAttendies: UITableView!
    @IBOutlet weak var hightTblTargetAttendies: NSLayoutConstraint!
    @IBOutlet weak var hightTblClientAttendees: NSLayoutConstraint!
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var btnClient: UIButton!
    @IBOutlet weak var btnMeetingsCover: UIButton!
    @IBOutlet weak var btnOutreach: UIButton!
    @IBOutlet weak var textViewRequestedNotes: UITextView!
    @IBOutlet weak var textViewMeetingNotes: UITextView!
    
    @IBOutlet weak var inpDuration: UITextField!
    
    var objSelItem = SelectedFilter()
    var selectedIndex = 0
    
    var objMeeting:Meeting!
    var arrTargetAttendees = [Attendees]()
    var arrClientAttendees = [Attendees]()
    var objMeetingDataDetails:ViewMeetingDetails!
    var durationPicker : UIPickerView = UIPickerView()
    var arrPickerHours = [Int]()
    var arrPickerMinutes = [Int]()
    
    var reloadDelegate :ReloadDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        //tagsView
        schedulerTagsView.allowsMultipleSelection = false
        schedulerTagsView.editable = false
        schedulerTagsView.scrollsHorizontally = false
        schedulerTagsView.delegate = self
        schedulerTagsView.font = UIFont.systemFont(ofSize: 14)
        
        //textview
        textViewRequestedNotes.contentInset = UIEdgeInsets.zero
        textViewRequestedNotes.textContainer.lineFragmentPadding = 0
        textViewMeetingNotes.contentInset = UIEdgeInsets.zero
        textViewMeetingNotes.textContainer.lineFragmentPadding = 0
        
        self.tblTargetAttendies.tableFooterView = UIView()
        self.tblTargetAttendies.register(UINib(nibName: "AttendeesViewCell", bundle: nil), forCellReuseIdentifier: "AttendeesViewCell")
        self.tblTargetAttendies.register(UINib(nibName: "MoreViewCell", bundle: nil), forCellReuseIdentifier: "MoreViewCell")
        self.tblClientAttendies.tableFooterView = UIView()
        self.tblClientAttendies.register(UINib(nibName: "AttendeesViewCell", bundle: nil), forCellReuseIdentifier: "AttendeesViewCell")
        self.tblClientAttendies.register(UINib(nibName: "MoreViewCell", bundle: nil), forCellReuseIdentifier: "MoreViewCell")
        let datePickerView  = UIDatePicker()
        datePickerView.datePickerMode = UIDatePickerMode.date
        inpDate.inputView = datePickerView
        datePickerView.addTarget(self, action: #selector(handleDatePicker), for: UIControlEvents.valueChanged)
        
        for i in 0...24 {
            arrPickerHours.append(i)
        }
        
        for i in 0...59 {
            arrPickerMinutes.append(i)
        }
        
        let timePickerView  = UIDatePicker()
        timePickerView.datePickerMode = UIDatePickerMode.time
        inpTime.inputView = timePickerView
        timePickerView.addTarget(self, action: #selector(handleTimePicker), for: UIControlEvents.valueChanged)
        
        durationPicker.delegate = self
        durationPicker.dataSource = self
        inpDuration.inputView = durationPicker
        
        getDataFromServer()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewDidAppear(_ animated: Bool) {
        self.view.layoutIfNeeded()
    }
    
    override func viewWillLayoutSubviews() {
        self.hightTblTargetAttendies.constant = self.tblTargetAttendies.contentSize.height
        self.hightTblClientAttendees.constant = self.tblClientAttendies.contentSize.height
        self.view.layoutIfNeeded()
        super.viewDidLayoutSubviews()
    }
    
    //Date Picker
    @objc func handleDatePicker(sender: UIDatePicker) {
        objSelItem.strDate = String(describing: sender.date)
        inpDate.text = DateFormatterDateTimeFullFormated(sender.date)
    }
    //Time Picker
    @objc func handleTimePicker(sender: UIDatePicker) {
        objSelItem.strTime = String(describing: sender.date)
        inpTime.text = timeFormatFromDate(sender.date)
    }
    
    func showEmployeeDatabase(_ employeeId:Int){
        let storyboard = UIStoryboard(name: "MenuSection3", bundle: nil)
        let destVc = storyboard.instantiateViewController(withIdentifier: "EmployeeDatabaseDetailsTableViewController") as! EmployeeDatabaseDetailsTableViewController
        destVc.isFromOtherView = true
        destVc.empId = employeeId
        let nav = UINavigationController(rootViewController: destVc)
        if !DeviceType(){
            self.modalPresentationStyle = .popover
        }
        self.present(nav, animated: true, completion: nil)
    }
    
    
    func setData(_ objMeetingDetails : ViewMeetingDetails){
        if let dateStr = objMeetingDetails.date{
            if dateStr != "" {
                inpDate.text = dateFormatWithDay(dateStr)
            }
        }
        if let timeStr = objMeetingDetails.time {
            if timeStr != "" {
                inpTime.text = timeFormatWithDay(timeStr)
            }
        }
        if let durationStr = objMeetingDetails.duration {
            inpDuration.text  = durationStr
        }
        if let locationStr = objMeetingDetails.location{
            lblLocation.text = locationStr
        }
        if let statusStr = objMeetingDetails.requestStatus{
            lblStatus.text = statusStr
        }
        if let companyStr = objMeetingDetails.company{
            lblCompany.text = companyStr
            btnCompany.setTitle(companyStr, for: .normal)
            if let companyColor = objMeeting.companyColor{
                switch companyColor{
                case 0:
                    btnCompany.backgroundColor = UIColor.clear
                case 1:
                    btnCompany.backgroundColor = UIColor.colorNotes
                case 2:
                    btnCompany.backgroundColor = UIColor.colorNoNotes
                default:
                    break
                }                
            }
        }
        if let clientStr = objMeetingDetails.clientName {
            btnClient.setTitle(clientStr, for: .normal)
            if let clientColor = objMeeting.clientColor{
                switch clientColor{
                case 0:
                    btnClient.backgroundColor = UIColor.clear
                case 1:
                    btnClient.backgroundColor = UIColor.colorNotes
                case 2:
                    btnClient.backgroundColor = UIColor.colorNoNotes
                default:
                    break
                }
                
            }
            
        }
        if let meetingCoverStr = objMeetingDetails.meetingCoverName{
            btnMeetingsCover.setTitle(meetingCoverStr, for: .normal)
            
            if let meetingColor = objMeeting.meetingColor{
                switch meetingColor{
                case 0:
                    btnMeetingsCover.backgroundColor = UIColor.clear
                case 1:
                    btnMeetingsCover.backgroundColor = UIColor.colorNotes
                case 2:
                    btnMeetingsCover.backgroundColor = UIColor.colorNoNotes
                default:
                    break
                }
                
            }
        }
        if let outreachLeadStr = objMeetingDetails.outreachLeadName{
            btnOutreach.setTitle(outreachLeadStr, for: .normal)
        }
        if objMeetingDetails.employees?.count != 0{
            for objItem in objMeetingDetails.employees!{
                self.schedulerTagsView.addTag(objItem.firstName!+" "+objItem.lastName!)
            }
        }
        if let requstNoteStr = objMeetingDetails.quickNote{
            if !requstNoteStr.isBlank{
                textViewRequestedNotes.textColor = UIColor.black
                textViewRequestedNotes.tag=1
                textViewRequestedNotes.text = requstNoteStr
            }
        }
        if let meetingNoteStr = objMeetingDetails.meetingNotes {
            if !meetingNoteStr.isBlank{
                textViewMeetingNotes.textColor = UIColor.black
                textViewMeetingNotes.tag=1
                textViewMeetingNotes.text = meetingNoteStr
            }
        }
        if objMeetingDetails.targetAttendees?.count != 0{
            self.arrTargetAttendees = objMeetingDetails.targetAttendees!
            self.tblTargetAttendies.reloadData()
        }
        if objMeetingDetails.clientAttendees?.count != 0{
            self.arrClientAttendees = objMeetingDetails.clientAttendees!
            self.tblClientAttendies.reloadData()
        }
        self.view.layoutIfNeeded()
        if objMeetingDetails.clientName != nil {
            btnClient.frame.size.width = btnClient.frame.size.width + 10
        }
        if objMeetingDetails.meetingCoverName != nil {
            btnMeetingsCover.frame.size.width = btnMeetingsCover.frame.size.width + 10
        }
        if objMeetingDetails.company != nil {
            btnCompany.frame.size.width = btnCompany.frame.size.width + 10
        }
    }
    //Delegate Selecteditem
    func didFilterDropdownSelectedItem(_ arrSelectedIndex: [Int], _ arrSelectedStrings: [String]) {
        switch selectedIndex {
        case 1:
            objSelItem.arrLocation = arrSelectedIndex
            objMeetingDataDetails.locationId = arrSelectedIndex[0]
            lblLocation.text = arrSelectedStrings[0]
        case 2:
            objSelItem.arrPartyStatus = arrSelectedIndex
            objMeetingDataDetails.outreachRequestStatusId = arrSelectedIndex[0]
            lblStatus.text = arrSelectedStrings[0]
        
        case 3:
            objSelItem.arrMeetingCover = arrSelectedIndex
            objMeetingDataDetails.meetingCover = arrSelectedIndex[0]
            btnMeetingsCover.setTitle(arrSelectedStrings[0], for: .normal)
        case 4:
            objSelItem.arrOutreach = arrSelectedIndex
            objMeetingDataDetails.outreachLead = arrSelectedIndex[0]
            btnOutreach.setTitle(arrSelectedStrings[0], for: .normal)
        
        default:
            break
        }
        self.view.layoutIfNeeded()
        if objMeetingDataDetails.meetingCoverName != nil {
            btnMeetingsCover.frame.size.width = btnMeetingsCover.frame.size.width + 10
        }
    }
    func selectedEmployee(_ arrObjEmp: [Employee]) {
        objMeetingDataDetails.employees = arrObjEmp
        if objMeetingDataDetails.employees?.count != 0{
            for objItem in objMeetingDataDetails.employees!{
                self.schedulerTagsView.addTag(objItem.firstName!+" "+objItem.lastName!)
            }
        }
        self.view.layoutIfNeeded()
    }
//    gusture actions
    
    
    @IBAction func clickedChangeLocation(_ sender: Any) {
        if objMeetingDataDetails != nil{
            self.selectedIndex = 1
            if let locId = objMeetingDataDetails.locationId{
                objSelItem.arrLocation = [locId]
            }
            self.performSegue(withIdentifier: "showChangeLocation", sender: nil)
        }
    }
    @IBAction func clickedChangeStatus(_ sender: Any) {
        if objMeetingDataDetails != nil{
            self.selectedIndex = 2
            if let reqStatusId = objMeetingDataDetails.outreachRequestStatusId{
                objSelItem.arrPartyStatus = [reqStatusId]
            }
            self.performSegue(withIdentifier: "showStatusDropdown", sender: nil)
        }
    }
    @IBAction func clickedChangeMeetingsCover(_ sender: Any) {
        if objMeetingDataDetails != nil{
            self.selectedIndex = 3
            if let meetingCoverId = objMeetingDataDetails.meetingCover{
                objSelItem.arrMeetingCover = [meetingCoverId]
            }
            self.performSegue(withIdentifier: "showEmployeeDropdown", sender: nil)
        }
    }
    @IBAction func clickedChangeOutreac(_ sender: Any) {
        if objMeetingDataDetails != nil{
            self.selectedIndex = 4
            if let outReach = objMeetingDataDetails.outreachLead{
                objSelItem.arrOutreach = [outReach]
            }
            self.performSegue(withIdentifier: "showEmployeeDropdown", sender: nil)
        }
    }
    @IBAction func clickedAddScheduler(_ sender: Any) {
        if objMeetingDataDetails != nil{
            self.selectedIndex = 5
            if objMeetingDataDetails.employees?.count != 0 && objSelItem.arrScheduler.count == 0{
                var arrMeeting = [Int]()
                for objItem in objMeetingDataDetails.employees!{
                    arrMeeting.append(objItem.id!)
                }
                objSelItem.arrScheduler = arrMeeting
            }
            self.performSegue(withIdentifier: "showEmployeeDropdown", sender: nil)
        }
    }
    //Actions
    @IBAction func clickedCompany(_ sender: Any) {
        /*if objMeetingDataDetails != nil{
            if objMeetingDataDetails.company != nil{
                switch btnCompany.tag {
                case 0:
                    btnCompany.tag = 1
                    btnCompany.backgroundColor = UIColor.colorNotes
                case 1:
                    btnCompany.tag = 2
                    btnCompany.backgroundColor = UIColor.colorNoNotes
                case 2:
                    btnCompany.tag = 0
                    btnCompany.backgroundColor = UIColor.clear
                default:
                    break
                }
            }
        }*/
    }
    @IBAction func clicckedClient(_ sender: Any) {
        /*if objMeetingDataDetails != nil{
            if let clientId = objMeetingDataDetails.clientId{
                let storyboard = UIStoryboard(name: "MenuSection3", bundle: nil)
                let destVc = storyboard.instantiateViewController(withIdentifier: "ClientDatabaseDetailsTableViewController") as! ClientDatabaseDetailsTableViewController
                destVc.isFromOtherView = true
                destVc.clientId = clientId
                let nav = UINavigationController(rootViewController: destVc)
                if !DeviceType(){
                    self.modalPresentationStyle = .popover
                }
                self.present(nav, animated: true, completion: nil)
            }
        }*/
        if objMeetingDataDetails != nil{
            if let clientId = objMeetingDataDetails.clientId{
                let storyboard = UIStoryboard(name: "MenuSection3", bundle: nil)
                let destVc = storyboard.instantiateViewController(withIdentifier: "ClientDatabaseDetailsTableViewController") as! ClientDatabaseDetailsTableViewController
                destVc.isFromOtherView = true
                destVc.clientId = clientId
                let nav = UINavigationController(rootViewController: destVc)
                if !DeviceType(){
                    self.modalPresentationStyle = .popover
                }
                self.present(nav, animated: true, completion: nil)
            }
        }
    }
    @IBAction func clickedMeetingCover(_ sender: Any) {
        /*if objMeetingDataDetails != nil{
            if objMeetingDataDetails.meetingCover != nil{
                switch btnMeetingsCover.tag {
                case 0:
                    btnMeetingsCover.tag = 1
                    btnMeetingsCover.backgroundColor = UIColor.colorNotes
                case 1:
                    btnMeetingsCover.tag = 2
                    btnMeetingsCover.backgroundColor = UIColor.colorNoNotes
                case 2:
                    btnMeetingsCover.tag = 0
                    btnMeetingsCover.backgroundColor = UIColor.clear
                default:
                    break
                }
            }
        }*/
        if objMeetingDataDetails != nil{
            if let meetingCoverId = objMeetingDataDetails.meetingCover{
                showEmployeeDatabase(meetingCoverId)
            }
        }
    }
    @IBAction func clickedOutreach(_ sender: Any) {
        if objMeetingDataDetails != nil{
            if let outreachId = objMeetingDataDetails.outreachLead{
                showEmployeeDatabase(outreachId)
            }
        }
    }
    @IBAction func clickedSave(_ sender: Any) {
        dismissKeyboard()
        if Reachability.isConnectedToNetwork(){
            if objMeetingDataDetails != nil{
                let url = MediaLinkUrls.updateMeetingDetails+String(objMeetingDataDetails.id!)
                let repo = MediaLinkUserDefaults()
                var params = ["user_id":repo.userId,
                              "token":repo.tocken!]
                if let locId = objMeetingDataDetails.locationId{
                    params["location_id"] = String(locId)
                }
                if let outReachId = objMeetingDataDetails.outreachRequestStatusId{
                    params["outreach_request_status_id"] = String(outReachId)
                }
                if let meetingCoverId = objMeetingDataDetails.meetingCover{
                    params["meeting_cover"] = String(meetingCoverId)
                }
                if let outReachId = objMeetingDataDetails.outreachLead{
                    params["outreach_lead"] = String(outReachId)
                }
                if textViewRequestedNotes.tag != 0{
                    params["quick_note"] = textViewRequestedNotes.text!
                }
                if textViewMeetingNotes.tag != 0{
                    params["meeting_notes"] = textViewMeetingNotes.text!
                }
                if objMeetingDataDetails.employees?.count != 0{
                    var arrEmpIds = [Int]()
                    for objItem in objMeetingDataDetails.employees!{
                        arrEmpIds.append(objItem.id!)
                    }
                    params["scheduler_id"] = convertToJson(arrEmpIds)
                }
                if !objSelItem.strDate.isEmpty{
                    params["date"] = DateFormatterDateTimeFull(objSelItem.strDate)
                }
                if !objSelItem.strTime.isEmpty{
                    params["time"] = timeFormatFromDateStringToServer(objSelItem.strTime)
                }
                if !objSelItem.strHours.isBlank {
                    params["hours"] = objSelItem.strHours
                }
                if !objSelItem.strMinutes.isBlank {
                    params["minutes"] = objSelItem.strMinutes
                }
                Alamofire.request(url, method: .post, parameters: params).validate()
                    .responseJSON { response in
                        activityIndicatorHide()
                        if response.result.error == nil{
                            if let data = response.data {
                                let responseJSON = try! JSON(data: data)
                                if responseJSON["message"].exists(){
                                    if let message = responseJSON["message"].string{
                                        self.alert(message: message)
                                        self.reloadDelegate?.reloadData()
                                    }
                                }
                            }
                        }else if response.response?.statusCode == 401{
                            authenticationError(self)
                        }else{
                            self.alert(message: checkNetworkError(response.result.error!))
                        }
                }
            }
        }else{
            self.alert(message: "It seems that there is no Internet connection.")
        }
    }
    //Server Response
    func getDataFromServer(){
        if Reachability.isConnectedToNetwork(){
            if objMeeting != nil{
                let url = MediaLinkUrls.viewMeetingDetails+String(objMeeting.id!)
                let repo = MediaLinkUserDefaults()
                let params = ["token":repo.tocken!]
                activityIndicatorShow(self)
                Alamofire.request(url, method: .get, parameters: params).validate()
                    .responseObject { (response:DataResponse<ViewMeetingDetails>) in
                        activityIndicatorHide()
                        if response.result.error == nil{
                            if let serverResponce = response.result.value{
                                self.objMeetingDataDetails = serverResponce
                                self.setData(serverResponce)
                            }
                        }else if response.response?.statusCode == 401{
                            authenticationError(self)
                        }else{
                            self.alert(message: checkNetworkError(response.result.error!))
                        }
                }
            }
        }else{
            self.alert(message: "It seems that there is no Internet connection.")
        }
    }
    
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showStatusDropdown"{
            let destVc = segue.destination as! StatusDropdownListViewController
            destVc.delegateSelectedItem = self
            destVc.arrSelectedIndex = objSelItem.arrPartyStatus
            destVc.isFromOntheGroundEdit = true
        }else if segue.identifier == "showChangeLocation"{
            let destVc = segue.destination as! LocationDropdownListViewController
            destVc.delegateSelectedItem = self
            destVc.arrSelectedIndex = objSelItem.arrLocation
            destVc.isFromOntheGroundEdit = true
        }else if segue.identifier == "showEmployeeDropdown"{
            let destVc = segue.destination as! EmployeeDropDownListViewController 
            switch selectedIndex{
            case 3:
                destVc.type = 4
                destVc.delegateSelectedItem = self
                destVc.arrSelectedIndex = objSelItem.arrMeetingCover
                destVc.isFromOntheGroundEdit = true
            case 4:
                destVc.type = 2
                destVc.delegateSelectedItem = self
                destVc.arrSelectedIndex = objSelItem.arrOutreach
                destVc.isFromOntheGroundEdit = true
            case 5:
                destVc.delegateEmployeeSelection = self
                destVc.arrSelectEmplyee = objMeetingDataDetails.employees!
                destVc.type = 5
                destVc.isFromOntheGroundEdit = false
            default:
                break
            }
        }
    }
}
extension OnTheGroundDetailsViewController:UITextViewDelegate{
    func textViewDidBeginEditing(_ textView: UITextView){
        if textView.tag == 0 {
            textView.text=""
            textView.tag=1
            textView.textColor=UIColor.black
        }
    }
    func textViewDidEndEditing(_ textView: UITextView){
        if textView.text.count == 0 {
            if textView == textViewMeetingNotes{
                textView.text="Meeting Notes"
            }else if textView == textViewRequestedNotes{
                textView.text="Requested Notes"
            }
            textView.tag=0
            textView.textColor=UIColor.hex(hexStr: "C7C7CC", alpha: 1)
        }
    }
}
//Tags View
extension OnTheGroundDetailsViewController: RKTagsViewDelegate{
    func tagsView(_ tagsView: RKTagsView, shouldSelectTagAt index: Int) -> Bool {
        if let empId = objMeetingDataDetails.employees![index].id{
            showEmployeeDatabase(empId)
        }
        return true
    }
}

// MARK:- UIPicker

extension OnTheGroundDetailsViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 2
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if component == 0 {
            return arrPickerHours.count
        }else {
            return arrPickerMinutes.count
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if component == 0{
            return "\(arrPickerHours[row]) Hours"
        }else {
            return "\(arrPickerMinutes[row]) Minutes"
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.view.endEditing(true)
        let selectedHours = pickerView.selectedRow(inComponent: 0)
        let selectedMinutes = pickerView.selectedRow(inComponent: 1)
        objSelItem.strHours = "\(arrPickerHours[selectedHours])"
        objSelItem.strMinutes = "\(arrPickerMinutes[selectedMinutes])"
        inpDuration.text = "\(arrPickerHours[selectedHours]) Hours \(arrPickerMinutes[selectedMinutes]) Minutes"
    }
}

// MARK:- UITableView

extension OnTheGroundDetailsViewController: UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tblTargetAttendies{
            if arrTargetAttendees.count > 3{
                return 4
            }
            return arrTargetAttendees.count
        }else{
            if arrClientAttendees.count > 3{
                return 4
            }
            return arrClientAttendees.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AttendeesViewCell", for: indexPath)as! AttendeesViewCell
        switch indexPath.row {
        case 0,1,2:
            var objAttendee:Attendees!
            if tableView == tblTargetAttendies{
                objAttendee = arrTargetAttendees[indexPath.row]
            }else{
                objAttendee = arrClientAttendees[indexPath.row]
            }
            cell.configCell(objAttendee.picture, objAttendee.firstName!+" "+objAttendee.lastName!, objAttendee.company)
            
        case 3:
            let cell = tableView.dequeueReusableCell(withIdentifier: "MoreViewCell", for: indexPath)as! MoreViewCell
            return cell
        default:
            break
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0,1,2:
            let storyboard = UIStoryboard(name: "MenuSection2", bundle: nil)
            let destVc = storyboard.instantiateViewController(withIdentifier: "ContactDatabaseDetailsTableViewController") as! ContactDatabaseDetailsTableViewController
            if tableView == tblTargetAttendies{
                destVc.contactId = arrTargetAttendees[indexPath.row].contactId!
            }else{
                destVc.contactId = arrClientAttendees[indexPath.row].contactId!
            }
            self.show(destVc, sender: nil)
        case 3:
            let storyboard = UIStoryboard(name: "Common", bundle: nil)
            let attendeesVC = storyboard.instantiateViewController(withIdentifier: "AttendeesListViewController") as! AttendeesListViewController
            if tableView == tblTargetAttendies{
                attendeesVC.arrAttendees = arrTargetAttendees
                attendeesVC.arrTempAttendees = arrTargetAttendees
            }else{
                attendeesVC.arrAttendees = arrClientAttendees
                attendeesVC.arrTempAttendees = arrClientAttendees
            }
            
            let nav = UINavigationController(rootViewController: attendeesVC)
            if !DeviceType(){
                self.modalPresentationStyle = .popover
            }
            UIApplication.shared.statusBarStyle = .lightContent
            self.present(nav, animated: true, completion: nil)
            
        default:
            break
        }
    }
}
