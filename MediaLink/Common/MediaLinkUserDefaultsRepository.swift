//
//  MediaLinkUserDefaultsRepository.swift
//  MediaLink
//
//  Created by Naveen on 3/9/18.
//  Copyright © 2018 Naveen. All rights reserved.
//

import Foundation
struct MediaLinkUserDefaultsStrings {
    static let loggedUserData = "LoggedUserData"
}
class MediaLinkUserDefaults{
    let defaults = UserDefaults.standard
    
    //set Log data(user)
    func setLoggedUserData(_ loggedData: Login) {
        let JSONString = loggedData.toJSONString(prettyPrint: true)
        defaults.set(JSONString, forKey: MediaLinkUserDefaultsStrings.loggedUserData)
        defaults.synchronize()
    }
    
    var notificationDate: String {
        get{
            return defaults.string(forKey: "NotificationDate")!
        }
        set{
            defaults.set(newValue, forKey: "NotificationDate")
            defaults.synchronize()
        }
    }
    
    //User Id
    var loggedUserId: String? {
        if defaults.value(forKey:MediaLinkUserDefaultsStrings.loggedUserData) as? String != nil{
            let jsonString = defaults.value(forKey:MediaLinkUserDefaultsStrings.loggedUserData) as! String
            let obj = Login(JSONString: jsonString)
            return String(describing: obj!.userId!)
            
        }
        return nil
    }
    var userId:String {
        if loggedUserId != nil {
            return loggedUserId!
        }
        return ""
    }
    
    //Authentication Tocken
    var tocken: String? {
        if defaults.value(forKey:MediaLinkUserDefaultsStrings.loggedUserData) as? String != nil{
            let jsonString = defaults.value(forKey:MediaLinkUserDefaultsStrings.loggedUserData) as! String
            let obj = Login(JSONString: jsonString)
            return obj?.token
            
        }
        return nil
    }
    var authenticationTocken:String {
        if tocken != nil {
            return tocken!
        }
        return ""
    }
    //Check User Login
    var isLoggedIn: Bool {
        if tocken != nil {
            return true
        }
        return false
    }
    //Admin check
    var isAdmin: Bool {
        if defaults.value(forKey:MediaLinkUserDefaultsStrings.loggedUserData) as? String != nil{
            let jsonString = defaults.value(forKey:MediaLinkUserDefaultsStrings.loggedUserData) as! String
            let obj = Login(JSONString: jsonString)
            return (obj?.isAdmin)!
            
        }
        return false
    }
    
    //Email
    var registeredEmail: String? {
        if defaults.value(forKey:MediaLinkUserDefaultsStrings.loggedUserData) as? String != nil{
            let jsonString = defaults.value(forKey:MediaLinkUserDefaultsStrings.loggedUserData) as! String
            let obj = Login(JSONString: jsonString)
            return obj?.email
        }
        return nil
    }
    var email:String {
        if registeredEmail != nil {
            return registeredEmail!
        }
        return ""
    }
    //Logged User Name
    var loggedUsername: String? {
        if defaults.value(forKey:MediaLinkUserDefaultsStrings.loggedUserData) as? String != nil{
            let jsonString = defaults.value(forKey:MediaLinkUserDefaultsStrings.loggedUserData) as! String
            let obj = Login(JSONString: jsonString)
            return obj?.name
        }
        return nil
    }
    var name:String {
        if loggedUsername != nil {
            return loggedUsername!
        }
        return ""
    }
}
