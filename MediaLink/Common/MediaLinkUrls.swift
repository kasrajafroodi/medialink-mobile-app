//
//  MediaLinkUrls.swift
//  MediaLink
//
//  Created by Naveen on 3/9/18.
//  Copyright © 2018 Naveen. All rights reserved.
//

import Foundation
struct MediaLinkUrls{
    
//    192.168.1.10/medialink-php7-v2.0/public/api/login
//    static let root = "http://192.168.1.10/medialink-php7-v2.0/public"
    static let root = "https://jafroodi.medialink.com"
    static let api = root+"/api"
    static let login = api+"/login"
    static let logout = api+"/logout"
    
    static let myScheduleActiveList = api+"/my-schedule-active"
    static let mySchedulePastList = api+"/my-schedule-past"
    
    static let viewMeetingDetails = api+"/meeting-details/"
    static let activeClients = api+"/get-clients"
    static let getConferences = api+"/get-conferences"
    static let locations = api+"/locations"
    static let getEmployees = api+"/get-employees"
    static let getPartyStatus = api+"/get-party-request-status"
    static let getOutreachRequestStatus = api+"/get-outreach-request-status"
    static let getCategories = api+"/get-categories"
    static let getParties = api+"/get-parties"
    
    static let myClientsMeetingListActive = api+"/my-clients-meetings-active"
    static let myClientsMeetingListPast = api+"/my-clients-meetings-past"
    
    static let myClientsEventAccessActive = api+"/my-clients-event-access-active"
    static let myClientsEventAccessPast = api+"/my-clients-event-access-past"
    static let myClientsEventAccess = api+"/my-clients-event-access"
    
    static let eventDatabaseListActive = api+"/events-active"
    static let eventDatabaseListPast = api+"/events-past"
    
    static let contactDatabase = api+"/contacts"
    
    static let clientDatabase = api+"/clients"
    static let clientDatabaseActive = api+"/clients-active"
    static let clientDatabaseInactive = api+"/clients-inactive"
    
    static let employeeDatabase = api+"/employees"
    
    static let meetingsCoverdActive = api+"/meetings-i-covered-active"
    static let meetingsCoverdPast = api+"/meetings-i-covered-past"
    
    static let onTheGroundActive = api+"/on-the-ground-active"
    static let onTheGroundPast = api+"/on-the-ground-past"
    
    static let updateMeetingDetails = api+"/update-meeting-details/"
    
    static let updateMeetingNotes = api+"/update-meeting-notes/"
    static let updateCompanyColor = api+"/update-company-color/"
    static let updateClientColor = api+"/update-client-color/"
    static let updateMeetingCoverColor = api+"/update-meeting-cover-color/"
    static let notification = api+"/notifications"
}
