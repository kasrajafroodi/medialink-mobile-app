//
//  CommonFunctions.swift
//  MediaLink
//
//  Created by Naveen on 2/28/18.
//  Copyright © 2018 Naveen. All rights reserved.
//

import Foundation
import UIKit
import EventKit
import SwiftyJSON
class SaveFilterData {
    var arrClient = [Client]() // contains client
    var arrConference = [Conference]() //contains Conference
    var arrLocation = [Location]() //contains Location
    var arrEmployee = [Employee]() //contains Scheduler or Outreach
    var arrPartyStatus = [PartyStatus]() //contains party Status
    var arrRequestedStatus = [RequestedStatus]() //contains party Status
    var arrCategory = [Categories]() //contains Category
    var arrPartHistory = [PartyHistory]() //contains Party History
}
class SelectedFilter {
    var strCompany = "" //Company Entry
    var strDate = "" //Date
    var strTime = "" //Time
    var strHours = "" // Duration Hours
    var strMinutes = "" // Duration Minutes
    var strEventName = "" //Event Name.
    var strPartyLocation = "" //Party Location.
    var isClient = 0 //Is client
    var arrClient = [Int]() // contains client
    var arrConference = [Int]() //contains Conference
    var arrLocation = [Int]() //contains Location
    var arrScheduler = [Int]() //contains Scheduler
    var arrOutreach = [Int]() //contains Outreach
    var arrPartyStatus = [Int]() //contains Party Status
    var arrCategory = [Int]() //contains Category
    var arrClientTeam = [Int]() //contains Category
    var arrMeetingCover = [Int]() //contains Meeting Cover
    var arrMeeting = [Int]() //contains Meeting History
    var arrPartyHistory = [Int]() //contains Party
    
    func clearValues(){
        strHours = ""
        strMinutes = ""
        strCompany = ""
        strDate = ""
        strTime = ""
        strPartyLocation = ""
        strEventName = ""
        isClient = 0
        arrClient = []
        arrConference = []
        arrLocation = []
        arrScheduler = []
        arrOutreach = []
        arrPartyStatus = []
        arrClientTeam = []
        arrMeetingCover = []
        arrCategory = []
        arrMeeting = []
        arrPartyHistory = []
    }
}
public struct ViewFrame {
    static func getRect(view: UIView) -> CGRect {
        return CGRect(
            origin: CGPoint(x: 0,y :0),
            size:CGSize(width: view.frame.size.width, height: view.frame.size.height))//64 nav bar space + 49 tab bar space
    }
    
}

func DeviceType()->Bool{
    let device = UIDevice.current.userInterfaceIdiom
    if device == .phone{
        return true
    }else{
        return false
    }
}
let eventStore = EKEventStore()
func addEventToCalendar(_ title:String?,_ location :String?,_ eventStartDate:Date,_ eventEndDate:Date,_ vc:UIViewController){
    eventStore.requestAccess(to: .event) { (success, error) in
        if success{
            if  error == nil {
                let event = EKEvent.init(eventStore: eventStore)
                if let titleStr = title{
                    event.title = titleStr
                }
                if let locationStr = location{
                    event.location = locationStr
                }
                event.calendar = eventStore.defaultCalendarForNewEvents // this will return deafult calendar from device calendars
                event.startDate = eventStartDate
                event.endDate = eventEndDate
                
                let alarm = EKAlarm.init(absoluteDate: Date.init(timeInterval: -3600, since: event.startDate))
                event.addAlarm(alarm)
                
                do {
                    try eventStore.save(event, span: .thisEvent)
                    //event created successfullt to default calendar
                } catch let error as NSError {
                    print("failed to save event with error : \(error)")
                }
                
            } else {
                
                //we have error in getting access to device calnedar
                print("error = \(String(describing: error?.localizedDescription))")
            }
        }else{
            vc.alert(message: "Calender Access is required for adding Reminder")
        }
        
    }
}
func callPhone(_ phoneNo:String) {
    if let url = URL(string: "tel://\(phoneNo)") {
        UIApplication.shared.openURL(url)
    }
}
//Json
func convertToJson(_ arrData:[Int])->String{
    let json = JSON(arrData)
    let paramsString = json.rawString(String.Encoding.utf8, options: JSONSerialization.WritingOptions.prettyPrinted)
    return paramsString!
}
//Loading Indicators
var indicator = UIActivityIndicatorView()

func activityIndicatorShow(button:UIButton){
    button.setTitleColor(UIColor.clear, for: .normal)
    button.isUserInteractionEnabled=false
    let halfButtonHeight = button.bounds.size.height / 2
    let buttonWidth = button.bounds.size.width / 2
    indicator.center = CGPoint.init(x: buttonWidth , y: halfButtonHeight)
    indicator.color=UIColor.white
    button.addSubview(indicator)
    indicator.startAnimating()
}
func activityIndicatorHide(button:UIButton){
    button.setTitleColor(UIColor.white, for: .normal)
    button.isUserInteractionEnabled=true
    if indicator.isAnimating{
        indicator.stopAnimating()
    }    
}
var bgView = UIView()
func activityIndicatorShow(_ vc:UIViewController ){
    let indicatorView = UIView()

    let window = UIApplication.shared.keyWindow!
    bgView.frame = window.frame
    indicatorView.frame = bgView.frame
    indicatorView.frame.size.height = bgView.frame.size.height-(vc.navigationController?.navigationBar.frame.size.height)!
    indicatorView.frame.origin.y = bgView.frame.origin.y+(vc.navigationController?.navigationBar.frame.size.height)!
    
    indicator.center = indicatorView.center
    indicator.color=UIColor.black
    bgView.addSubview(indicator)
    UIApplication.shared.delegate?.window!!.addSubview(bgView)
//    vc.view.addSubview(indicator)
    indicator.startAnimating()
}
func activityIndicatorShow(_ vc:UIView ,_ isNav:Bool){
    let indicatorView = UIView(frame: ViewFrame.getRect(view: vc))
    if isNav{
        indicatorView.frame.size.height = vc.frame.size.height-64
    }
    indicator.center = indicatorView.center
    indicator.color=UIColor.black
    vc.addSubview(indicator)
    indicator.startAnimating()
}
func activityIndicatorHide(){
    if indicator.isAnimating{
        bgView.removeFromSuperview()
        indicator.stopAnimating()
    }
}

//Tocken expired
func authenticationError(_ vc:UIViewController){
    
    let alertController = UIAlertController(title: "Authentication Error", message: "Your session expired, Please login again!", preferredStyle: .alert)
    let oKAction = UIAlertAction(title: "OK", style: .default, handler: { alert in
        logOutFromApp()
    })
    alertController.addAction(oKAction)
    vc.present(alertController, animated: true, completion: nil)
    
}
//Log Out
func logOutFromApp(){
    UIApplication.shared.statusBarStyle = .default
    UserDefaults.standard.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
    UserDefaults.standard.synchronize()
    let obj = UIApplication.shared.delegate as! AppDelegate
    
    obj.setRootViewcontroller()
}
//Date Format
func dateFormatBase(_ dateString:String)->Date{
    let formatter=DateFormatter()
    formatter.timeZone = TimeZone.current
    formatter.dateFormat="yyyy-MM-dd HH:mm:ss"
    return formatter.date(from: dateString)!
}
func dateFormatBaseFull(_ dateString:String)->Date{
    let formatter=DateFormatter()
    formatter.timeZone = TimeZone.current
    formatter.dateFormat="yyyy-MM-dd HH:mm:ss Z"
    return formatter.date(from: dateString)!
}

func dateFormatWithDay(_ dateString:String)->String{
    let date = dateFormatBase(dateString)
    let dateFormatter=DateFormatter()
    dateFormatter.timeZone = TimeZone.current
    dateFormatter.dateFormat="EEEE, MMMM dd"
    return dateFormatter.string(from: date)
}
func DateFormatterDateTimeFull(_ date:String) -> String {
    let strDate=dateFormatBaseFull(date)
    let Formatter=DateFormatter()
    Formatter.dateFormat="MM/dd/yyyy"
    return Formatter.string(from: strDate)
}
func DateFormatterDateTimeFullString(_ date:String) -> String {
    let strDate:Date=dateFormatBaseFull(date)
    let Formatter=DateFormatter()
    Formatter.dateFormat="EEEE, MMMM dd"
    return Formatter.string(from: strDate)
}

func DateFormatterDateTimeFullFormated(_ date:Date) -> String {
    let Formatter=DateFormatter()
    Formatter.dateFormat="EEEE, MMMM dd"
    return Formatter.string(from: date)
}
//time
func timeFormat(_ timeString:String)->Date{
    let formatter=DateFormatter()
    formatter.dateFormat="HH:mm:ss"
    return formatter.date(from: timeString)!
}
func timeFormatWithFulldate(_ dateString:String)->String{
    let date = dateFormatBase(dateString)
    let timeFormatter=DateFormatter()
    timeFormatter.timeZone = TimeZone.current
    timeFormatter.dateFormat="hh:mm a"
    return timeFormatter.string(from: date)
}
func timeFormatWithDay(_ timeString:String)->String{
    let time = timeFormat(timeString)
    let timeFormatter=DateFormatter()
    timeFormatter.timeZone = TimeZone.current
    timeFormatter.dateFormat="hh:mm a"
    return timeFormatter.string(from: time)
}
func timeFormatFromDate(_ date:Date) -> String {
    let Formatter=DateFormatter()
    Formatter.dateFormat="hh:mm a"
    return Formatter.string(from: date)
}
func DateFormatTimeFullString(_ date:String) -> String {
    let strDate=dateFormatBaseFull(date)
    let Formatter=DateFormatter()
    Formatter.dateFormat="hh:mm a"
    return Formatter.string(from: strDate)
}
func timeFormatFromDateStringToServer(_ date:String) -> String {
    let strDate:Date=dateFormatBaseFull(date)
    let Formatter=DateFormatter()
    Formatter.dateFormat="HH:mm"
    let dateStr = Formatter.string(from: strDate)
    return dateStr+":00"
}


//Network errors
func checkNetworkError(_ error:Error) -> String{
    switch error._code{
    case NSURLErrorTimedOut:
        return "Connection time out error, Please try again later."
    case NSURLErrorCannotConnectToHost:
        return "Server is not available, Please try again later."
    case NSURLErrorCannotFindHost:
        return "Host is not available, Please try again later."
    case NSURLErrorNetworkConnectionLost:
        return "Network Connection is not available, Please try again later."
    case NSURLErrorNotConnectedToInternet:
        return "It seems that there is no Internet connection."
    default:
        return "Please try again later."
    }
}

