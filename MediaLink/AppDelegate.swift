//
//  AppDelegate.swift
//  MediaLink
//
//  Created by Naveen on 2/27/18.
//  Copyright © 2018 Naveen. All rights reserved.
//

import UIKit
import UserNotifications
import Firebase
import RealmSwift
import Realm
import Alamofire
import AlamofireObjectMapper

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, MessagingDelegate {

    var window: UIWindow?
    var objFilterData = SaveFilterData()
    var deviceTocken = String()
    var isAppStarting = false
    var arrNotifications = [Notification]()    

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        // Register for Push notification
        registerPushNotification(application)
        
        //configure Firebase
        FirebaseApp.configure()
        Messaging.messaging().delegate = self 
        
        // Override point for customization after application launch.
        setRootViewcontroller()
        setNavigationColors()
        
        if let userInfo = launchOptions?[UIApplicationLaunchOptionsKey.remoteNotification] as? [String: Any] {
            // App launching from a remote notification
            if
                let info = userInfo["aps"] as? [String:Any],
                let strID = userInfo["id"] as? String {
                
                if
                    let msg = info["alert"] as? [String: Any],
                    let id = Int(strID) {
                    
                    addNotificationToRealm(with: msg, id: id, completionHandler: { (success) -> Void in
                        if success {
                            print("App loaded with new notification data")                            
                        }
                    })
                }
            }
        } else if application.applicationIconBadgeNumber > 0 {
            let repo = MediaLinkUserDefaults()
            if repo.isLoggedIn {
                let realm = try! Realm()
                let notifications = realm.objects(Notification.self)
                for element in notifications {
                    self.arrNotifications.append(element)
                }
                self.getNotificationListFromServer()
            }
        }
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
        self.isAppStarting = false
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        self.isAppStarting = false
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        self.isAppStarting = true
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        application.applicationIconBadgeNumber = 0
        self.isAppStarting = false
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    func application(received remoteMessage: MessagingRemoteMessage) {
        print("Received data message: \(remoteMessage.appData)")
    }
    
    // Register for push notification
    func registerPushNotification(_ application : UIApplication){
        if #available(iOS 10, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()
    }
    
    // Start receiving messages
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        
        // Print full message.
        print(userInfo)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        let appState = UIApplication.shared.applicationState
        
        if appState == UIApplicationState.background || appState == .inactive && !self.isAppStarting {
        // perform the background fetch and
        // call completion handler
            if
                let info = userInfo["aps"] as? [String:Any],
                let strId = userInfo["id"] as? String {
                
                if
                    let msg = info["alert"] as? [String: Any],
                    let id = Int(strId) {
                    
                    addNotificationToRealm(with: msg, id: id, completionHandler: { (success) -> Void in
                        if success {
                            completionHandler(.newData)
                        } else {
                            completionHandler(.noData)
                        }
                    })
                }
            }
        } else if appState == .inactive && self.isAppStarting {
        // User launched the app
            if
                let info = userInfo["aps"] as? [String:Any],
                let strId = userInfo["id"] as? String {
                
                if
                    let msg = info["alert"] as? [String: Any],
                    let id = Int(strId) {
                    
                    addNotificationToRealm(with: msg, id: id, completionHandler: { (success) -> Void in
                        if success {
                            completionHandler(.newData)
                        } else {
                            completionHandler(.noData)
                        }
                    })
                }
            }
        } else {
        // app is active
            if
                let info = userInfo["aps"] as? [String:Any],
                let strId = userInfo["id"] as? String {
                
                if
                    let msg = info["alert"] as? [String: Any],
                    let id = Int(strId) {
                    
                    addNotificationToRealm(with: msg, id: id, completionHandler:{ (success) -> Void in
                        if success {
                            completionHandler(.newData)
                        } else {
                            completionHandler(.noData)
                        }
                    })
                }
            }
        }
    }
    // End receiving messages
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Unable to register for remote notifications: \(error.localizedDescription)")
    }
    
    // This function is added here only for debugging purposes, and can be removed if swizzling is enabled.
    // If swizzling is disabled then this function must be implemented so that the APNs token can be paired to
    // the FCM registration token.
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let tokenParts = deviceToken.map { data -> String in
            return String(format: "%02.2hhx", data)
        }
        let token = tokenParts.joined()
        self.deviceTocken = token        
        print("APNs token retrieved: \(token)")
        // With swizzling disabled you must set the APNs token here.
        // Messaging.messaging().apnsToken = deviceToken
    }
    
    func setRootViewcontroller(){
        let storyboard = UIStoryboard(name: "MenuSection1", bundle: nil)
        let leftMenuVC = storyboard.instantiateViewController(withIdentifier: "LeftMenuViewController") as! LeftMenuViewController
        let rightMenuVC = storyboard.instantiateViewController(withIdentifier: "RightMenuViewController") as! RightMenuViewController
        var slideMenuController = ExSlideMenuController()
        let repo = MediaLinkUserDefaults()
        if repo.isLoggedIn{
            if DeviceType() { //iPhone
                let mainVC = storyboard.instantiateViewController(withIdentifier: "MyScheduleViewController") as! MyScheduleViewController
                let nvc: UINavigationController = UINavigationController(rootViewController: mainVC)
                leftMenuVC.mainViewController = nvc
                rightMenuVC.mainViewController = nvc
                slideMenuController = ExSlideMenuController(mainViewController: nvc, leftMenuViewController: leftMenuVC, rightMenuViewController: rightMenuVC)
                
                slideMenuController.delegate = mainVC                
            }else{//iPad
                let mainVC = storyboard.instantiateViewController(withIdentifier: "MyScheduleSplitViewViewController")
                leftMenuVC.mainViewController = mainVC
                rightMenuVC.mainViewController = mainVC
                slideMenuController = ExSlideMenuController(mainViewController:mainVC, leftMenuViewController: leftMenuVC, rightMenuViewController: rightMenuVC)

                slideMenuController.delegate = mainVC as? SlideMenuControllerDelegate

            }
            slideMenuController.automaticallyAdjustsScrollViewInsets = true
            self.window?.backgroundColor = UIColor(red: 236.0, green: 238.0, blue: 241.0, alpha: 1.0)
            self.window?.rootViewController = slideMenuController
            self.window?.makeKeyAndVisible()
        }else{
            if DeviceType() { //iPhone
                let signInVC = storyboard.instantiateViewController(withIdentifier: "SignInsTableViewController") as! SignInsTableViewController
                self.window?.rootViewController = signInVC
            }else{
                let signInIpadVC = storyboard.instantiateViewController(withIdentifier: "IpadSignInsTableViewController") as! SignInsTableViewController
                self.window?.rootViewController = signInIpadVC
            }
        }
    }
    
    // MARK: - Helper methods
    
    // Set theme for application
    func setNavigationColors(){
        UINavigationBar.appearance().barTintColor = UIColor.colorPrimary()
        UINavigationBar.appearance().isTranslucent = false
        UINavigationBar.appearance().shadowImage = UIImage()
        UINavigationBar.appearance().setBackgroundImage(UIImage(), for: .default)
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        UINavigationBar.appearance().barStyle = UIBarStyle.blackOpaque
        UINavigationBar.appearance().tintColor = UIColor.white
        UIApplication.shared.statusBarStyle = .lightContent
    }
    
    // Add element to realm
    func addNotificationToRealm(with info: [String: Any], id: Int, completionHandler: (_ success: Bool) -> Void) {
        let notification = Notification()
        if
            let title = info["title"] as? String,
            let message = info["body"] as? String {
            
            notification.id = id
            notification.status = 0
            notification.title = title
            notification.message = message
            notification.date = Date()
        
            do {
                let realm = try Realm()
                try realm.write {
                    realm.add(notification, update: true)
                    completionHandler(true)
                }
            } catch let error as NSError {
                print(error.localizedDescription)
                completionHandler(false)
            }
        }
    }
    
    // Convert JSON string to dictionary
    func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    // MARK: - Webservice
    
    func getNotificationListFromServer() {
        let url = MediaLinkUrls.notification
        let repo = MediaLinkUserDefaults()
        let notificationdate = repo.notificationDate.toDate        
        var params = ["token":repo.tocken!]
        
        let compaired = Calendar.current.compare(Date(), to: notificationdate, toGranularity: .day)
        
        switch compaired {
        case .orderedDescending: // Date is less than current date
            if let date = Calendar.current.date(byAdding: .day, value: 1, to: notificationdate) {
                repo.notificationDate = date.stringFormatted
                params["date"] = date.stringFormatted
            }
        case .orderedAscending: // Date is greater than current date
            let todayDate = Date()
            repo.notificationDate = todayDate.stringFormatted
            params["date"] = todayDate.stringFormatted
        case .orderedSame: // Current date
            params["date"] = notificationdate.stringFormatted
        }
        
        request(url, method: .get, parameters: params)
            .validate()
            .responseArray { (response: DataResponse<[Notification]>) in
                
                switch response.result {
                case.success(let successValue):
                    if successValue.count != 0 {
                        for element in successValue {
                            if !self.arrNotifications.contains(where: { $0.id == element.id }) {                               
                                let notification = Notification()
                                notification.id = element.id
                                notification.date = Date()
                                notification.status = 0
                                notification.title = element.title
                                notification.message = element.message
                                
                                do {
                                    let realm = try Realm()
                                    try realm.write {
                                        realm.add(notification, update: true)
                                    }
                                } catch let error as NSError {
                                    print(error.localizedDescription)
                                }
                            }
                        }
                    }
                case.failure(let error):
                    if let statusCode = response.response?.statusCode {
                        if statusCode == 422 {
                            
                        }
                    } else {
                        print(checkNetworkError(error))
                    }
                }
        }
    }
}

// MARK: - User notificationcenter delegate methods

@available(iOS 10, *)
extension AppDelegate : UNUserNotificationCenterDelegate {
    
    // Receive displayed notifications for iOS 10 devices.
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
 
        let userInfo = notification.request.content.userInfo
        
        if UIApplication.shared.applicationState == UIApplicationState.active {
            if
                let info = userInfo["aps"] as? [String:Any],
                let strId = userInfo["id"] as? String {
                
                if
                    let msg = info["alert"] as? [String: Any],
                    let id = Int(strId) {
                    
                    addNotificationToRealm(with: msg, id: id, completionHandler:{ (success) -> Void in
                        if success {
                            completionHandler([])
                        } else {
                            completionHandler([])
                        }
                    })
                }
            }
        }
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
        
        // Print full message.
        print(userInfo)
        
        completionHandler()
    }
}
