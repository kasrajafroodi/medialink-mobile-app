//
//  EventDatabase.swift
//  MediaLink
//
//  Created by Naveen on 3/16/18.
//  Copyright © 2018 Naveen. All rights reserved.
//

import ObjectMapper
class EventDatabase: Mappable {
    var data : [EventParty]?
    var date : String?
    required init?(map: Map) {
        
    }
    func mapping(map: Map)
    {
        data <- map["data"]
        date <- map["date"]
        
    }
}
