//
//  Clients.swift
//  MediaLink
//
//  Created by Naveen on 3/13/18.
//  Copyright © 2018 Naveen. All rights reserved.
//

import ObjectMapper
class Client: Mappable {
    var clientName : String?
    var id : Int?
    required init?(map: Map) {
        
    }
    func mapping(map: Map)
    {
        clientName <- map["client_name"]
        id <- map["id"]
        
    }
}
