//
//  Location.swift
//  MediaLink
//
//  Created by Naveen on 3/13/18.
//  Copyright © 2018 Naveen. All rights reserved.
//

import ObjectMapper
class Location: Mappable {
    var id : Int?
    var location : String?
    required init?(map: Map) {
        
    }
    func mapping(map: Map)
    {
        id <- map["id"]
        location <- map["location"]
        
    }
}
