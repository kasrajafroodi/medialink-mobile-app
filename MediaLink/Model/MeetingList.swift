//
//  ActiveMeeting.swift
//  MediaLink
//
//  Created by Naveen on 3/13/18.
//  Copyright © 2018 Naveen. All rights reserved.
//

import ObjectMapper
class MeetingList: Mappable {
    var data : [Meeting]?
    var date : String?
    required init?(map: Map) {
        
    }
    func mapping(map: Map)
    {
        data <- map["data"]
        date <- map["date"]
        
    }
}
