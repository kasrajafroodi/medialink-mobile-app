//
//  ContactDatabase.swift
//  MediaLink
//
//  Created by Naveen on 3/16/18.
//  Copyright © 2018 Naveen. All rights reserved.
//

import ObjectMapper
class ContactDatabase: Mappable {
    var assistantEmail : String?
    var company : String?
    var contactCategories : [ContactCategory]?
    var contactConferences : [Conference]?
    var contactNotes : String?
    var contactParties : [EventParty]?
    var country : String?
    var email : String?
    var firstName : String?
    var id : Int?
    var lastName : String?
    var meetingHistory : [MeetingHistory]?
    var outreachLead : Int?
    var outreachLeadName : String?
    var phoneNumber : String?
    var picture : String?
    var schedulerId : Int?
    var schedulerName : String?
    required init?(map: Map) {
        
    }
    func mapping(map: Map)
    {
        assistantEmail <- map["assistant_email"]
        company <- map["company"]
        contactCategories <- map["contact_categories"]
        contactConferences <- map["contact_conferences"]
        contactNotes <- map["contact_notes"]
        contactParties <- map["contact_parties"]
        country <- map["country"]
        email <- map["email"]
        firstName <- map["first_name"]
        id <- map["id"]
        lastName <- map["last_name"]
        meetingHistory <- map["meeting_history"]
        outreachLead <- map["outreach_lead"]
        outreachLeadName <- map["outreach_lead_name"]
        phoneNumber <- map["phone_number"]
        picture <- map["picture"]
        schedulerId <- map["scheduler_id"]
        schedulerName <- map["scheduler_name"]
        
    }
}
