//
//  Attendees.swift
//  MediaLink
//
//  Created by Naveen on 3/14/18.
//  Copyright © 2018 Naveen. All rights reserved.
//

import ObjectMapper
class Attendees: Mappable {
    var company : String?
    var contactId : Int?
    var firstName : String?
    var lastName : String?
    var outreachId : Int?
    var picture : String?
    required init?(map: Map) {
        
    }
    func mapping(map: Map)
    {
        company <- map["company"]
        contactId <- map["contact_id"]
        firstName <- map["first_name"]
        lastName <- map["last_name"]
        outreachId <- map["outreach_id"]
        picture <- map["picture"]
        
    }
}
