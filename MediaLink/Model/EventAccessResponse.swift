//
//  EventAccessResponse.swift
//  MediaLink
//
//  Created by Naveen on 3/15/18.
//  Copyright © 2018 Naveen. All rights reserved.
//

import ObjectMapper

class EventAccessResponse: Mappable {
    var activeRequests : [EventRequest]?
    var pastRequests : [EventRequest]?
    required init?(map: Map) {
        
    }
    func mapping(map: Map)
    {
        activeRequests <- map["active_requests"]
        pastRequests <- map["past_requests"]
        
    }
}
