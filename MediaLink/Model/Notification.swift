//
//  Notification.swift
//  MediaLink
//
//  Created by Naveen on 8/3/18.
//  Copyright © 2018 Naveen. All rights reserved.
//


import Realm
import RealmSwift
import ObjectMapper

class Notification: Object, Mappable {
    
    @objc dynamic var createdAt : String?
    @objc dynamic var id : Int=0
    @objc dynamic var date: Date?
    @objc dynamic var message : String?
    @objc dynamic var title : String?
    @objc dynamic var status: Int=0

    override class func primaryKey() -> String? {
        return "id"
    }
    
    required convenience init?(map: Map){
        self.init()
    }
    
    func mapping(map: Map)
    {
        createdAt <- map["created_at"]
        id <- map["id"]
        message <- map["message"]
        title <- map["title"]
    }
}
