//
//  Category.swift
//  MediaLink
//
//  Created by Naveen on 3/20/18.
//  Copyright © 2018 Naveen. All rights reserved.
//

import ObjectMapper
class Categories: Mappable {
    var categoryName : String?
    var id : Int?
    required init?(map: Map) {
        
    }
    func mapping(map: Map)
    {
        categoryName <- map["category_name"]
        id <- map["id"]
        
    }
}
