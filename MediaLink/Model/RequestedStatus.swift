//
//  RequestedStatus.swift
//  MediaLink
//
//  Created by Naveen on 3/20/18.
//  Copyright © 2018 Naveen. All rights reserved.
//

import ObjectMapper
class RequestedStatus: Mappable {
    var id : Int?
    var requestStatus : String?
    required init?(map: Map) {
        
    }
    func mapping(map: Map)
    {
        id <- map["id"]
        requestStatus <- map["request_status"]
        
    }
}
