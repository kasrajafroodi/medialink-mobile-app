//
//  MeetingsResponse.swift
//  MediaLink
//
//  Created by Naveen on 3/13/18.
//  Copyright © 2018 Naveen. All rights reserved.
//

import ObjectMapper

class MeetingResponse: Mappable {
    var activeMeetings : [MeetingList]?
    var pastMeetings : [MeetingList]?
    required init?(map: Map) {
        
    }
    func mapping(map: Map)
    {
        activeMeetings <- map["active_meetings"]
        pastMeetings <- map["past_meetings"]
        
    }
}
