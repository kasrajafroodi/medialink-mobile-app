//
//  Login.swift
//  MediaLink
//
//  Created by Naveen on 3/9/18.
//  Copyright © 2018 Naveen. All rights reserved.
//

import ObjectMapper

class Login : Mappable{
    var email : String?
    var isAdmin : Bool = false
    var message : String?
    var name : String?
    var token : String?
    var userId : Int?
    required init?(map: Map) {
        
    }
    func mapping(map: Map)
    {
        email <- map["email"]
        isAdmin <- map["is_admin"]
        message <- map["message"]
        name <- map["name"]
        token <- map["token"]
        userId <- map["user_id"]
        
    }
}
