//
//  EventRequest.swift
//  MediaLink
//
//  Created by Naveen on 3/15/18.
//  Copyright © 2018 Naveen. All rights reserved.
//

import ObjectMapper

class EventRequest: Mappable {
    var data : [EventAccess]?
    var partyName : String?
    required init?(map: Map) {
        
    }
    func mapping(map: Map)
    {
        data <- map["data"]
        partyName <- map["party_name"]
        
    }
}
