//
//  EventAccess.swift
//  MediaLink
//
//  Created by Naveen on 3/15/18.
//  Copyright © 2018 Naveen. All rights reserved.
//

import ObjectMapper
class EventAccess: Mappable {
    var company : String?
    var contactId : Int?
    var dateTime : String?
    var firstName : String?
    var id : Int?
    var lastName : String?
    var partyId : Int?
    var notes : String?
    var partyLocation : String?
    var partyName : String?
    var partyStatus : String?
    required init?(map: Map) {
        
    }
    func mapping(map: Map)
    {
        company <- map["company"]
        contactId <- map["contact_id"]
        dateTime <- map["date_time"]
        firstName <- map["first_name"]
        id <- map["id"]
        lastName <- map["last_name"]
        partyId <- map["party_id"]
        notes <- map["notes"]
        partyLocation <- map["party_location"]
        partyName <- map["party_name"]
        partyStatus <- map["party_status"]
        
    }
}
