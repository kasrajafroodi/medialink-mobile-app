//
//  PastMeeting.swift
//  MediaLink
//
//  Created by Naveen on 3/13/18.
//  Copyright © 2018 Naveen. All rights reserved.
//

import ObjectMapper
class Meeting: Mappable {
    var clientColor : Int?
    var clientId : Int?
    var clientName : String?
    var company : String?
    var companyColor : Int?
    var date : String?
    var id : Int?
    var location : String?
    var meetingColor : Int?
    var meetingCoverName : String?    
    var meetingNotes : String?
    var outreachLead : Int?
    var requestStatus : String?
    var time : String?
    
    
    
    
    required init?(map: Map) {
        
    }
    func mapping(map: Map)
    {
        clientColor <- map["client_color"]
        clientId <- map["client_id"]
        clientName <- map["client_name"]
        company <- map["company"]
        companyColor <- map["company_color"]
        date <- map["date"]
        id <- map["id"]
        location <- map["location"]
        meetingColor <- map["meeting_color"]
        meetingCoverName <- map["meeting_cover_name"]
        meetingNotes <- map["meeting_notes"]
        outreachLead <- map["outreach_lead"]
        requestStatus <- map["request_status"]
        time <- map["time"]
        
    }
}
