//
//  Status.swift
//  MediaLink
//
//  Created by Naveen on 3/15/18.
//  Copyright © 2018 Naveen. All rights reserved.
//

import ObjectMapper
class PartyStatus: Mappable {
    var id : Int?
    var partyStatus : String?
    required init?(map: Map) {
        
    }
    func mapping(map: Map)
    {
        id <- map["id"]
        partyStatus <- map["party_status"]
        
    }
}
