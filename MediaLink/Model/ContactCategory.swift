//
//  ContactCategory.swift
//  MediaLink
//
//  Created by Naveen on 3/16/18.
//  Copyright © 2018 Naveen. All rights reserved.
//

import ObjectMapper
class ContactCategory: Mappable {
    var categoryName : String?
    var createdAt : String?
    var deletedAt : String?
    var employeId : Int?
    var id : Int?
    var updatedAt : String?
    
    required init?(map: Map) {
        
    }
    func mapping(map: Map)
    {
        categoryName <- map["category_name"]
        createdAt <- map["created_at"]
        deletedAt <- map["deleted_at"]
        employeId <- map["employe_id"]
        id <- map["id"]
        updatedAt <- map["updated_at"]
        
    }
}
