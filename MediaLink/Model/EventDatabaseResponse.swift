//
//  EventDatabaseResponse.swift
//  MediaLink
//
//  Created by Naveen on 3/16/18.
//  Copyright © 2018 Naveen. All rights reserved.
//

import ObjectMapper
class EventDatabaseResponse: Mappable {
    var activeParties : [EventDatabase]?
    var pastParties : [EventDatabase]?
    required init?(map: Map) {
        
    }
    func mapping(map: Map)
    {
        activeParties <- map["active_parties"]
        pastParties <- map["past_parties"]
        
    }
}
