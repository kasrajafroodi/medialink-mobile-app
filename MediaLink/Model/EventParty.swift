//
//  EventParties.swift
//  MediaLink
//
//  Created by Naveen on 3/16/18.
//  Copyright © 2018 Naveen. All rights reserved.
//

import ObjectMapper
class EventParty: Mappable {
    var date : String?
    var dateTime : String?
    var eventName : String?
    var id : Int?
    var notes : String?
    var partyLocation : String?
    var partyName : String?

    required init?(map: Map) {
        
    }
    func mapping(map: Map)
    {
        date <- map["date"]
        dateTime <- map["date_time"]
        eventName <- map["event_name"]
        id <- map["id"]
        notes <- map["notes"]
        partyLocation <- map["party_location"]
        partyName <- map["party_name"]
        
    }
}
