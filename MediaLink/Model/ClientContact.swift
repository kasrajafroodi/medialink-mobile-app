//
//  ClientContact.swift
//  MediaLink
//
//  Created by Naveen on 4/4/18.
//  Copyright © 2018 Naveen. All rights reserved.
//

import ObjectMapper
class ClientContact: Mappable {
    var firstName : String?
    var id : Int?
    var lastName : String?
    required init?(map: Map) {
        
    }
    func mapping(map: Map)
    {
        firstName <- map["first_name"]
        id <- map["id"]
        lastName <- map["last_name"]
        
    }
}
