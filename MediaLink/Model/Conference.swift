//
//  Conference.swift
//  MediaLink
//
//  Created by Naveen on 3/13/18.
//  Copyright © 2018 Naveen. All rights reserved.
//

import ObjectMapper
class Conference: Mappable {
    var eventName : String?
    var id : Int?
    required init?(map: Map) {
        
    }
    func mapping(map: Map)
    {
        eventName <- map["event_name"]
        id <- map["id"]
        
    }
}
