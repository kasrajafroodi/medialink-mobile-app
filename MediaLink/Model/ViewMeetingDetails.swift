//
//  ViewMeetingDetails.swift
//  MediaLink
//
//  Created by Naveen on 3/14/18.
//  Copyright © 2018 Naveen. All rights reserved.
//

import ObjectMapper
class ViewMeetingDetails: Mappable {
    var clientAttendees : [Attendees]?
    var clientId : Int?
    var clientName : String?
    var company : String?
    var date : String?
    var employees : [Employee]?
    var eventName : String?
    var id : Int?
    var location : String?
    var locationId : Int?
    var meetingCover : Int?
    var meetingCoverName : String?
    var meetingNotes : String?
    var outreachLead : Int?
    var outreachLeadName : String?
    var outreachRequestStatusId : Int?
    var quickNote : String?
    var requestStatus : String?
    var schedulerId : Int?
    var schedulerName : String?
    var targetAttendees : [Attendees]?
    var time : String?
    var clientColor : Int?
    var companyColor : Int?
    var meetingColor : Int?
    var duration : String?
    
    required init?(map: Map) {
        
    }
    func mapping(map: Map)
    {
        clientAttendees <- map["client_attendees"]
        clientId <- map["client_id"]
        clientName <- map["client_name"]
        company <- map["company"]
        date <- map["date"]
        employees <- map["employees"]
        eventName <- map["event_name"]
        id <- map["id"]
        location <- map["location"]
        locationId <- map["location_id"]
        meetingCover <- map["meeting_cover"]
        meetingCoverName <- map["meeting_cover_name"]
        meetingNotes <- map["meeting_notes"]
        outreachLead <- map["outreach_lead"]
        outreachLeadName <- map["outreach_lead_name"]
        quickNote <- map["quick_note"]
        requestStatus <- map["request_status"]
        outreachRequestStatusId <- map["outreach_request_status_id"]
        schedulerId <- map["scheduler_id"]
        schedulerName <- map["scheduler_name"]
        targetAttendees <- map["target_attendees"]
        time <- map["time"]
        clientColor <- map["client_color"]
        companyColor <- map["company_color"]
        meetingColor <- map["meeting_color"]
        duration <- map["duration"]
    }
}
