//
//  MeetingHistory.swift
//  MediaLink
//
//  Created by Naveen on 3/16/18.
//  Copyright © 2018 Naveen. All rights reserved.
//

import ObjectMapper
class MeetingHistory: Mappable {
    var clientId : Int?
    var clientName : String?
    var contactId : Int?
    var isScheduledRequest : Int?
    var outreachRequestStatusId : Int?
    var requestStatus : String?
    required init?(map: Map) {
        
    }
    func mapping(map: Map)
    {
        clientId <- map["client_id"]
        clientName <- map["client_name"]
        contactId <- map["contact_id"]
        isScheduledRequest <- map["is_scheduled_request"]
        outreachRequestStatusId <- map["outreach_request_status_id"]
        requestStatus <- map["request_status"]
        
    }
}
