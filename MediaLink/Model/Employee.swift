//
//  Employee.swift
//  MediaLink
//
//  Created by Naveen on 3/14/18.
//  Copyright © 2018 Naveen. All rights reserved.
//

import ObjectMapper
class Employee: Mappable {
    var createdAt : String?
    var clientList : [Client]?
    var dailySummary : Int?
    var deletedAt : String?
    var email : String?
    var firstName : String?
    var id : Int?
    var immediateNotification : Int?
    var isAdmin : Int?
    var lastName : String?
    var phone : String?
    var timeZone : String?
    var title : String?
    var updatedAt : String?
    required init?(map: Map) {
        
    }
    func mapping(map: Map)
    {
        clientList <- map["client_list"]
        createdAt <- map["created_at"]
        dailySummary <- map["daily_summary"]
        deletedAt <- map["deleted_at"]
        email <- map["email"]
        firstName <- map["first_name"]
        id <- map["id"]
        immediateNotification <- map["immediate_notification"]
        isAdmin <- map["is_admin"]
        lastName <- map["last_name"]
        phone <- map["phone"]
        timeZone <- map["time_zone"]
        title <- map["title"]
        updatedAt <- map["updated_at"]
        
    }
}
