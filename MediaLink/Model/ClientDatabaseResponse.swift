//
//  ClientDatabaseResponse.swift
//  MediaLink
//
//  Created by Naveen on 3/17/18.
//  Copyright © 2018 Naveen. All rights reserved.
//

import ObjectMapper
class ClientDatabaseResponse: Mappable{
    var activeClients : [ClientDatabase]?
    var inActiveClients : [ClientDatabase]?
    required init?(map: Map) {
        
    }
    func mapping(map: Map)
    {
        activeClients <- map["active_clients"]
        inActiveClients <- map["in_active_clients"]
        
    }
}
