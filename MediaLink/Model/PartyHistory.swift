//
//  PartyHistory.swift
//  MediaLink
//
//  Created by Naveen on 3/23/18.
//  Copyright © 2018 Naveen. All rights reserved.
//

import ObjectMapper
class PartyHistory: Mappable {
    var id : Int?
    var partyName : String?
    required init?(map: Map) {
        
    }
    func mapping(map: Map)
    {
        id <- map["id"]
        partyName <- map["party_name"]
        
    }
}
