//
//  ClientDatabase.swift
//  MediaLink
//
//  Created by Naveen on 3/17/18.
//  Copyright © 2018 Naveen. All rights reserved.
//

import ObjectMapper
class ClientDatabase: Mappable {
    var categories : [ContactCategory]?
    var clientName : String?
    var clientContacts : [ClientContact]?
    var clientTeam : [ClientTeam]?
    var id : Int?
    var internalDescription : String?
    var rationaleForAgencies : String?
    var rationaleForBrands : String?
    var rationaleForPublishers : String?
    required init?(map: Map) {
        
    }
    func mapping(map: Map)
    {
        categories <- map["categories"]
        clientName <- map["client_name"]
        clientContacts <- map["client_contacts"]
        clientTeam <- map["client_team"]
        id <- map["id"]
        internalDescription <- map["internal_description"]
        rationaleForAgencies <- map["rationale_for_agencies"]
        rationaleForBrands <- map["rationale_for_brands"]
        rationaleForPublishers <- map["rationale_for_publishers"]
        
    }
}
