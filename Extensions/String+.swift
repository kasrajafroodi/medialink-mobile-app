//
//  String+.swift
//  MediaLink
//
//  Created by Naveen on 3/13/18.
//  Copyright © 2018 Naveen. All rights reserved.
//

import Foundation
extension String {
    
    //To check text field or String is blank or not
    var isBlank: Bool {
        get {
            let trimmed = trimmingCharacters(in: CharacterSet.whitespaces)
            return trimmed.isEmpty
        }
    }
    
    var toDate: Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        
        return dateFormatter.date(from: self)!
    }
}
