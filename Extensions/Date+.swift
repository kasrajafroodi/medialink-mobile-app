//
//  Date+.swift
//  MediaLink
//
//  Created by Naveen on 8/14/18.
//  Copyright © 2018 Naveen. All rights reserved.
//

import Foundation

extension Date {
    
    var stringFormatted: String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        return dateFormatter.string(from: self)
    }
}
