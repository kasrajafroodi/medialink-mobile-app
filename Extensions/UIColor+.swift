//
//  UIColor+.swift
//  MediaLink
//
//  Created by Naveen on 2/27/18.
//  Copyright © 2018 Naveen. All rights reserved.
//

//
//  UIColor+.swift
//  Juiced
//
//  Created by Raisal on 06/05/16.
//  Copyright © 2016 Pixbit. All rights reserved.
//

import UIKit


extension UIColor {
    
    
    class func hex (hexStr : NSString, alpha : CGFloat) -> UIColor {
        
        let hexStr = hexStr.replacingOccurrences(of: "#", with: "")
        let scanner = Scanner(string: hexStr as String)
        var color: UInt32 = 0
        if scanner.scanHexInt32(&color) {
            let r = CGFloat((color & 0xFF0000) >> 16) / 255.0
            let g = CGFloat((color & 0x00FF00) >> 8) / 255.0
            let b = CGFloat(color & 0x0000FF) / 255.0
            return UIColor(red:r,green:g,blue:b,alpha:alpha)
        } else {
            print("invalid hex string", terminator: "")
            return UIColor.white
        }
    }
    
    class func colorPrimary() -> UIColor {
        return UIColor.hex(hexStr: "253a5d", alpha: 1)
    }
    
    class func colorPrimaryLight() -> UIColor {
        return UIColor.hex(hexStr: "2C6D99", alpha: 1)
    }
    
    class func colorPrimaryDark() -> UIColor {
        return UIColor.hex(hexStr: "346676", alpha: 1)
    }
    
    class func colorAccent() -> UIColor {
        return UIColor.hex(hexStr: "FF5C41", alpha: 1)
    }
    
    class func colorLightGrey() -> UIColor {
        return UIColor.hex(hexStr: "EEEEEE", alpha: 1)
    }
    class func colorError() -> UIColor {
        return UIColor.hex(hexStr: "EE525F", alpha: 1)
    }
    class func colorMessage() -> UIColor {
        return lightGray
    }
    class func colorSuccus() -> UIColor {
        return UIColor.hex(hexStr: "1dc91d", alpha: 1)
    }
    class var colorNoNotes: UIColor {
        return UIColor.hex(hexStr: "f2918a", alpha: 1)
    }
    
    class var colorNotes: UIColor {
        return UIColor.hex(hexStr: "68c1a7", alpha: 1)
    }
    class var colorGreen: UIColor {
        return UIColor.hex(hexStr: "06a306", alpha: 1)
    }
    
    class var colorRed: UIColor {
        return UIColor.hex(hexStr: "d1443a", alpha: 1)
    }
}

