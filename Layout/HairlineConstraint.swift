//
//  HairlineConstraint.swift
//  MediaLink
//
//  Created by Naveen on 2/27/18.
//  Copyright © 2018 Naveen. All rights reserved.
//

import UIKit

class HairlineConstraint: NSLayoutConstraint {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.constant = 1.0 / UIScreen.main.scale
    }
    
}
